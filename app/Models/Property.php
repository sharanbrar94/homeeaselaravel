<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Library\CommonFunction;
use Illuminate\Support\Facades\Validator;

class Property extends Model
{

    protected $fillable = [
        "id", "user_id", "agent_id", "is_subject_prop","type","address_1","address_2", "lat", "lng", "city", "county", "zip_code", "country", "sale_price","sale_type","sqft_above","sqft_below","total_sqft","date_sold","year_build","bedroom_above","bathroom_above","total_rooms","bedroom_below","bathroom_below", "other", "heat_type","ac","garage_space","porch","fire_place","mls_id", "acerage", "floor", "condo_fees", "unit1", "unit2", "description"
    ];
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Relations
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function agent() {
        return $this->belongsTo('App\Models\User', 'agent_id', 'id');
    }

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    public function getImagesAttribute($value) {
        $image_arr =[];
        if ($value) {             
            $image_arr = json_decode($value);   
            foreach($image_arr as $key => $value2) {
                $image_arr[$key] = url(CommonFunction::PICTURE_PATH.$value2);
            }                
            return $image_arr;
        }
        else {
            return $image_arr;
        }
    }


    public function getPositiveAddAttribute($value) {
        $positive = [];
        if ($value) {          
            $positive = json_decode($value);   
            foreach($positive as $key => $value2) {
            }  
            return $positive;             
        }
        else {
            return $positive; 
        }
    }

    public function getNegativeAddAttribute($value) {
        $negative = [];
        if ($value) {          
            $negative = json_decode($value);   
            foreach($negative as $key => $value2) {
            }  
            return $negative;             
        }
        else {
            return $negative; 
        }
    }

    public function getLocationAttribute($value) {
        $location = [];
        if ($value) {          
            $location = json_decode($value);   
            foreach($location as $key => $value2) {
            }  
            return $location;             
        }
        else {
            return $location; 
        }
    }

    public function getAcerageValueAttribute($value) {
        $acerage = [];
        if ($value) {          
            $acerage = json_decode($value);   
            foreach($acerage as $key => $value2) {
            }  
            return $acerage;             
        }
        else {
            return $acerage; 
        }
    }

    public function getCondoArrAttribute($value) {
        $condo = [];
        if ($value) {          
            $condo = json_decode($value);   
            foreach($condo as $key => $value2) {
            }  
            return $condo;             
        }
        else {
            return $condo; 
        }
    }

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Validation Rules
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------
         * Api Validations
        --------------------------------------------------------------------------*/

        public static $apiEditPropertyRules = array(
            'property_id' => 'required',
            'type' => 'in:1,2,3,4',
            'sale_price' => 'numeric',
            'sqft_above' => 'numeric',
            'sqft_below' => 'numeric',
            'total_sqft' => 'numeric',
            'year_build' => 'numeric:|min:1111|max:9999',
            'bedroom_above' => 'numeric|max:25',
            'bathroom_above' => 'numeric|max:25',
            'total_rooms' => 'numeric|max:50',
            'bedroom_below' => 'numeric|max:25',
            'bathroom_below' => 'numeric|max:25',
            'heat_type' => 'numeric|in:0,1,2',
            'ac' => 'numeric|in:0,1',
            'garage_space' => 'numeric|max:20',
            'porch' => 'numeric|max:20',
            'fire_place' => 'numeric|max:20',
            // 'condo_fees' => 'numeric',
        );
        
        public static $someRules = array(
            'type' => 'in:1,2,3,4',
            'sale_price' => 'required|numeric',
            'sqft_above' => 'required|numeric',
            'sqft_below' => 'required|numeric',
            'total_sqft' => 'required|numeric',
            'year_build' => 'required|numeric:|min:1111|max:9999',
            'bedroom_above' => 'required|numeric|max:25',
            'bathroom_above' => 'required|numeric|max:25',
            'total_rooms' => 'required|numeric|max:50',
            'bedroom_below' => 'required|numeric|max:25',
            'bathroom_below' => 'required|numeric|max:25',
            'heat_type' => 'required|numeric|in:0,1,2',
            'ac' => 'numeric|in:0,1',
            'garage_space' => 'required|numeric|max:20',
            'porch' => 'required|numeric|max:20',
            'fire_place' => 'required|numeric|max:20',
            // 'condo_fees' => 'numeric',

        );

        public static $apiPropertyRules = array(
            'property_id' => 'required',
        );

        public static $apiTakeShortcutRules = array(
            'property_id' => 'required'
        );


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        // Get Property Details
        public static function getPropertyDetails($property_id) {

            
            $property = Property::where('id', $property_id)->first();
            

            return $property;
        }

        // Add Comp prop
        public static function addComp($comp, $property_id, $user_id, $comp_id) {
            
            
            $comp_data = json_decode($comp, true);

            $property = Property::where('id', $property_id)->first();
            $comp_property = Property::where('id', $comp_id)->first();
   
                $new_comp = Property::updateOrCreate(
                    ['id'=> $comp_id],
                    [
                        'agent_id' => $user_id,
                        'user_id' => $user_id,
                        'is_subject_prop' => 0,
                        'type' => $property->type,
                        'address_1' => isset($comp_data['address_1']) ? $comp_data['address_1'] : (empty($comp_property) ? NULL : $comp_property->address_1),
                        'address_2' => isset($comp_data['address_2']) ? $comp_data['address_2'] : (empty($comp_property) ? NULL : $comp_property->address_2),
                        'description' => isset($comp_data['description']) ? $comp_data['description'] : (empty($comp_property) ? NULL : $comp_property->description),
                        'city' => isset($comp_data['city']) ? $comp_data['city'] : (empty($comp_property) ? NULL : $comp_property->city),
                        'county' => isset($comp_data['county']) ? $comp_data['county'] : (empty($comp_property) ? NULL : $comp_property->county),
                        'state' => isset($comp_data['state']) ? $comp_data['state'] : (empty($comp_property) ? NULL : $comp_property->state),
                        'zip_code' => isset($comp_data['zip_code']) ? $comp_data['zip_code'] : (empty($comp_property) ? NULL : $comp_property->zip_code),
                        'country' => isset($comp_data['country']) ? $comp_data['country'] : (empty($comp_property) ? NULL : $comp_property->country),
                        'lat' => isset($comp_data['lat']) ? $comp_data['lat'] : (empty($comp_property) ? NULL : $comp_property->lat),
                        'lng' => isset($comp_data['lng']) ? $comp_data['lng'] : (empty($comp_property) ? NULL : $comp_property->lng),
                        'sale_price' => isset($comp_data['sale_price']) ? $comp_data['sale_price'] : (empty($comp_property) ? NULL : $comp_property->sale_price),
                        'sale_type' => isset($comp_data['sale_type']) ? $comp_data['sale_type'] : (empty($comp_property) ? NULL : $comp_property->sale_type),
                        'sqft_above' => isset($comp_data['sqft_above']) ? $comp_data['sqft_above'] : (empty($comp_property) ? 0 : $comp_property->sqft_above),
                        'sqft_below' => isset($comp_data['sqft_below']) ? $comp_data['sqft_below'] : (empty($comp_property) ? 0 : $comp_property->sqft_below),
                        'total_sqft' => isset($comp_data['total_sqft']) ? $comp_data['total_sqft'] : (empty($comp_property) ? NULL : $comp_property->total_sqft),
                        'date_sold' => isset($comp_data['date_sold']) ? $comp_data['date_sold'] : (empty($comp_property) ? NULL : $comp_property->date_sold),
                        'year_build' => isset($comp_data['year_build']) ? $comp_data['year_build'] : (empty($comp_property) ? NULL : $comp_property->year_build),
                        'bedroom_above' => isset($comp_data['bedroom_above']) ? $comp_data['bedroom_above'] : (empty($comp_property) ? 0 : $comp_property->bedroom_below),
                        'bathroom_above' => isset($comp_data['bathroom_above']) ? $comp_data['bathroom_above'] : (empty($comp_property) ? 0 : $comp_property->bathroom_above),
                        'total_rooms' => isset($comp_data['total_rooms']) ? $comp_data['total_rooms'] : (empty($comp_property) ? 0 : $comp_property->total_rooms),
                        'bedroom_below' => isset($comp_data['bedroom_below']) ? $comp_data['bedroom_below'] : (empty($comp_property) ? 0 : $comp_property->bedroom_below),
                        'bathroom_below' => isset($comp_data['bathroom_below']) ? $comp_data['bathroom_below'] : (empty($comp_property) ? 0 : $comp_property->bathroom_below),
                        'other' => isset($comp_data['other']) ? $comp_data['other'] : (empty($comp_property) ? NULL : $comp_property->other),        
                        'heat_type' => isset($comp_data['heat_type']) ? $comp_data['heat_type'] : (empty($comp_property) ? NULL : $comp_property->heat_type),
                        'ac' => isset($comp_data['ac']) ? $comp_data['ac'] : (empty($comp_property) ? 0 : $comp_property->ac),
                        'garage_space' => isset($comp_data['garage_space']) ? $comp_data['garage_space'] : (empty($comp_property) ? 0 : $comp_property->garage_space),
                        'porch' => isset($comp_data['porch']) ? $comp_data['porch'] : (empty($comp_property) ? 0 : $comp_property->porch),
                        'fire_place' => isset($comp_data['fire_place']) ? $comp_data['fire_place'] : (empty($comp_property) ? 0 : $comp_property->fire_place),
                        'mls_id' => isset($comp_data['mls_id']) ? $comp_data['mls_id'] : (empty($comp_property) ? NULL : $comp_property->mls_id),
                        'acerage' => isset($comp_data['acerage']) ? $comp_data['acerage'] : (empty($comp_property) ? NULL : $comp_property->acerage),
                        'floor' => isset($comp_data['floor']) ? $comp_data['floor'] : (empty($comp_property) ? NULL : $comp_property->floor),
                        'condo_fees' => isset($comp_data['condo_fees']) ? $comp_data['condo_fees'] : (empty($comp_property) ? 0 : $comp_property->condo_fees),
                        'unit1' => isset($comp_data['unit1']) ? $comp_data['unit1'] : (empty($comp_property) ? 0 : $comp_property->unit1),
                        'unit2' => isset($comp_data['unit2']) ? $comp_data['unit2'] : (empty($comp_property) ? 0 : $comp_property->unit2),
                        'listing_number' => isset($comp_data['listing_number']) ? $comp_data['listing_number'] : (empty($comp_property) ? 0 : $comp_property->listing_number),
    
                    ]
                );

            return $new_comp;
        }

        // Add properties
        public static function addProperties($prop_id, $type, $user_id) {

            $comp_prop = CompProperty::where('subject_prop_id', $prop_id)->where('type', 1)->first();
            $city_multiplier = Multiplier::where('type', 1)->where('property_id', $prop_id)->first();
            $property = Property::where('id', $prop_id)->first();

            $opinion = HomeyOpinion::orderBy('id', 'desc')->where([
                ['user_id', '=', $property->user_id],
                ['property_id', '=', $prop_id],
            ])->first();
            if(empty($opinion)) {
                $opinion_no = 1;
                if($property->agency_type == 3) {
                    $credits = User::where('id', $user_id)->value('credits');
                    User::where('id', $user_id)->update(['credits' => $credits + 25]);
                }
            }
            else {
                $opinion_no = $opinion->opinion_no + 1;
                Payment::orderBy('id', 'desc')->where([
                        ['user_id', $property->user_id],
                        ['property_id', $prop_id],
                        ['agent_id', NULL]
                    ])->update(['agent_id' => $user_id]);

            }

            // //new homey opinion
            // $new_opinion = new HomeyOpinion;
            // $new_opinion->user_id = $property->user_id;
            // $new_opinion->property_id = $prop_id;
            // $new_opinion->agent_id = $user_id;
            // $new_opinion->opinion_no = $opinion_no;
            // $new_opinion->comment = isset($request->comment) ? $request->comment : NULL;
            // $new_opinion->save();

            $comp1 = Property::addProp($comp_prop->comp_prop1);
            $comp2 = Property::addProp($comp_prop->comp_prop2);
            $comp3 = Property::addProp($comp_prop->comp_prop3);

            $new_prop = new CompProperty;
            $new_prop->type = $type;
            $new_prop->subject_prop_id = $prop_id;

            if($opinion != '') {
                $new_prop->homey_opinion_id = $opinion->id;
            }
            
            // $new_prop->homey_opinion_id = $new_opinion->id;
            $new_prop->comp_prop1 = $comp1->id;
            $new_prop->comp_prop2 = $comp2->id;
            $new_prop->comp_prop3 = $comp3->id;
            $new_prop->save();

            //inserting multipliers
            $multiplier = new Multiplier;
            $multiplier->type = $type;
            $multiplier->property_id = $prop_id;
            $multiplier->above_sqft = $city_multiplier->above_sqft;
            $multiplier->below_sqft = $city_multiplier->below_sqft;
            $multiplier->year_build = $city_multiplier->year_build;
            $multiplier->garage1 = $city_multiplier->garage1;
            $multiplier->garage2 = $city_multiplier->garage2;
            $multiplier->heat = $city_multiplier->heat;
            $multiplier->ac = $city_multiplier->ac;
            $multiplier->fire_place = $city_multiplier->fire_place;
            $multiplier->porch = $city_multiplier->porch;
            $multiplier->bedroom_above = $city_multiplier->bedroom_above;
            $multiplier->bedroom_below = $city_multiplier->bedroom_below;
            $multiplier->bathroom_above = $city_multiplier->bathroom_above;
            $multiplier->bathroom_below = $city_multiplier->bathroom_below;
            $multiplier->quality = $city_multiplier->quality;
            $multiplier->condition_value = $city_multiplier->condition_value;
            $multiplier->save();

            return 1;
        }

        // Add Prop
        public static function addProp($comp_prop_id) {
            $comp = Property::where('id', $comp_prop_id)->first();

            $new_comp = new Property;
            $new_comp->user_id = $comp->user_id;
            $new_comp->is_subject_prop = 0;
            $new_comp->type = $comp->type;
            $new_comp->tax_key = $comp->tax_key;
            $new_comp->address_1 = $comp->address_1;
            $new_comp->address_2 = $comp->address_2;
            $new_comp->city = $comp->city;
            $new_comp->county = $comp->county;
            $new_comp->state = $comp->state;
            $new_comp->zip_code = $comp->zip_code;
            $new_comp->country = $comp->country;
            $new_comp->lat = $comp->lat;
            $new_comp->lng = $comp->lng;
            $new_comp->description = $comp->description;
            $new_comp->location = json_encode($comp->location);
            $new_comp->sqft_above = $comp->sqft_above;
            $new_comp->sqft_below = $comp->sqft_below;
            $new_comp->year_build = $comp->year_build;
            $new_comp->garage_space = $comp->garage_space;
            $new_comp->heat_type = $comp->heat_type;
            $new_comp->ac = $comp->ac;
            $new_comp->fire_place = $comp->fire_place;
            $new_comp->porch = $comp->porch;
            $new_comp->quality = $comp->quality;
            $new_comp->conditions = $comp->conditions;
            $new_comp->bedroom_above = $comp->bedroom_above;
            $new_comp->bedroom_below = $comp->bedroom_below;
            $new_comp->bathroom_above = $comp->bathroom_above;
            $new_comp->bathroom_below = $comp->bathroom_below;
            $new_comp->total_rooms = $comp->total_rooms;
            $new_comp->basement_finish = $comp->basement_finish;
            $new_comp->total_rooms_value = $comp->total_rooms_value;
            $new_comp->basement_value = $comp->basement_value;
            $new_comp->positive_add = json_encode($comp->positive_add);
            $new_comp->negative_add = json_encode($comp->negative_add);
            $new_comp->sale_type = $comp->sale_type;
            $new_comp->date_sold = $comp->date_sold;
            $new_comp->total_sqft = $comp->total_sqft;
            $new_comp->other = $comp->other;
            $new_comp->mls_id = $comp->mls_id;
            $new_comp->acerage = $comp->acerage;
            $new_comp->acerage_value = json_encode($comp->acerage_value);
            $new_comp->sale_price = $comp->sale_price;
            $new_comp->floor = $comp->floor;
            $new_comp->condo_fees = $comp->condo_fees;
            $new_comp->condo_arr = json_encode($comp->condo_arr);
            $new_comp->condo_value = $comp->condo_value;
            $new_comp->step1 = $comp->step1;
            $new_comp->step2 = $comp->step2;
            $new_comp->step3 = $comp->step3;
            $new_comp->step4 = $comp->step4;
            $new_comp->unit1 = $comp->unit1;
            $new_comp->unit2 = $comp->unit2;
            $new_comp->detail_result = $comp->detail_result;
            $new_comp->quality_result = $comp->quality_result;
            $new_comp->location_result = $comp->location_result;
            $new_comp->room_result = $comp->room_result;
            $new_comp->save();

            return $new_comp;

        }
}
