<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{

    protected $fillable = [
        "id", "agent_id", "subject_prop_id", "compared_prop_id", "reviewed_by", "review_type", "homey_opinion_id", "detail", "quality", "location", "room", "total"
    ];

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/



    // /*--------------------------------------------------------------------------
    // ----------------------------------------------------------------------------
    //  * Validation Rules
    // ----------------------------------------------------------------------------
    // --------------------------------------------------------------------------*/

    //     /*--------------------------------------------------------------------------
    //      * Api Validations
    //     --------------------------------------------------------------------------*/

    //     public static $apiStep1Rules = array(
    //         'subject_prop_id' => 'required'
    //     );

    //     public static $apiStep2Rules = array(
    //         'subject_prop_id' => 'required',
    //         'subject_quality' => 'required',
    //         'c1_quality' => 'required',
    //         'c2_quality' => 'required',
    //         'c3_quality' => 'required',
    //         'subject_condition' => 'required',
    //         'c1_condition' => 'required',
    //         'c2_condition' => 'required',
    //         'c3_condition' => 'required'
    //     );

    //     public static $apiStep2DataRules = array(
    //         'subject_prop_id' => 'required'
    //     );

    //     public static $apiStep3DataRules = array(
    //         'subject_prop_id' => 'required'
    //     );

    //     public static $apiStep3Rules = array(
    //         'subject_prop_id' => 'required',
    //         'location_c1' => 'required',
    //         'acerage_c1' => 'required',
    //         'location_c2' => 'required',
    //         'acerage_c2' => 'required',
    //         'location_c3' => 'required',
    //         'acerage_c3' => 'required',
    //     );

    //     public static $apiStep4DataRules = array(
    //         'subject_prop_id' => 'required'
    //     );

    //     public static $apiStep4Rules = array(
    //         'subject_prop_id' => 'required',
    //         'total_rooms1' => 'required',
    //         'total_rooms2' => 'required',
    //         'total_rooms3' => 'required',
    //         'basement_finish1' => 'required',
    //         'basement_finish2' => 'required',
    //         'basement_finish3' => 'required'
    //     );

    //     public static $apiReviewPageRules = array(
    //         'subject_prop_id' => 'required'
    //     );

    //     public static $apiAgentReviewRules = array(
    //         'subject_prop_id' => 'required',
    //         'c1' => 'required',
    //         'c2' => 'required', 
    //         'c3' => 'required'
    //     );

    //     public static $apiEditAgentReviewRules = array(
    //         'subject_prop_id' => 'required',
    //         'opinion_id' => 'required',
    //         'c1' => 'required',
    //         'c2' => 'required', 
    //         'c3' => 'required',
    //         'c1_id' => 'required',
    //         'c2_id' => 'required',
    //         'c3_id' => 'required',
    //     );

    //     public static $apiCashFlowRules = array(
    //         'rate' => 'required',
    //         'loan_term' => 'required',
    //         'purchase_price' => 'required',
    //         'down_payment_rate' => 'required',
    //         'property_id' => 'required',
    //         'other_expenses' => 'required',
    //         'unit1' => 'required',
    //         'unit2' => 'required'
    //     );

    //     public static $apiCondoRules = array(
    //         'condo1' => 'required',
    //         'condo2' => 'required',
    //         'condo3' => 'required'
    //     );

    //     public static $someRules = array(
    //         'compared_prop_id' => 'required',
    //         'detail' => 'required',
    //         'quality' => 'required',
    //         'location' => 'required',
    //         'room' => 'required'
    //     );

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    // public static function getMultipliers($request, $prop_id) {

    //     $default = Multiplier::where('property_id', $prop_id)->first();
    //     //sqft above
    //     if(empty($request->above_sqft)) {
    //         $above_sqft = $default->above_sqft;
    //     }
    //     else {
    //         $above_sqft = $request->above_sqft;
    //     }

    //     //sqft below
    //     if(empty($request->below_sqft)) {
    //         $below_sqft = $default->below_sqft;
    //     }
    //     else {
    //         $below_sqft = $request->below_sqft;
    //     }

    //     //year build
    //     if(empty($request->year_build)) {
    //         $year_build = $default->year_build;
    //     }
    //     else {
    //         $year_build = $request->year_build;
    //     }

    //     //garage1
    //     if(empty($request->garage1)) {
    //         $garage1 = $default->garage1;
    //     }
    //     else {
    //         $garage1 = $request->garage1;
    //     }

    //     //garage2
    //     if(empty($request->garage2)) {
    //         $garage2 = $default->garage2;
    //     }
    //     else {
    //         $garage2 = $request->garage2;
    //     }

    //     //heat
    //     if(empty($request->heat)) {
    //         $heat = $default->heat;
    //     }
    //     else {
    //         $heat = $request->heat;
    //     }
        
    //     //ac
    //     if(empty($request->ac)) {
    //         $ac = $default->ac;
    //     }
    //     else {
    //         $ac = $request->ac;
    //     }
        
    //     //fire place
    //     if(empty($request->fire_place)) {
    //         $fire_place = $default->fire_place;
    //     }
    //     else {
    //         $fire_place = $request->fire_place;
    //     }
        
    //     //porch
    //     if(empty($request->porch)) {
    //         $porch = $default->porch;
    //     }
    //     else {
    //         $porch = $request->porch;
    //     }
        
    //     //bedroom above
    //     if(empty($request->bedroom_above)) {
    //         $bedroom_above = $default->bedroom_above;
    //     }
    //     else {
    //         $bedroom_above = $request->bedroom_above;
    //     }

    //     //bedroom below
    //     if(empty($request->bedroom_below)) {
    //         $bedroom_below = $default->bedroom_below;
    //     }
    //     else {
    //         $bedroom_below = $request->bedroom_below;
    //     }

    //     //bathroom above
    //     if(empty($request->bathroom_above)) {
    //         $bathroom_above = $default->bathroom_above;
    //     }
    //     else {
    //         $bathroom_above = $request->bathroom_above;
    //     }

    //     //bathroom below
    //     if(empty($request->bathroom_below)) {
    //         $bathroom_below = $default->bathroom_below;
    //     }
    //     else {
    //         $bathroom_below = $request->bathroom_below;
    //     }

    //     Multiplier::where('property_id', $prop_id)->update([
    //         "above_sqft" => $above_sqft,
    //         "below_sqft" => $below_sqft,
    //         "year_build" => $year_build,
    //         "garage1" => $garage1,
    //         "garage2" => $garage2,
    //         "heat" => $heat,
    //         "ac" => $ac,
    //         "fire_place" =>$fire_place,
    //         "porch" => $porch,
    //         "bedroom_above" => $bedroom_above,
    //         "bedroom_below" => $bedroom_below,
    //         "bathroom_above" => $bathroom_above,
    //         "bathroom_below" => $bathroom_below
    //     ]);

    // }

    public static function addAgentReview($c, $user_id, $sub_id, $id, $c_id) {

        // $review_data = json_decode($c);
        // $data = Adjustment::where('id', $c_id)->first();

        // $detail = isset($review_data->detail) ? $review_data->detail : $data->detail;
        // $quality = isset($review_data->quality) ? $review_data->quality : $data->quality;
        // $location = isset($review_data->location) ? $review_data->location : $data->location;
        // $room = isset($review_data->room) ? $review_data->room : $data->room;

        // if(empty($data)) {
        //     $total = ($review_data->detail + $review_data->quality + $review_data->location + $review_data->room);
        // }
        // else {
        //     $total = ($detail + $quality + $location + $room);
        // }

        // $new = Adjustment::updateOrCreate(
        //     ['id'=> $c_id],
        //     [
        //         'agent_id' => $user_id,
        //         'subject_prop_id' => $sub_id,
        //         'compared_prop_id' => isset($review_data->compared_prop_id) ? $review_data->compared_prop_id : $data->compared_prop_id,
        //         'reviewed_by' => $user_id,
        //         'review_type' => 3,
        //         'homey_opinion_id' => $id,
        //         'detail' => $detail,
        //         'quality' => $quality,
        //         'location' => $location,
        //         'room' => $room,
        //         'total' => $total,
                
        //     ]
        // );


        // return $new;

    }

}
