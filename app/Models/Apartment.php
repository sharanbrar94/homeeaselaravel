<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        public function getBuildingRepairsAttribute($value) {
            $repair_arr = [];
            if ($value) {          
                $repair_arr = json_decode($value);   
                foreach($repair_arr as $key => $value2) {
                }  
                return $repair_arr;             
            }
            else {
                return $repair_arr; 
            }
        }

        public function getRevenueDetailsAttribute($value) {
            $revenue_arr = [];
            if ($value) {          
                $revenue_arr = json_decode($value);   
                foreach($revenue_arr as $key => $value2) {
                }  
                return $revenue_arr;             
            }
            else {
                return $revenue_arr; 
            }
        }

        public function getExpenseDetailsAttribute($value) {
            $expense_arr = [];
            if ($value) {          
                $expense_arr = json_decode($value);   
                foreach($expense_arr as $key => $value2) {
                }  
                return $expense_arr;             
            }
            else {
                return $expense_arr; 
            }
        }

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Validation Rules
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------
         * Api Validations
        --------------------------------------------------------------------------*/

        public static $apiApartmentCalculationRules = array(
            'apartment_id' => 'required',
            'cap_rate' => 'required',
            'building_repairs' => 'required',
            'revenue_details' => 'required',
            'revenue_laundry' => 'required',
            'revenue_parking' => 'required',
            'expense_details' => 'required'
        );

        public static $apiApartmentRules = array(
            'apartment_id' => 'required',
            'yearly_revenue' => 'required',
            'percent' => 'required',
            'cap_rate' => 'required'
        );

        public static $apiApartmentDataRules = array(
            'apartment_id' => 'required',
        );
}
