<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Library\CommonFunction;

class Chat extends Model
{
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Relations
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/
    
    public function sentBy() {
        return $this->belongsTo('App\Models\User', 'sent_by', 'id');
    }

    public function sentTo() {
        return $this->belongsTo('App\Models\User', 'sent_to', 'id');
    }


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        public function getFileAttribute($value) {
            $files_json_arr = [];
            if ($value) {          
                $files_json_arr = json_decode($value); 
                
                foreach($files_json_arr as $key => $value2) {
                   // print_r($value2); 
                    //print_r($value2);
                    $value2 = url(CommonFunction::FILE_PATH.$value2);
                }  
                return $files_json_arr;             
            }
            else {
                return $files_json_arr; 
            }
        }


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Validation Rules
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/
         
        /*--------------------------------------------------------------------------
         * Api Validations
        --------------------------------------------------------------------------*/
    
        public static $apiSendMessageRules = array(                                 
            'property_id' => 'required',
            'msg_type' => 'required|in:0,1'                
        );

        public static $apiSendByAdminRules = array(
            'sent_to' => 'required',
        );

        public static $apiGetRecentMessagesRules = array(                                 
            'property_id' => 'required'               
        );

        public static $apiPreviousMessagesRules = array(                                 
            'sent_to' => 'required',
            'property_id' => 'required',
            'last_msg_id' => 'required'               
        );

        public static $apiDeleteConversationsRules = array(                                 
            'property_id' => 'required',
            'sent_to' => 'required'               
        );

        public static $apiChatPaymentRules = array(
            'user_id_1' => 'required',
            'property_id' => 'required',
            // 'token' => 'required'
        );

        public static $apiChatRequestRules = array(
            'property_id' => 'required',
            'agent_id' => 'required'
        );


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/
        
        // Get Sent Message
        public static function getSentMessage($id, $user_id, $sent_to, $property_id, $zone) {

            $current_time = Carbon::now();

            $chat = Chat::where('id', $id)->first();

            $chat->time_since =  Carbon::parse($current_time)->timezone('-06:00')->format('Y-m-d h:i:s A');
            // ->format('h:i A')
            if($user_id != NULL) {
                $chat->sender_name = User::where('id', $user_id)->value('first_name');
                $chat->image = User::where('id', $user_id)->value('image');             
            }
            else {
                $chat->sender_name = "Admin";
                $chat->image = url(CommonFunction::PICTURE_PATH_D).'/default.png';
            }

            if($sent_to != NULL) {
                $chat->receiver_name = User::where('id', $sent_to)->value('first_name');
            }
            else {
                $chat->receiver_name = "Admin";
            }
     
            return $chat;                        

        }

        // Get Recent Message
        public static function getRecentMessages($user_id, $sent_to, $property_id, $zone) {

            Chat::where('sent_by', $sent_to)->where('sent_to', $user_id)->where('property_id', $property_id)->update(['is_read' => '1']);
            $img_base_path = url(CommonFunction::PICTURE_PATH).'/';
            $img_base_path1 = url(CommonFunction::PICTURE_PATH_D).'/default.png';
            $name = 'Admin';
        
            $chat = Chat::selectRaw("*, 
            IFNULL((SELECT `first_name` from `users` where `id` = `sent_by`), '$name') AS `sender_name`, 
            IFNULL((SELECT `first_name` from `users` where `id` = `sent_to`), '$name') AS `receiver_name`, 
            CASE 
            WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) = 0 THEN (SELECT '$img_base_path1')
            ELSE 
                CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_by`)) 
            END
            AS `image`, (DATE_FORMAT(convert_tz(`created_at`,@@session.time_zone,'-06:00') ,'%Y-%m-%d %h:%i:%s %p')) AS `time_since` ")
            ->orderBy('id', 'desc')->where([
                ['sent_by', $user_id],
                ['sent_to', $sent_to],
                ['property_id', $property_id]
            ])->orWhere([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id]
            ])->limit(40)->get();

            $files = Chat::where([
                ['sent_by', $user_id],
                ['sent_to', $sent_to],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->orWhere([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->pluck('file');

            $data[] = [
                'files' => $files,
                'messages' => $chat
            ];
        
            return $data;                        
        
        }

        //Get Previous Message
        public static function getPreviousMessages($user_id, $sent_to, $property_id, $last_msg_id, $zone) {

            Chat::where('sent_by', $sent_to)->where('sent_to', $user_id)->where('property_id', $property_id)->update(['is_read' => '1']);
            $img_base_path = url(CommonFunction::PICTURE_PATH).'/';
            $img_base_path1 = url(CommonFunction::PICTURE_PATH_D).'/default.png';
            $name = 'Admin';
            
            $current_time = Carbon::now();
        
            $chat = Chat::selectRaw("*,             
            IFNULL((SELECT `first_name` from `users` where `id` = `sent_by`), '$name') AS `sender_name`, 
            IFNULL((SELECT `first_name` from `users` where `id` = `sent_to`), '$name') AS `receiver_name`, 
            CASE 
            WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) = 0 THEN (SELECT '$img_base_path1')
            ELSE 
                CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_by`)) 
            END
            AS `image`,  (DATE_FORMAT(convert_tz(`created_at`,@@session.time_zone,'-06:00') ,'%Y-%m-%d %h:%i:%s %p')) AS `time_since` ")
            ->orderBy('id', 'desc')->where([
                ['sent_by', $user_id],
                ['sent_to', $sent_to],
                ['property_id', $property_id],
                ['id', '<', $last_msg_id]
            ])->orWhere([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id],
                ['id', '<', $last_msg_id]
            ])->limit(40)->get();
        
            $files = Chat::where([
                ['sent_by', $user_id],
                ['sent_to', $sent_to],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->orWhere([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->pluck('file');

            $data[] = [
                'files' => $files,
                'messages' => $chat
            ];
        
            return $data;                       
        
        }

        // Get Message
        public static function getMessages($user_id, $sent_to, $property_id, $last_msg_id, $zone) {

            Chat::where('sent_by', $sent_to)->where('sent_to', $user_id)->where('property_id', $property_id)->update(['is_read' => '1']);
            
            $current_time = Carbon::now();
        
            $chat = Chat::selectRaw("*, (SELECT `first_name` from `users` where `id` = `sent_by`) AS `sender_name`, (SELECT `first_name` from `users` where `id` = `sent_to`) AS `receiver_name`, (DATE_FORMAT(convert_tz(`created_at`,@@session.time_zone,'-06:00') ,'%Y-%m-%d %h:%i:%s %p')) AS `time_since` ")
            ->orderBy('id', 'desc')->where([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id],
                ['id', '>', $last_msg_id]
            ])->limit(40)->get();
        
            $files = Chat::where([
                ['sent_by', $user_id],
                ['sent_to', $sent_to],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->orWhere([
                ['sent_by', $sent_to],
                ['sent_to', $user_id],
                ['property_id', $property_id],
                ['file', '<>', NULL]
            ])->pluck('file');

            $data[] = [
                'files' => $files,
                'messages' => $chat
            ];
        
            return $data;                         
        
        }


}
