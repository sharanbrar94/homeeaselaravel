<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Admin extends Model
{
    /******************************************************************
     * ****************************************************************
     * Common Functions
     * ****************************************************************
    ******************************************************************/
        public static function generateAndSaveUserToken($user_id) {
            $token = str_random(30);
            Admin::where('id', $user_id)->update(['access_token' => $token]);
            return $token;
        }

        // Admin Dashboard
        public static function adminDashboard() {
            $current_time = Carbon::now();
        
            $dashboard_data = DB::select("
                SELECT `id`,`admin_name`,`email`,
                (SELECT count(`id`) FROM `users` WHERE `user_type` = 1) AS `total_users`,
                (SELECT count(`id`) FROM `users` WHERE `user_type` = 2) AS `total_agents`,
                (SELECT count(`id`) FROM `properties`) AS `total_properties`,
                (SELECT count(`id`) FROM `cities`) AS total_cities,
                (SELECT count(`id`) FROM `counties`) AS total_counties
                FROM `admins`
                LIMIT 1
            ");
    
            return $dashboard_data;
        }
}