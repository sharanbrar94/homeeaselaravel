<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompProperty extends Model
{
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Validation Rules
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------
         * Api Validations
        --------------------------------------------------------------------------*/

        public static $apiStep1Rules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiStep2Rules = array(
            'subject_prop_id' => 'required',
            'subject_quality' => 'required',
            'c1_quality' => 'required',
            'c2_quality' => 'required',
            'c3_quality' => 'required',
            'subject_condition' => 'required',
            'c1_condition' => 'required',
            'c2_condition' => 'required',
            'c3_condition' => 'required'
        );

        public static $apiStep2DataRules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiStep3DataRules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiStep3Rules = array(
            'subject_prop_id' => 'required',
            'location_c1' => 'required',
            'acerage_c1' => 'required',
            'location_c2' => 'required',
            'acerage_c2' => 'required',
            'location_c3' => 'required',
            'acerage_c3' => 'required',
        );

        public static $apiStep4DataRules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiStep4Rules = array(
            'subject_prop_id' => 'required',
            'total_rooms1' => 'required',
            'total_rooms2' => 'required',
            'total_rooms3' => 'required',
            'basement_finish1' => 'required',
            'basement_finish2' => 'required',
            'basement_finish3' => 'required'
        );

        public static $apiReviewPageRules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiAgentReviewRules = array(
            'subject_prop_id' => 'required',
            'c1' => 'required',
            'c2' => 'required', 
            'c3' => 'required'
        );

        public static $apiEditAgentReviewRules = array(
            'subject_prop_id' => 'required',
            'opinion_id' => 'required',
            'c1' => 'required',
            'c2' => 'required', 
            'c3' => 'required',
            'c1_id' => 'required',
            'c2_id' => 'required',
            'c3_id' => 'required',
        );

        public static $apiCashFlowRules = array(
            'rate' => 'required',
            'loan_term' => 'required',
            'purchase_price' => 'required',
            'down_payment_rate' => 'required',
            'property_id' => 'required',
            'other_expenses' => 'required',
            'unit1' => 'required',
            'unit2' => 'required'
        );

        public static $apiCondoRules = array(
            'condo1' => 'required',
            'condo2' => 'required',
            'condo3' => 'required'
        );

        public static $someRules = array(
            'compared_prop_id' => 'required',
            'detail' => 'required',
            'quality' => 'required',
            'location' => 'required',
            'room' => 'required'
        );

        
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Function
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        // Add Prop
        public static function getReview($user_id, $prop_id, $type, $sale_price, $prop_user, $review_type) {
            $response = NULL;
            if($user_id == $prop_user) {
                $comp_properties = CompProperty::where('type', $type)->where('is_finished', 1)->where('subject_prop_id', $prop_id)->first();
            }
            elseif($user_id == 0) {
                if($review_type == 1) {
                    $comp_properties = CompProperty::where('type', $type)->where('is_finished', 1)->where('subject_prop_id', $prop_id)->first();
                }
                elseif($review_type == 2) {
                    $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $prop_id)->first();
                }
                elseif($review_type == 3) {
                    $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $prop_id)->first();
                }

            }
            else {
                $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $prop_id)->first();
            }

            if(empty($comp_properties)) {
                return $response;
            }

            if($comp_properties->homey_opinion_id != NULL) {
                $comment = HomeyOpinion::where('id', $comp_properties->homey_opinion_id)->value('comment');
                $homey_opinion_id = $comp_properties->homey_opinion_id;
            }
            else {
                $comment = "";
                $homey_opinion_id = null;
            }

            $c1 = Property::select('id', 'type', 'address_1', 'address_2', 'sale_price', 'city', 'county', 'zip_code', 'country', 'detail_result', 'location_result', 'room_result', 'quality_result', 'unit1', 'unit2')->where('id', $comp_properties->comp_prop1)->first();
            $c2 = Property::select('id', 'type', 'address_1', 'address_2', 'sale_price', 'city', 'county', 'zip_code', 'country', 'detail_result', 'location_result', 'room_result', 'quality_result', 'unit1', 'unit2')->where('id', $comp_properties->comp_prop2)->first();
            $c3 = Property::select('id', 'type', 'address_1', 'address_2', 'sale_price', 'city', 'county', 'zip_code', 'country', 'detail_result', 'location_result', 'room_result', 'quality_result', 'unit1', 'unit2')->where('id', $comp_properties->comp_prop3)->first();

            
            $c1->total = $c1->detail_result + $c1->location_result + $c1->room_result + $c1->quality_result + $c1->sale_price;
            //$c1->total = $c1->detail_result + $c1->quality_c1 + $c1->condition_c1 ;
            
            $c2->total = $c2->detail_result + $c2->location_result + $c2->room_result + $c2->quality_result + $c2->sale_price;
            //$c2->total = $c2->detail_result + $c2->quality_c2 + $c2->condition_c2;
            
            $c3->total = $c3->detail_result + $c3->location_result + $c3->room_result + $c3->quality_result + $c3->sale_price;
            //$c3->total = $c3->detail_result + $c3->quality_c3 + $c3->condition_c3;

            //$user->running_c1 = ($user->c1->detail_result + $user->c1->quality_c1 + $user->c1->condition_c1);
            //$user->running_c2 = ($user->c2->detail_result + $user->c2->quality_c2 + $user->c2->condition_c2);
            //$user->running_c3 = ($user->c3->detail_result + $user->c3->quality_c3 + $user->c3->condition_c3);
            //$user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);

            //return json response
        
            

            // $c1->quality_result = $c1->detail_result + $c1->quality_result;
            // $c1->location_result = $c1->quality_result+ $c1->location_result;
            // $c1->room_result = $c1->location_result+ $c1->room_result;

            // $c2->quality_result = $c2->detail_result + $c2->quality_result;
            // $c2->location_result = $c2->quality_result+ $c2->location_result;
            // $c2->room_result = $c2->location_result+ $c2->room_result;

            // $c3->quality_result = $c3->detail_result + $c3->quality_result;
            // $c3->location_result = $c3->quality_result+ $c3->location_result;
            // $c3->room_result =$c3->location_result+ $c3->room_result;

            $sale_avg = round(($c1->sale_price + $c2->sale_price + $c3->sale_price)/3, 2);
            $detail_avg = round(($c1->detail_result + $c2->detail_result + $c3->detail_result)/3, 2);
            $quality_avg = round(($c1->quality_result + $c2->quality_result + $c3->quality_result)/3, 2);
            $location_avg = round(($c1->location_result + $c2->location_result + $c3->location_result)/3, 2);
            $room_avg = round(($c1->room_result + $c2->room_result + $c3->room_result)/3, 2);
            $total_avg = round((($c1->total + $c2->total + $c3->total)/3), 2);
        
            $total = array($c1->total,$c2->total,$c3->total);
            sort($total);
        
            $total_median = round($total[1], 2);
        
            $avg_overpriced = round(($sale_price - $total_avg),2);
            $median_overpriced = round(($sale_price - $total_median), 2);

            $response = [
                'c1' => $c1,
                'c2' => $c2,
                'c3' => $c3,
                'sale_avg' => $sale_avg,
                'detail_avg' => $detail_avg,
                'quality_avg' => $quality_avg,
                'location_avg' => $location_avg,
                'room_avg' => $room_avg,
                "total_avg" => $total_avg,
                "total_median" => $total_median,
                "avg_overpriced" => $avg_overpriced,
                "median_overpriced" => $median_overpriced,
                'comment' => $comment,
                'comment_id' => $homey_opinion_id
            ];

            //print_r($response);die;

            return $response;

        }
}
