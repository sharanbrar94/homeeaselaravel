<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Multiplier extends Model
{
        /*--------------------------------------------------------------------------
        ----------------------------------------------------------------------------
        * Common Functions
        ----------------------------------------------------------------------------
        --------------------------------------------------------------------------*/

        public static function getMultipliers($request, $prop_id, $type) {

            $default = Multiplier::where('type', $type)->where('property_id', $prop_id)->first();
            //sqft above
            if(empty($request->above_sqft)) {
                $above_sqft = $default->above_sqft;
            }
            else {
                $above_sqft = $request->above_sqft;
            }

            //sqft below
            if(empty($request->below_sqft)) {
                $below_sqft = $default->below_sqft;
            }
            else {
                $below_sqft = $request->below_sqft;
            }

            //year build
            if(empty($request->year_build)) {
                $year_build = $default->year_build;
            }
            else {
                $year_build = $request->year_build;
            }

            //garage1
            if(empty($request->garage1)) {
                $garage1 = $default->garage1;
            }
            else {
                $garage1 = $request->garage1;
            }

            //garage2
            if(empty($request->garage2)) {
                $garage2 = $default->garage2;
            }
            else {
                $garage2 = $request->garage2;
            }

            //heat
            if(empty($request->heat)) {
                $heat = $default->heat;
            }
            else {
                $heat = $request->heat;
            }
            
            //ac
            if(empty($request->ac)) {
                $ac = $default->ac;
            }
            else {
                $ac = $request->ac;
            }
            
            //fire place
            if(empty($request->fire_place)) {
                $fire_place = $default->fire_place;
            }
            else {
                $fire_place = $request->fire_place;
            }
            
            //porch
            if(empty($request->porch)) {
                $porch = $default->porch;
            }
            else {
                $porch = $request->porch;
            }
            
            //bedroom above
            if(empty($request->bedroom_above)) {
                $bedroom_above = $default->bedroom_above;
            }
            else {
                $bedroom_above = $request->bedroom_above;
            }

            //bedroom below
            if(empty($request->bedroom_below)) {
                $bedroom_below = $default->bedroom_below;
            }
            else {
                $bedroom_below = $request->bedroom_below;
            }

            //bathroom above
            if(empty($request->bathroom_above)) {
                $bathroom_above = $default->bathroom_above;
            }
            else {
                $bathroom_above = $request->bathroom_above;
            }

            //bathroom below
            if(empty($request->bathroom_below)) {
                $bathroom_below = $default->bathroom_below;
            }
            else {
                $bathroom_below = $request->bathroom_below;
            }

            //quality
            if(empty($request->quality)) {
                $quality = $default->quality;
            }
            else {
                $quality = $request->quality;
            }

            //condition value
            if(empty($request->condition_value)) {
                $condition_value = $default->condition_value;
            }
            else {
                $condition_value = $request->condition_value;
            }

            Multiplier::where('type', $type)->where('property_id', $prop_id)->update([
                "above_sqft" => $above_sqft,
                "below_sqft" => $below_sqft,
                "year_build" => $year_build,
                "garage1" => $garage1,
                "garage2" => $garage2,
                "heat" => $heat,
                "ac" => $ac,
                "fire_place" =>$fire_place,
                "porch" => $porch,
                "bedroom_above" => $bedroom_above,
                "bedroom_below" => $bedroom_below,
                "bathroom_above" => $bathroom_above,
                "bathroom_below" => $bathroom_below,
                "quality" => $quality,
                "condition_value" => $condition_value
            ]);

        }
}
