<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Library\CommonFunction;

class User extends Model
{


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Relations
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    public function userDevices() {
        return $this->hasMany('App\Models\UserDevice', 'user_id', 'id');
    }

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/


    public function getPreferredCitiesAttribute($value) {
        $cities_arr = [];
        if ($value) {          
            $cities_arr = json_decode($value);   
            foreach($cities_arr as $key => $value2) {
            }  
            return $cities_arr;             
        }
        else {
            return $cities_arr; 
        }
    }

    public function getPreferredCountiesAttribute($value) {
        $countries_arr = [];
        if ($value) {          
            $countries_arr = json_decode($value);   
            foreach($countries_arr as $key => $value2) {
            }  
            return $countries_arr;             
        }
        else {
            return $countries_arr; 
        }
    }

    public function getPreferredStatesAttribute($value) {
        $countries_arr = [];
        if ($value) {          
            $countries_arr = json_decode($value);   
            foreach($countries_arr as $key => $value2) {
            }  
            return $countries_arr;             
        }
        else {
            return $countries_arr; 
        }
    }

    public function getCitiesAttribute($value) {
        $cities = [];
        if ($value) {          
            $cities = json_decode($value);   
            foreach($cities as $key => $value2) {
            }  
            return $cities;             
        }
        else {
            return $cities; 
        }
    }

    public function getEstateTypeAttribute($value) {
        $estate_arr = [];
        if ($value) {          
            $estate_arr = json_decode($value);   
            foreach($estate_arr as $key => $value2) {
            }  
            return $estate_arr;             
        }
        else {
            return $estate_arr; 
        }
    }

    public function getImageAttribute($value){
        if ($value) {
            return url(CommonFunction::PICTURE_PATH.$value);
        } else {
            return url(CommonFunction::PICTURE_PATH_D."default.png");
        }
    }

    public function getRemovedPropertiesAttribute($value) {
        $properties_arr = [];
        if ($value) {          
            $properties_arr = json_decode($value);   
            foreach($properties_arr as $key => $value2) {
            }  
            return $properties_arr;             
        }
        else {
            return $properties_arr; 
        }
    }



    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Validation Rules
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------
         * Api Validations
        --------------------------------------------------------------------------*/

        public static $addUserDeviceRules = array(
            'device_type' => 'required'
        );

        public static $apiAgentSignUpRules = array(
            'first_name' => 'required',
            'mls_id' => 'required',
            'licence_type' => 'required',
            'email' => 'required|email|Unique:users',
            'password' => 'required|min:6|confirmed',
            'device_type' => 'required',
        );

        public static $apiUserSignUpRules = array(
            'email' => 'required|email|Unique:users',
            'password' => 'required|min:6|confirmed',
            'have_agent' => 'required',
            'device_type' => 'required',
        );

        public static $apiUserAgentRules = array(                                 
            'agent_email' => 'required'
        );

        public static $apiLoginRules = array(                                 
            'email' => 'required|email',
            'password' => 'required|min:6' , 
            'device_type' => 'required',              
        );

        public static $apiChangePasswordRules = array(                                              
            'password' => 'required|min:6',
        ); 

        public static $apiForgotPasswordRules = array(                                 
            'email' => 'required|email'
        );
        
        public static $apiResetPasswordRules = array(                                 
            'token' => 'required',
            'password' => 'required|min:6|confirmed',                
        );

        public static $apiStartTrialRules = array(
            'first_name' => 'required',
            'last_name'=>'required',
            'email' => 'required|email',
            //'address_1' => 'required',
            //'city' => 'required',
            //'county' => 'required',
            //'state' => 'required',
            // 'zip_code' => 'required',
            // 'country' => 'required',
            //'type' => 'required',
            // 'agency_type' => 'required'
        );

        public static $apiAgencyTypeRules = array(
            //'agency_type' => 'required'
        );

        public static $apiCheckUserRules = array(
            'email' => 'required|email'
        );

        public static $apiUserCompdRules = array(
            'subject_prop_id' => 'required'
        );

        public static $apiAddSubscriptionRules = array(
            'token' => 'required',
        );

        public static $apiInviteUserRules = array(
            'email' => 'required|email',
        );



    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        // Get User Details
        public static function getUserDetails($user_id, $fcm_id=0) {

            
            $user_details = User::where('id', $user_id)->first();
            
            $user_details->user_devices = $user_details->userDevices()->where('fcm_id', $fcm_id)->first();
            if($user_details->user_type == 2) {
                // $cities = ExtraCity::where('user_id', $user_id)->select('state as state_name', 'county as county_name')->groupby('state','county')->get();
                // foreach($user_details->cities as $key => $value) {
                //     $value->city_names = ExtraCity::where('user_id', $user_id)->where('county', $value->county_name)->where('state', $value->state_name)->pluck('city');
                // }


                // $user_details->cities = $cities;

                // $user_details->states = 
                // $user_details->other_cities = ExtraCity::where('user_id', $user_id)->where('is_city', 1)->get();
                // $user_details->other_counties = ExtraCity::where('user_id', $user_id)->where('is_city', 2)->get();
                // $user_details->states = State::
                $states = [];
                foreach($user_details->cities as $key => $value) {
                    $states[] = $value->state_name;
                }
                $user_details->states = array_unique($states);
                $user_details->invitations = UserInvite::where('agent_id', $user_details->id)->get();
                $user_details->files = Upload::where('user_id', $user_id)->get();
                foreach($user_details->files as $key => $value) {
                    $value->city_name = City::where('id', $value->city_id)->value('name');
                    $value->county_name = County::where('id', $value->county_id)->value('name');
                    $value->state_name = State::where('id', $value->state_id)->value('name');
                }
            }


            return $user_details;
        }
}
