<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model
{

    protected $fillable = [
        "id", "property_id", "unit1", "unit2", "expenses", "cash_flow", "loan_term", "rate", "monthly_payment", "first_payment_principle", "income", "down_payment", "return_investment", "purchase_price", "down_payment_rate", "total", "sum"
    ];

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
    * Getters Setters
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    public function getExpensesAttribute($value) {
        $expense_arr = [];
        if ($value) {          
            $expense_arr = json_decode($value);   
            foreach($expense_arr as $key => $value2) {
            }  
            return $expense_arr;             
        }
        else {
            return $expense_arr; 
        }
    }
}
