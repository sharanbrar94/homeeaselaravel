<?php

namespace App\Library;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Admin;
use App\Library\CommonFunction;
use App\Models\UserDevice;
use Stripe;
use App\Models\UserStripe;
use AWS;

class CommonFunction {

    const PICTURE_PATH_D = "uploads/";
    const PICTURE_PATH = "photos/thumb/";

    const FILE_PATH_D = "chat-files/";
                      //https://s3-us-west-1.amazonaws.com/homeease-test/chat-files
    const FILE_PATH = "https://s3-us-west-1.amazonaws.com/homeease-test/chat-files/";


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * CommonFunctions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

    //Generating Tokens
    public static function generateAccessToken(){
        return $access_token = str_random(100);
    }        


    // Validating Token
    public static function validateAccessToken($access_token) {            
        return $user_device_data = UserDevice::where('access_token', $access_token)->first();            
    }

    // Validating Token
    public static function validateAdminAccessToken($access_token) {            
        return $user_device_data = Admin::where('access_token', $access_token)->first();            
    }

    // Add User Device
    public static function addEditUserDevice($request, $user_id) {
        
        //Check Validation	        
        //$validation = Validator::make($request->all(), User::$addUserDeviceRules);
        
        //if($validation->fails())
            //return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $access_token = CommonFunction::generateAccessToken();
        $current_time = Carbon::now();
       
        // Check if Fcm Id provided
        if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
            $fcm_id = $request->fcm_id;
        }   
        else {
            $fcm_id = NULL;
        }

        // Get User Device Information
        $user_device_data = UserDevice::where('user_id', $user_id)->where('fcm_id', $fcm_id)->first();
        
        if(!empty($user_device_data)) {
            // Update Access Token          
            UserDevice::where('id', $user_device_data->id)->update(['access_token' => $access_token]);                        
        }
        else {
            // Inserting in User Devices
            $new_user_devices = new UserDevice;
            $new_user_devices->user_id = $user_id;
            $new_user_devices->access_token = $access_token;
            $new_user_devices->fcm_id = $fcm_id;
            $new_user_devices->device_type = $request->device_type;
            $new_user_devices->app_version = isset($request->app_version) ? $request->app_version : 1;
            $new_user_devices->ip = request()->ip();
            $new_user_devices->last_login = $current_time;            
            $new_user_devices->save();                
        }

        return "add_edit_user_device_success";
    }  

    // // Upload Image
    // public static function uploadImage($image) {
    //     if($image->isValid()) {            
    //         //get extension of file
    //         $ext = $image->getClientOriginalExtension();
    //         //directory to store images
    //         $dir = 'uploads';
    //         // change filename to random name
    //         $filename = substr(time(), 0, 15).str_random(30) . ".{$ext}";
    //         // move uploaded file to temp. directory
    //         $upload_success = $image->move($dir, $filename);
    //         $img = $upload_success ? $filename : '';
    //     }
    //     return $img;   
    // }        

    public static function uploadImage($image) {
        if($image->isValid()) {
            //get extension of file
            $ext = $image->getClientOriginalExtension();
            // change filename to random name
            $filename = substr(time(), 0, 15).str_random(30) . ".{$ext}";


            $s3 = AWS::createClient('s3');

            $s3->putObject(array(
                'Bucket'     => 'homeease-test',
                'Key'        => 'uploads/'.$filename,
                'SourceFile' => $image->getPathname(),
                'ContentType' => 'images/jpeg',
                'ACL' => 'public-read'
            ));
            return $filename;            
        }
        else
            return 0;            
    }

    // Extract Image Name from URL
    public static function extractImageName($image_path) {
        if (strpos($image_path, 'photos/thumb') !== false) {                
            $img_path_arr = explode('/', $image_path);                                
            return $img_path_arr_end = end($img_path_arr);
        }
        else 
            return $image_path;
    }


    public static function uploadFile($file) {
        if($file->isValid()) {
            //get extension of file
            $ext = $file->getClientOriginalExtension();
            // dd($file);
            // change filename to random name
            $name = $file->getClientOriginalName();
            $name = str_replace(' ', '', $name);
            $filename = substr(time(), 0, 15).str_random(30) . ".{$ext}";
            $original_filename = "{$name}";

            $s3 = AWS::createClient('s3');
            $s3->putObject(array(
                'Bucket'     => 'homeease-test',
                'Key'        => 'chat-files/'.$filename,
                'SourceFile' => $file->getPathname(),
                'ContentType' => 'files/txt',
                'ACL' => 'public-read'
            ));

            $response = [
                'filename' => $filename,
                'original_filename' => $original_filename
            ];
            return $response;            
        }
        else
            return 0;            
    }

    // Extract Image Name from URL
    public static function extractFileName($file_path) {
        if (strpos($file_path, 'https://s3-us-west-1.amazonaws.com/homeease-test/chat-files') !== false) {                
            $file_path_arr = explode('/', $file_path);                                
            return $file_path_arr_end = end($file_path_arr);
        }
        else 
            return $file_path;
    }

    /*============Make stripe payment for start pricing============*/

    public static function stripePayment($request,$amount)
    {
        \Stripe\Stripe::setApiKey("sk_test_8N9H8KPT7YIZ3vRCOPbWkIlc");
        $token = $request->token;
        
        $user_id = $request->user_data->user_id;            
        $user_stripe_data = UserStripe::where('user_id', $user_id)->first();
        if(!empty($user_stripe_data)) {                                
            $customer_id = $user_stripe_data->customer_id;
            $user_stripe_id = $user_stripe_data->id;
        }
        else {
            try {
                // Creating Customer
               
                $customer = \Stripe\Customer::create(array(
                    "email" => $request->user_data->user->email
                ));
                $customer_id = $customer['id'];
                // Saving Customer
                $new_user_stripe = new UserStripe;
                $new_user_stripe->user_id = $user_id;
                $new_user_stripe->customer_id = $customer_id;
                $new_user_stripe->save();    

                $user_stripe_id = $new_user_stripe->id;
            }                
            catch (\Exception $e) {     
                return $e->getMessage();
            }
        }
        // Update Card
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);
            $customer->sources->create(array("source" => $token));

        } 
        catch (\Exception $e) {     
            return $e->getMessage();
        }
        $customer = \Stripe\Customer::retrieve($customer_id);
        $card = $customer->sources->retrieve($customer['default_source']);
        $card->name = isset($request->name) ? $request->name : $request->user_data->user->first_name;
        $card->save();
        
        try {
            \Stripe\Charge::create(array(
                "amount" => $amount,
                "currency" => "usd",
                "customer" => $customer_id,
            ));
        }
        catch (\Exception $e) {                         
            return $e->getMessage();   
        }
        return 'payment_success';
    }


    // Make Stripe Payment
    public static function makeStripePayment($request, $amount) {

        //Check Validation          
        $validation = Validator::make($request->all(), array('token' => 'required'));
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => 'Token is required.'], 400);

            
        $current_time = Carbon::now();
        $token = $request->token;
        $user_id = $request->user_data->user_id;            
        $user_stripe_data = UserStripe::where('user_id', $user_id)->first();
        if(!empty($user_stripe_data)) {                                
            $customer_id = $user_stripe_data->customer_id;
            $user_stripe_id = $user_stripe_data->id;
        }
        else {
            try {
                // Creating Customer
                \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
                $customer = \Stripe\Customer::create(array(
                    "email" => $request->user_data->user->email
                ));
                $customer_id = $customer['id'];
                // Saving Customer
                $new_user_stripe = new UserStripe;
                $new_user_stripe->user_id = $user_id;
                $new_user_stripe->customer_id = $customer_id;
                $new_user_stripe->save();    

                $user_stripe_id = $new_user_stripe->id;
            }                
            catch (\Exception $e) {     
                return $e->getMessage();
            }
        }   

        // Update Card
        try {
            \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

            $customer = \Stripe\Customer::retrieve($customer_id);
            $customer->sources->create(array("source" => $token));

        } 
        catch (\Exception $e) {     
            return $e->getMessage();
        }
        \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

        $customer = \Stripe\Customer::retrieve($customer_id);
        $card = $customer->sources->retrieve($customer['default_source']);
        $card->name = isset($request->name) ? $request->name : $request->user_data->user->first_name;
        $card->save();
        
        //create charge
        try {
            \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
            $charge = \Stripe\Charge::create(array(
                "amount" => $amount,
                "currency" => "usd",
                "customer" => $customer_id,
            ));
        }
        catch (\Exception $e) {                         
            return $e->getMessage();   
        }

        return 'payment_success';

    }  
    
    // Add Subscription Card
    public static function addSubscriptionCard($request, $amount) {
                
        //Check Validation          
        $validation = Validator::make($request->all(), array('token' => 'required'));
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => 'Token is required.'], 400);

        \Stripe\Stripe::setApiKey("sk_test_8N9H8KPT7YIZ3vRCOPbWkIlc");
        $token = $request->token;
        $user_id = $request->user_data->user_id;            
        $user_stripe_data = UserStripe::where('user_id', $user_id)->first();
        if(!empty($user_stripe_data)) {                                
            $customer_id = $user_stripe_data->customer_id;
            $user_stripe_id = $user_stripe_data->id;
        }
        else {
            try {
                // Creating Customer
                $customer = \Stripe\Customer::create(array(
                    "email" => $request->user_data->user->email
                ));
                $customer_id = $customer['id'];
                // Saving Customer
                $new_user_stripe = new UserStripe;
                $new_user_stripe->user_id = $user_id;
                $new_user_stripe->customer_id = $customer_id;
                $new_user_stripe->save();    

                $user_stripe_id = $new_user_stripe->id;
            }                
            catch (\Exception $e) {     
                return $e->getMessage();
            }
        }   
        
        // Update Card
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);
            $customer->sources->create(array("source" => $token));

        } 
        catch (\Exception $e) {     
            return $e->getMessage();
        }
        $customer = \Stripe\Customer::retrieve($customer_id);
        $card = $customer->sources->retrieve($customer['default_source']);
        $card->name = isset($request->name) ? $request->name : $request->user_data->user->first_name;
        $card->save();


        UserStripe::where('id', $user_stripe_id)->update([
            'card_id' => $card['id'],
            'card_brand' => $card['brand'],
            'card_last_four' => $card['last4'],
            'trial_ends_at' => $card['exp_year'].'-'.$card['exp_month'].'-01 00:00:00'
        ]);    
        
        try {
            $subscription = \Stripe\Subscription::create([
            "customer" => $customer_id,
            "items" => [
                [
                //"plan" => "plan_EoqdmKRtPhatbd", 
                "plan"=>"price_1IMBBQJacb5f8ZjUYMOVy6Zx" ,  //monthly test creds
                // "plan" => "plan_Eqdys976MUSjLO",        //daily test creds
                //"plan" => "plan_EsVwQezwibYpjI",    //monthly live creds
                ],
            ]
            ]);
        }
        catch (\Exception $e) {                         
            return $e->getMessage();   
        }

        UserStripe::where('id', $user_stripe_id)->update([
            'subscription_id' => $subscription['id']
        ]);    
        

        return 'payment_success';

    }    
    
    // Make invoice
    public static function makeInvoice($request, $amount) {

        // //Check Validation          
        // $validation = Validator::make($request->all(), array('token' => 'required'));
        // if($validation->fails())
        //     return response()->json(['error' => 'bad_request', 'error_description' => 'Token is required.'], 400);

            
        $current_time = Carbon::now();
        // $token = $request->token;
        $user_id = $request->user_data->user_id;            
        $user_stripe_data = UserStripe::where('user_id', $user_id)->first();
        if(!empty($user_stripe_data)) {                                
            $customer_id = $user_stripe_data->customer_id;
            $user_stripe_id = $user_stripe_data->id;
        }
        else {
            try {
                // Creating Customer
                \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
                $customer = \Stripe\Customer::create(array(
                    "email" => $request->user_data->user->email
                ));
                $customer_id = $customer['id'];
                // Saving Customer
                $new_user_stripe = new UserStripe;
                $new_user_stripe->user_id = $user_id;
                $new_user_stripe->customer_id = $customer_id;
                $new_user_stripe->save();    

                $user_stripe_id = $new_user_stripe->id;
            }                
            catch (\Exception $e) {     
                return $e->getMessage();
            }
        }   

        // // Update Card
        // try {
        //     \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

        //     $customer = \Stripe\Customer::retrieve($customer_id);
        //     $customer->sources->create(array("source" => $token));

        // } 
        // catch (\Exception $e) {     
        //     return $e->getMessage();
        // }
        // \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

        // $customer = \Stripe\Customer::retrieve($customer_id);
        // $card = $customer->sources->retrieve($customer['default_source']);
        // $card->name = isset($request->name) ? $request->name : $request->user_data->user->first_name;
        // $card->save();
        
        // //create charge
        // try {
        //     \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
        //     $charge = \Stripe\Charge::create(array(
        //         "amount" => $amount,
        //         "currency" => "usd",
        //         "customer" => $customer_id,
        //     ));
        // }
        // catch (\Exception $e) {                         
        //     return $e->getMessage();   
        // }

        //create invoice item
        try {
            \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

            \Stripe\InvoiceItem::create([
                "customer" => $customer_id,
                "amount" => $amount,
                "currency" => "usd",
                "description" => "Referral fee"
            ]);
        }
        catch (\Exception $e) {                         
            return $e->getMessage();   
        }

     

        return 'payment_success';

    } 
    
    public static function sendSms($country_code,$phone_number,$content){
        $id = "ACd0df20f0b58fa51ff8b8999b2b253228";
        $token = "f7e1b55dc05c4c35000ab66ae340c379";
        $url = "https://api.twilio.com/2010-04-01/Accounts/".$id."/SMS/Messages";
        $from = "+17245646633";
        $to = '+'.$country_code.$phone_number; // twilio trial verified number
        $body = $content;
        $sendMsg = array (
            'From' => $from,
            'To' => $to,
            'Body' => $body,
        );
        $post = http_build_query($sendMsg);
        $x = curl_init($url );
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
    }
    public static function smsContent(){
        $data['incompletePricing'] = "We'd love to help you price your home! Click the link to finish your request.https://homeease.pro/residential";
        $data['coupon'] = "Your first time user coupon code is ready! Use it at checkout in the next 10 mins for a $15 discount";
        $data['paid-e-appraisal'] = "Your e-appraisal is complete! Go to your https://homeease.pro to see the current market price of your home";
        $data['comp-recieved'] = "We have provided you with 3 comparable home sales to help you appraise your home! Visit https://homeease.pro/ Don't have the time? Use the shortcut option";
        $data['incompleteCal'] = "Do you still want to appraise your home? Visit https://homeease.pro to calculate your pricing  or use the shortcut option and a pro will do it for you!";
        return $data;
    }
    

}