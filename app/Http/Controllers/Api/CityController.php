<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\City;
use App\Models\County;
use App\Models\State;

class CityController extends Controller
{
    /*-----------------------------------------------------
    -------------------------------------------------------
        * City and County Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

    //Api cities listing
    public static function apiCitiesListing(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), City::$apiCityListingRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
           
        // $counties = json_decode($request->counties);

        $cities = City::where('county_id', $request->counties)->get();
    
        //send json response 
        return response()->json($cities,200);
    }

    //Api counties listing
    public static function apiCountiesListing(Request $request) {
        // $county_ids = City::distinct()->pluck('county_id');
        $validation = Validator::make($request->all(),[
            'state_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            
        
        $counties = County::where('deleted_at', NULL)->where('state_id', $request->state_id)->get();
    
        //send json response 
        return response()->json($counties,200);
    }

    //Api all cities listing
    public static function apiAllCities() {
           
        $cities = City::get();
    
        //send json response 
        return response()->json($cities,200);
    }

    // version 3 *******************************************************************

    //Api all states listing
    public static function apiStatesListing() {
        
        $states = State::where("deleted_at", NULL)->get();
    
        //send json response 
        return response()->json($states,200);
    }

    //Api all counties without state listing
    public static function apiAllCounties() {
        
        $states = County::where("deleted_at", NULL)->get();
    
        //send json response 
        return response()->json($states,200);
    }

}
