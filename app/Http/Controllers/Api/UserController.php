<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Email;
use App\Library\CommonFunction;
use Carbon\Carbon;
use App\Models\PasswordReset;
use App\Models\Property;
use App\Models\ExtraProperty;
use App\Models\UserDevice;
use App\Models\Apartment;
use App\Models\County;
use App\Models\City;
use App\Models\State;
use App\Models\Upload;
use App\Models\ExtraCity;
use App\Models\CompProperty;
use App\Models\UserStripe;
use App\Models\Payment;
use App\Models\UserInvite;
use App\Models\HomeyOpinion;
use App\Models\Chat;
use App\Models\CouponDiscount;

class UserController extends Controller
{
    /*-----------------------------------------------------
    -------------------------------------------------------
        * User Functions
    -------------------------------------------------------
    ------------------------------------------------------- */
   

        //Api Check User
        public static function apiCheckUser(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiCheckUserRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $fcm_id = $request->fcm_id;
            $email = $request->email;
            $user = User::where('email', $email)->first();
            if(empty($user)) {
                $user_status = 1;  // new user
            }
            elseif(!empty($user)) {
                if($user->user_type == 1)
                {
                    $access = UserDevice::where('user_id', $user->id)->where('fcm_id', $fcm_id)->first();
                    if(empty($access)) {
                        $user_status = 2;  //user logged out
                    }
                    else {
                        $user_status = 3;   //user logged in
                    }
                }
                else {
                    $user_status = 4;  //agent
                }
            }

            return response()->json($user_status,200);   
        }
    
        //Api User Signup
           public static function apiUserSignUp(Request $request) {
                
            //Check Validation    
               
            $validation = Validator::make($request->all(), User::$apiUserSignUpRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $password = Hash::make($request->password);           
            $url = $request->url; 
            $user_name = $request->user_name;
            $type = "Client";

            // Check if Fcm Id provided
            if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
                $fcm_id = $request->fcm_id;
            
                UserDevice::where('fcm_id', $fcm_id)->delete();
            }   
            else {
                $fcm_id = NULL;
            }
            //Insert in user
            $new_user = new User;
            $new_user->email = $request->email;
            $new_user->first_name = $request->first_name;
            $new_user->last_name = $request->last_name;
            $new_user->user_type = 1; // 1 - User, 2 - Agent
            $new_user->password = $password;
            $new_user->image = isset($request->image) ? $request->image : "";
            $new_user->have_agent = $request->have_agent;
            $new_user->agent_email = NULL;
            $new_user->save();


            // Add Edit User Device
            $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $new_user->id);
            if($add_edit_user_device_response!="add_edit_user_device_success")
                return $add_edit_user_device_response;  


            // Get User Details
            $user_details = User::getUserDetails($new_user->id, $fcm_id);

            // $user_details = User::where('id', $new_user->id)->first();
            //Send JSON response
            return response()->json($user_details,200);                     
        }

        //Api User Dashboard
        public static function apiUserDashboard(Request $request) {

            $user_id = $request->user_data->user_id;
            
            $user = User::where('id', $user_id)->first();
            $user->active_properties = Property::orderBy('id', 'desc')->where('user_id', $user_id)->where('status', '<>', 3)->where('status', '!=', 15)->whereIn('agency_type', [1,2,3,4])->where('payment_status','!=',0)->where('deleted_at', NULL)->where('blocked_at', NULL)->where('property_type','property')->get();

            foreach($user->active_properties as $key =>$value) {
                if($value->type == 4) {
                    $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $user->id)->where('property_id',$value->id)->first(); //5-yearly, 8-monthly
                    if($payment != '') {
                        
                        $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
                        if($payment->payment_type == 8) {
                            if($diff > 30) {
                                $value->apartment_payment = 0;
                            }
                            else {
                                $value->apartment_payment = 1;
                            }
                        }
                        else {
                            if($diff > 365) {
                                $value->apartment_payment = 0;
                            }
                            else {
                                $value->apartment_payment = 1;
                            }
                        }

                        // dd($diff);
                        $value->pricing_payment = 1;

                    }
                    else {
                        $value->apartment_payment = 0;
                        $value->pricing_payment = 0;
                    }
                }
                else {
                    $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $user->id)->where('property_id',$value->id)->whereIn('payment_type', [6,7,2])->first();
                    if($payment != '')
                    {
                        $value->pricing_payment = 1;
                    }
                    else
                    {
                        $value->pricing_payment = 0;
                    }
                    $value->apartment_payment = 0;
                }
                if($value->agent_id != NULL) {
                    $value->agent_name = User::select('first_name', 'last_name')->where('id', $value->agent_id)->first();
                    $value->image = User::where('id', $value->agent_id)->value('image');
                }
                else {
                    $value->agent_name = NULL;
                    $value->image = NULL;
                }

                if($value->second_agent != NULL) {
                    $value->second_agent_name = User::select('first_name', 'last_name')->where('id', $value->second_agent)->first();
                    $value->second_image = User::where('id', $value->second_agent)->value('image');
                }
                else {
                    $value->second_agent_name = NULL;
                    $value->second_image = NULL;
                }
                $value->paymentData = $payment;

                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->communication = Chat::orderBy('id', 'desc')->where([
                    ['property_id', $value->id],
                    ['sent_to', $user_id]
                ])->orWhere([
                    ['property_id', $value->id],
                    ['sent_by', $user_id]
                ])->first();
            }

            $user->completed = Property::orderBy('id', 'desc')->where('user_id', $user_id)->where('status', 3)->where('deleted_at', NULL)->where('blocked_at', NULL)->get();
            $user->completed_free = Property::orderBy('id', 'desc')->where('user_id', $user_id)->where('status', 15)->where('deleted_at', NULL)->where('blocked_at', NULL)->get();
            
            foreach($user->completed as $key =>$value) {
                if($value->agent_id != NULL) {
                    $value->agent_name = User::select('first_name', 'last_name')->where('id', $value->agent_id)->first();
                    $value->image = User::where('id', $value->agent_id)->value('image');
                }
                else {
                    $value->agent_name = NULL;
                    $value->image = NULL;
                }

                
                if($value->second_agent != NULL) {
                    $value->second_agent_name = User::select('first_name', 'last_name')->where('id', $value->second_agent)->first();
                    $value->second_image = User::where('id', $value->second_agent)->value('image');
                }
                else {
                    $value->second_agent_name = NULL;
                    $value->second_image = NULL;
                }

                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->communication = Chat::orderBy('id', 'desc')->where([
                    ['property_id', $value->id],
                    ['sent_to', $user_id]
                ])->orWhere([
                    ['property_id', $value->id],
                    ['sent_by', $user_id]
                ])->first();
            }
            foreach($user->completed_free as $key =>$value) {
                if($value->agent_id != NULL) {
                    $value->agent_name = User::select('first_name', 'last_name')->where('id', $value->agent_id)->first();
                    $value->image = User::where('id', $value->agent_id)->value('image');
                }
                else {
                    $value->agent_name = NULL;
                    $value->image = NULL;
                }

                
                if($value->second_agent != NULL) {
                    $value->second_agent_name = User::select('first_name', 'last_name')->where('id', $value->second_agent)->first();
                    $value->second_image = User::where('id', $value->second_agent)->value('image');
                }
                else {
                    $value->second_agent_name = NULL;
                    $value->second_image = NULL;
                }

                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->communication = Chat::orderBy('id', 'desc')->where([
                    ['property_id', $value->id],
                    ['sent_to', $user_id]
                ])->orWhere([
                    ['property_id', $value->id],
                    ['sent_by', $user_id]
                ])->first();
            }

            $user->extra_properties = Property::orderBy('id', 'desc')->whereIn('agency_type', [1,2,3,4])->where('user_id', $user_id)->where('property_type','extra')->where('deleted_at', NULL)->get();

            $user->edit_pricing_extra_properties = Property::orderBy('id', 'desc')->where('agency_type', NULL)->where('user_id', $user_id)->where('deleted_at', NULL)->where('property_type','extra')->get();

            $user->edit_pricing_active_properties = Property::orderBy('id', 'desc')->where('user_id', $user_id)->where('status', '<>', 3)->where('deleted_at', NULL)->where('property_type','property')->where('blocked_at', NULL)->where(function ($query) {
                $query->where('agency_type', NULL)
                ->orWhere('payment_status',0);
            })->get();
            
            foreach($user->edit_pricing_active_properties as $key =>$value) {
                if($value->type == 4) {
                    $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $user->id)->whereIn('payment_type', [5,8])->first(); //5-yearly, 8-monthly
                    if(empty($payment)) {
                        $value->apartment_payment = 0;
                    }
                    else {
                        $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
                        if($payment->payment_type == 8) {
                            if($diff > 30) {
                                $value->apartment_payment = 0;
                            }
                            else {
                                $value->apartment_payment = 1;
                            }
                        }
                        else {
                            if($diff > 365) {
                                $value->apartment_payment = 0;
                            }
                            else {
                                $value->apartment_payment = 1;
                            }
                        }

                        // dd($diff);
                    }
                    $value->pricing_payment = 0;
                }
                else {
                    $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $user->id)->where('property_id',$value->id)->where('payment_type', 7)->first();
                    if($payment != '')
                    {
                        $value->pricing_payment = 1;
                    }
                    else
                    {
                        $value->pricing_payment = 0;
                    }
                    $value->apartment_payment = 0;
                }
                if($value->agent_id != NULL) {
                    $value->agent_name = User::select('first_name', 'last_name')->where('id', $value->agent_id)->first();
                    $value->image = User::where('id', $value->agent_id)->value('image');
                }
                else {
                    $value->agent_name = NULL;
                    $value->image = NULL;
                }

                if($value->second_agent != NULL) {
                    $value->second_agent_name = User::select('first_name', 'last_name')->where('id', $value->second_agent)->first();
                    $value->second_image = User::where('id', $value->second_agent)->value('image');
                }
                else {
                    $value->second_agent_name = NULL;
                    $value->second_image = NULL;
                }
                $value->paymentData = $payment;

                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->communication = Chat::orderBy('id', 'desc')->where([
                    ['property_id', $value->id],
                    ['sent_to', $user_id]
                ])->orWhere([
                    ['property_id', $value->id],
                    ['sent_by', $user_id]
                ])->first();
            }

            return response()->json($user,200);             

        }

        //Api User Compd
        public static function apiUserCompd(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiUserCompdRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;

            $subject_prop_id = $request->subject_prop_id;

            $propertyDetail = Property::where('id',$subject_prop_id)->first();

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $propertyDetail->user_id)->first();
            if($payment == '') {
                $broker_payment = 0;
                $pricing_amount = 0;
            }
            else {
                $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
                if($diff > 30) {
                    $broker_payment = 0;
                    $pricing_amount = 0;
                }
                else {
                    $broker_payment = 1;
                    $pricing_amount = $payment->amount;
                }
                
            }

            $user = User::where('id', $user_id)->first();
            $user->subject_property = Property::where('id', $subject_prop_id)->first();
            $user->subject_property->prop_city = City::where('id', $user->subject_property->city)->value('name');
            $user->subject_property->prop_county = County::where('id', $user->subject_property->county)->value('name');

            $comp_properties = CompProperty::where('type', 1)->where('subject_prop_id', $subject_prop_id)->first();
        
            $user->c1 = Property::where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::where('id', $comp_properties->comp_prop3)->first();
            $user->broker_payment = $broker_payment;
            $user->pricing_amount = $pricing_amount;
            $user->paymentData = $payment;

            return response()->json($user,200);      


        }

    /*==========User Start pricing============*/

    public static function apiStartTrial1(Request $request) {
        //Check Validation
        $dbEmail = Email::where('id',1)->first();
        $client_email = $dbEmail->client_email;
        $test_email = $dbEmail->test_email;

        if($request->id == '')
        {
            if($request->user_id == '')
            {
                $checkEmail = User::where('email',$request->email)->first();
                if($checkEmail != '')
                {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'This email already exist. Please login to start pricing'], 400);
                
                }
                else
                {
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr( str_shuffle( $chars ), 0, 8 );
                    $data['new_signup']   = true;   
                    $url = 'https://homeease.pro/broker/agent'; 
                    if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
                        $fcm_id = $request->fcm_id;
                    
                        UserDevice::where('fcm_id', $fcm_id)->delete();
                    }   
                    else {
                        $fcm_id = NULL;
                    }
                    
                    $new_user = new User;
                    $new_user->email = $request->email;
                    $new_user->first_name = $request->first_name;
                    $new_user->last_name = $request->last_name;
                    $new_user->user_type = 1; // 1 - User, 2 - Agent
                    $new_user->password = Hash::make($password);
                    $new_user->image = isset($request->image) ? $request->image : "";
                    $new_user->have_agent = 0;
                    $new_user->agent_email = NULL;
                    $new_user->save();
                    $email = $request->email;
                    $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $new_user->id);
                    
                    //send email to user when start residential pricing
                    $mail = Mail::send("emails.new-user", [ 'request' => $request,'password'=>$password ], function ($m) use ($email) {
                        $m->to($email)->subject("Welcome to HomeEase!");
                    });
                    //send sms to user when start residential pricing
                    if($request->country_code && $request->phone_number) {
                        $content = 'Your HomeEase Login Email : '.$request->email.' Password : '.$password;
                        CommonFunction::sendSms($request->country_code,$request->phone_number,$content);
                    }
                }
                $user_id = $new_user->id;
            }
            else
            {
                $getUser = User::find($request->user_id);
                $getFcm = UserDevice::where('user_id',$getUser->id)->first();
                $user_id = $getUser->id;
                if($getFcm == '')
                {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'Session Expired!'], 400);
                }
                else
                {
                    $fcm_id = $getFcm->fcm_id;
                }
                $getCoupon = CouponDiscount::where('user_id',$request->user_id)->get();
                if(count($getCoupon) == 0){
                    $data['new_signup']   = true;
                }
            }
            $user = User::where('id', $user_id)->first();

            $city = City::where('name', 'like', $request->city)->value('id');
            $county = County::where('name', 'like', $request->county)->value('id');
            $state = State::where('name', 'like', $request->state)->value('id');
            if($user->user_type == 2) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'Agent can not start pricing.'], 400);
            }
            $agent_id = NULL;
            if($request->lat == NULL && $request->lng == NULL) {
                $lat = 0; 
                $lng = 0;
            }
            else {
                $lat = $request->lat;
                $lng = $request->lng;
            }
            if($request->type !=4) {
                $validation = Validator::make($request->all(), User::$apiAgencyTypeRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            }
            $new_property = new Property;
            if($state && $county) {
                if(!$city) {
                    // new code 
                    $city1 = new City;
                    $city1->county_id = $county;
                    $city1->name = $request->city;
                    $city1->above_sqft = 14;
                    $city1->below_sqft = 7;
                    $city1->year_build = 2000;
                    $city1->garage1 = 5000;
                    $city1->garage2 = 2500;
                    $city1->heat = 1000;
                    $city1->ac = 2500;
                    $city1->fire_place = 2000;
                    $city1->porch = 1000;
                    $city1->bedroom_above = 5000;
                    $city1->bedroom_below = 3000;
                    $city1->bathroom_above = 4000;
                    $city1->bathroom_below = 2500;
                    $city1->quality = 7;
                    $city1->condition_value = 3;
                    $city1->save();

                    $city = $city1->id;

                }
            }
            else {
                $county = $request->county;
                $state = $request->state;
                $city = $request->city;
                if($request->type != 4) {
                    $extraProperty = new ExtraProperty;
                    $extraProperty->user_id = $user->id;
                    $extraProperty->agent_id = $agent_id;
                    $extraProperty->is_subject_prop = 1;
                    $extraProperty->type = $request->type;
                    $extraProperty->address_1 = $request->address_1;
                    $extraProperty->address_2 = isset( $request->address_2) ? $request->address_2 : NULL;
                    $extraProperty->city = $city;
                    $extraProperty->county = $county;
                    $extraProperty->state = $state;
                    $extraProperty->zip_code = $request->zip_code;
                    $extraProperty->country = $request->country;
                    $extraProperty->lat = $lat;
                    $extraProperty->lng = $lng;
                    $extraProperty->save();
                    $data['extra_property_id'] = $extraProperty->id;
                    $new_property->property_type = 'extra';
                    $new_property->outside_area = 1;
                }
                
            }
            $new_property->user_id = $user->id;
            $new_property->agent_id = $agent_id;
            $new_property->is_subject_prop = 1;
            $new_property->type = $request->type;
            $new_property->phone_number = $request->phone_number;
            $new_property->preferred_communication = $request->preferred_communication;
            $new_property->address_1 = $request->address_1;
            $new_property->address_2 = isset( $request->address_2) ? $request->address_2 : NULL;
            $new_property->city = $city;
            $new_property->county = $county;
            $new_property->state = $state;
            $new_property->zip_code = $request->zip_code;
            $new_property->country = $request->country;
            $new_property->lat = $lat;
            $new_property->lng = $lng;
            $new_property->country_code = $request->country_code;
            $new_property->country_abv = $request->country_abv;
            $new_property->status = 0;
            $new_property->step1 = 1;
            $new_property->save();
            
            if($request->type==4) {
                $apartment = new Apartment;
                $apartment->id = $new_property->id;
                $apartment->save();

                Property::where('id', $new_property->id)->update([
                    'status' => 7,
                    'agency_type' => NULL
                ]);
            }
            $property = Property::where('id', $new_property->id)->first();
            
            $property->user_detail = User::getUserDetails($new_property->user_id,$fcm_id);
            $data['property'] = $property;
            $data['property_id'] = $new_property->id;
            
            $data['p_type'] = $property->property_type;
            
            return response()->json($data,200); 
        }
        else
        {
            $getCoupon = couponDiscount::where('user_id',$request->user_id)->get();
            if(count($getCoupon) == 0){
                $data['new_signup']   = true;
            }
            //$extra_property = ExtraProperty::find($request->id);

            $new_property = Property::find($request->id);
            $new_property->agency_type = $request->agency_type;
            $new_property->agency_type = $request->agency_type;
            $new_property->how_soon = $request->how_soon;
            $new_property->selling_status = $request->selling_status;
            $new_property->credit_score = $request->credit_score;
            $new_property->prequalified = $request->prequalified;
            $new_property->progress_status = 0;
            $new_property->save();

            $userData = User::find($new_property->user_id);
            $getEmail = $userData->email;

            $property = Property::where('id', $request->id)->first();
            $getFcm = UserDevice::where('user_id',$new_property->user_id)->first();
            $property->user_detail = User::getUserDetails($new_property->user_id,$getFcm->fcm_id);

            if($request->agency_type == 4) {
                User::where('id', $request->user_id)->update([
                    'is_agent' => 1
                ]);
            }
            $data['property'] = $property;
            $data['message'] = 'Property added successfully.';
            $data['p_type'] = $property->property_type;
            return response()->json($data,200); 
        }
    }

    /*======Coupon discount for pricing====*/
    public static function couponDiscount(Request $request) {
        $user_id = $request->user_data->user_id;
        $userData = User::find($user_id);
        $username = $userData->first_name.' '.$userData->last_name;
        $email = $userData->email;
        $property = Property::where('id',$request->property_id)->first();
        
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $coupon = substr( str_shuffle( $chars ), 0, 6 ); 
        $now = time();
        $ten_minutes = $now + (10 * 60);
        $getTime = date('Y-m-d H:i:s', $ten_minutes);

        $CouponDiscount = new CouponDiscount;
        $CouponDiscount->user_id = $user_id;
        $CouponDiscount->coupon_code = $coupon;
        $CouponDiscount->expire_time = $getTime;
        $CouponDiscount->save();
        //Start take a shortcut email//
        $mail = Mail::send("emails.coupon-discount", ['coupon' => $coupon, 'username' => $username], function ($m) use ($email) {
            $m->to($email)->subject("Coupon Discount");
        });
        //Send coupons sms//
        if($property->country_code && $property->phone_number) {
            $getContent = CommonFunction::smsContent();
            $content = $getContent['coupon'].' '.$coupon;
            CommonFunction::sendSms($property->country_code,$property->phone_number,$content);
        }

        return response()->json('success', 200);
        //return response()->json($email,200); 
        
    }

    /*==========Start Pricing Payment=========*/
    public function startPricingPayment(Request $request)
    {
        $dbEmail = Email::where('id',1)->first();
        $user_id = $request->user_data->user_id;
        $user = User::where('id', $user_id)->first();
        $property = Property::where('id', $request->property_id)->first();
        $updateProperty = Property::find($request->property_id);
        $investor = User::where('id',1)->first();
        
        $email = $user->email;
        $investorEmail = $investor->email;
        $test_email = $dbEmail->test_email;
        $client_email = $dbEmail->client_email;
        $myemail = 'gurjeevan.kaur.henceforth@gmail.com';
        $userData = User::find($user_id);
        if($request->is_self){
            $getEmail = $userData->email;
            if($property->property_type == 'property'){
                if($property->type == 4){
                    //Start email for apartment start pricing
                    $mail = Mail::send("emails.apartment-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($getEmail) {
                        $m->to($getEmail)->subject("Let’s price your property!");
                    });
                    $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("Let’s price your property!");
                    });
                    if($test_email != ''){
                        $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($test_email) {
                            $m->to($test_email)->subject("Let’s price your property!");
                        });
                    }
                }
                    
                else{
                    
                    //Start Email for start pricing
                    $mail = Mail::send("emails.start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($getEmail) {
                        $m->to($getEmail)->subject("Let’s price your property!");
                    });
                    $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("Let’s price your property!");
                    });
                    if($test_email != ''){
                        $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($test_email) {
                            $m->to($test_email)->subject("Let’s price your property!");
                        });
                    }
                }
                    //End Email for start pricing
                
            }
            else{
                
                $mail = Mail::send("emails.extra-prop", ['new_property' => $property, 'first_name' => $userData->first_name, 'last_name' => $userData->last_name, 'email' => $userData->email], function ($m) use ($client_email) {
                    $m->to($client_email)->subject("Extra Property - HomeEase");
                });
                if($test_email != ''){
                    $mail = Mail::send("emails.extra-prop", ['new_property' => $property, 'first_name' => $userData->first_name, 'last_name' => $userData->last_name, 'email' => $userData->email], function ($m) use ($test_email) {
                        $m->to($test_email)->subject("Extra Property - HomeEase");
                    });
                }
    
                $mail = Mail::send("emails.user-extra-property", ['new_property' => $property, 'first_name' => $userData->first_name, 'last_name' => $userData->last_name, 'email' => $userData->email], function ($m) use ($email) {
                    $m->to($email)->subject("We can still Help!");
                });
            }
        }
        else{
            if($request->isFreeEAppraisal != 1){
                $amount = $request->amount;
                if($request->coupon_code){
                    $currentTime = date("Y-m-d H:i:s");
                    $coupon = CouponDiscount::where('user_id', $user_id)->where('coupon_code', $request->coupon_code)->first();
                    if($coupon != ''){
                        $expire_time = $coupon->expire_time;
                        if($currentTime > $expire_time){
                            return response()->json(['error' => 'bad_request', 'error_description' => 'Coupon code expired'], 400);
                        }else{
                            $amount = $request->amount-15;
                        }
                    }
                    else{
                        return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid coupon code'], 400);
                    }
                }

                $totalAmount = $amount*100;
                $stripe = CommonFunction::stripePayment($request, $totalAmount);
                if($stripe != 'payment_success') {
                    return $stripe;
                }
                if($property->type == 4){
                    $mail = Mail::send("emails.user-apartment-e-appraisal",['request'=>$user], function ($m) use ($email) {
                        $m->to($email)->subject("Savvy investor receipt");
                    });
                    // $mail = Mail::send("emails.investor-apartment-e-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($investorEmail) {
                    //     $m->to($investorEmail)->subject("Pre-paid appraisal");
                    // });
                    
                    $mail = Mail::send("emails.investor-apartment-e-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("Pre-paid appraisal");
                    });
                }
                else{
                    $mail = Mail::send("emails.user-e-appraisal",['request'=>$user,'amount'=>$amount], function ($m) use ($email) {
                        $m->to($email)->subject("Savvy homeowner receipt");
                    });
                    if($test_email != ''){
                        $mail = Mail::send("emails.user-admin-e-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($test_email) {
                            $m->to($test_email)->subject("Pre-paid appraisal");
                        });
                    }
                    $mail = Mail::send("emails.user-admin-e-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("Pre-paid appraisal");
                    });
                    // if($property->country_code && $property->phone_number) {
                    //     $getContent = CommonFunction::smsContent();
                    //     $content = $getContent['paid-e-appraisal'];
                    //     CommonFunction::sendSms($property->country_code,$property->phone_number,$content);
                    // }
                }
            }
            else{
                $amount = 0;
                if($property->type == 4){
                    $mail = Mail::send("emails.user-apartment-free-appraisal",['request'=>$user], function ($m) use ($email) {
                        $m->to($email)->subject("HomeEase has received your request!");
                    });
                    // $mail = Mail::send("emails.investor-apartment-free-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($investorEmail) {
                    //     $m->to($investorEmail)->subject("New free appraisal request");
                    // });
                    if($test_email != ''){
                        $mail = Mail::send("emails.investor-apartment-free-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($test_email) {
                            $m->to($test_email)->subject("New free appraisal request");
                        });
                    }
                    $mail = Mail::send("emails.investor-apartment-free-appraisal",['request'=>$user,'property'=>$property], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("New free appraisal request");
                    });
                    
                }
                else{
                    $mail = Mail::send("emails.user-free-appraisal",['request'=>$user], function ($m) use ($email) {
                        $m->to($email)->subject("Free Appraisal");
                    });
                    if($test_email != ''){
                        $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($test_email) {
                            $m->to($test_email)->subject("Free Appraisal");
                        });
                    }
                    $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$userData], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("Free Appraisal");
                    });

                    
                }
            }
            $payment = new Payment;
            $payment->user_id = $user_id;
            $payment->payment_type = 7; //broker payment
            $payment->property_id = $request->property_id;
            $payment->amount = $amount;
            $payment->save();
            $updateProperty->property_type = 'property';
        }
        
        $updateProperty->cron_status = 1;
        if($request->isFreeEAppraisal == 1 && $property->type == 4){
            $updateProperty->payment_status = 3;
        }
        else{
            $updateProperty->payment_status = 1;
        }
        $updateProperty->save();
        return response()->json('success', 200);
    }
    
    //Api Take Shortcut
    public static function apiBrokerPayment(Request $request) {
        $user_id = $request->user_data->user_id;
        $user = User::where('id', $user_id)->first();
        $amount = 7500;
        $stripe = CommonFunction::makeStripePayment($request, $amount);
        if($stripe != 'payment_success'){
            return $stripe;
        }
        $payment = new Payment;
        $payment->user_id = $user_id;
        $payment->payment_type = 7; //broker payment
        $payment->amount = 75;
        $payment->save();
        $payment_by = $user->first_name;
        $payment_by_email = $user->email;
        $text = "Broker/Agent Payment";

        //Start take a shortcut email//
        $mail = Mail::send("emails.take-shortcut", ['name' => $payment_by, 'text' => $text, 'amount' => 75.00], function ($m) use ($payment_by_email) {
            $m->to($payment_by_email)->subject("Successful Payment");
        });
        //End take a shortcut email//

        return response()->json('success', 200);
        
    }


    /*-----------------------------------------------------
    -------------------------------------------------------
        * Agent Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api Agent Signup
        public static function apiAgentSignUp(Request $request) {
              
            //Check Validation          
           
             $validation = Validator::make($request->all(), User::$apiAgentSignUpRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $password = Hash::make($request->password);            

            // Check if Fcm Id provided
            if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
            $fcm_id = $request->fcm_id;
                        
                UserDevice::where('fcm_id', $fcm_id)->delete();
            }   
            else {
                $fcm_id = NULL;
            }

            $new_user = new User;
            $new_user->email = $request->email;
            $new_user->user_type = 2; // 1 - User, 2 - Agent
            $new_user->password = $password;
            $new_user->first_name = $request->first_name;
            $new_user->last_name = $request->last_name;
            $new_user->mls_id = $request->mls_id;
            $new_user->licence_type = $request->licence_type;  //1-Broker, 2-Agent
            $new_user->image = isset($request->image) ? $request->image : "";
            $new_user->save();


            // Add Edit User Device
            $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $new_user->id);
            if($add_edit_user_device_response!="add_edit_user_device_success")
                return $add_edit_user_device_response;  

            // Get User Details
            $user_details = User::getUserDetails($new_user->id, $fcm_id);

            //Send JSON response
            return response()->json($user_details,200);                     
        }

        //Api Agent Dashboard
        public static function apiAgentDashboard(Request $request) {

            
            $user_id = $request->user_data->user_id;
            
                        
            $user = User::where('id', $user_id)->first();
            if($user_id == 1 ){
                $user->active_properties = Property::orderBy('id', 'desc')->where([
                    ['status', '=', 2],
                    ['status', '!=', 15],
                    ['type', '=', 4],
                    ['property_type', '=', 'property'],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL],
                ])->get();
            }
            // else if($user_id == 2){
            //     $user->active_properties = Property::orderBy('id', 'desc')->where('agent_id', $user_id)->where([
            //         ['status', '<>', 3],
            //         ['status', '<>', 0],
            //         ['status', '<>', 8],
            //         ['status', '<>', 9],
            //         ['status', '!=', 15],
            //         ['outside_area', '=', 1],
            //         ['property_type', '=', 'property'],
            //         ['deleted_at', NULL],
            //         ['blocked_at', NULL],
            //     ])->get();
            // }
            else{
                $user->active_properties = Property::orderBy('id', 'desc')->where('agent_id', $user_id)->where([
                    ['status', '<>', 3],
                    ['status', '<>', 0],
                    ['status', '<>', 8],
                    ['status', '<>', 9],
                    ['status', '!=', 15],
                    ['type', '<>', 4],
                    ['property_type', '=', 'property'],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL],
                ])->get();
            }

            foreach($user->active_properties as $key => $value)
            {
                $payment = Payment::where('property_id',$value->id)->where('user_id',$value->user_id)->first();
                if($payment != '')
                {
                    $value->pricing_payment = 1;
                }
                else
                {
                    $value->pricing_payment = 0;
                }
                $value->paymentData = $payment;

                $value->first_name = User::where('id', $value->user_id)->value('first_name');
                $value->last_name = User::where('id', $value->user_id)->value('last_name');
                $value->image = User::where('id', $value->user_id)->value('image');
                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->homey_opinion = HomeyOpinion::where([
                    ['agent_id', $value->agent_id],
                    ['property_id', $value->id],
                    ['user_id', $value->user_id]
                ])->value('id');
            }

            if($user_id == 1)
            {
                $user->completed = Property::orderBy('id', 'desc')
                ->where([
                    ['agent_id', $user_id], 
                    ['status', 3],
                    ['type', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->orWhere([
                    ['second_agent', $user_id], 
                    ['status', 3],
                    ['type', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->get();

                $user->completed_free = Property::orderBy('id', 'desc')
                ->where([
                    ['agent_id', $user_id], 
                    ['status', 15],
                    ['type', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->orWhere([
                    ['second_agent', $user_id], 
                    ['status', 15],
                    ['type', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->get();
            }
            // elseif($user_id == 2)
            // {
            //     $user->completed = Property::orderBy('id', 'desc')
            //     ->where([
            //         ['agent_id', $user_id], 
            //         ['status', 3],
            //         ['outside_area', '=', 1],
            //         ['property_type', '=', 'property'],
            //         ['deleted_at', NULL],
            //         ['blocked_at', NULL]
            //     ])->orWhere([
            //         ['second_agent', $user_id], 
            //         ['status', 3],
            //         ['outside_area', '=', 1],
            //         ['property_type', '=', 'property'],
            //         ['type', 4],
            //         ['deleted_at', NULL],
            //         ['blocked_at', NULL]
            //     ])->get();

            //     $user->completed_free = Property::orderBy('id', 'desc')
            //     ->where([
            //         ['agent_id', $user_id], 
            //         ['status', 15],
            //         ['outside_area', '=', 1],
            //         ['property_type', '=', 'property'],
            //         ['deleted_at', NULL],
            //         ['blocked_at', NULL]
            //     ])->orWhere([
            //         ['second_agent', $user_id], 
            //         ['status', 15],
            //         ['outside_area', '=', 1],
            //         ['property_type', '=', 'property'],
            //         ['type', 4],
            //         ['deleted_at', NULL],
            //         ['blocked_at', NULL]
            //     ])->get();
            // }
            else{
                $user->completed = Property::orderBy('id', 'desc')
                ->where([
                    ['agent_id', $user_id], 
                    ['status', 3],
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->orWhere([
                    ['second_agent', $user_id], 
                    ['status', 3],
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->get();

                $user->completed_free = Property::orderBy('id', 'desc')
                ->where([
                    ['agent_id', $user_id], 
                    ['status', 15],
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->orWhere([
                    ['second_agent', $user_id], 
                    ['status', 15],
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL]
                ])->get();
            }
            foreach($user->completed as $key => $value)
            {
                $value->first_name = User::where('id', $value->user_id)->value('first_name');
                $value->last_name = User::where('id', $value->user_id)->value('last_name');
                $value->image = User::where('id', $value->user_id)->value('image');
                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
            }
            foreach($user->completed_free as $key => $value)
            {
                $value->first_name = User::where('id', $value->user_id)->value('first_name');
                $value->last_name = User::where('id', $value->user_id)->value('last_name');
                $value->image = User::where('id', $value->user_id)->value('image');
                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
            }

            

            if($user_id == 1)
            {
                $user->requests = Property::orderBy('id', 'desc')->where([['type',4], ['status','=',7],['deleted_at', NULL],['blocked_at', NULL],
                ])
                ->where(function($query) use ($user){
                        $query->whereNotIn('id', $user->removed_properties);
                })->get();
            }
            elseif($user_id == 2)
            {
                $user->requests = Property::orderBy('id', 'desc')->where([
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL],
                    ['property_type', 'property'],
                ])
                ->where(function($query) use ($user){
                        $query->where([
                            ['status','=',0],
                        ])->whereIn('agency_type', [1,2,3,4])   // earliar only 2,3 
                        ->whereIn('type', $user->estate_type)
                        ->whereIn('county', $user->preferred_counties)
                        ->whereNotIn('id', $user->removed_properties)
                        ->orWhere('outside_area', '=', 1);
                        
                })->get();
                // $user->requests = Property::orderBy('id', 'desc')->where('outside_area', '=', 1)->where('property_type','property')->where([['status','=',0],['deleted_at', NULL],['blocked_at', NULL],
                // ])->get();
            }
            else
            {
                $user->requests = Property::orderBy('id', 'desc')->where([
                    ['type', '<>', 4],
                    ['deleted_at', NULL],
                    ['blocked_at', NULL],
                ])
                ->where(function($query) use ($user){
                        $query->where([
                            ['status','=',0],
                        ])->whereIn('agency_type', [1,2,3,4])   // earliar only 2,3 
                        ->whereIn('type', $user->estate_type)
                        ->whereIn('county', $user->preferred_counties)
                        ->whereNotIn('id', $user->removed_properties);
                        
                })->get();
            }
            
            foreach($user->requests as $key => $value)
            {
                $payment = Payment::where('property_id',$value->id)->where('user_id',$value->user_id)->first();
                if($payment != '')
                {
                    $value->pricing_payment = 1;
                }
                else
                {
                    $value->pricing_payment = 0;
                }
                $value->paymentData = $payment;

                $value->first_name = User::where('id', $value->user_id)->value('first_name');
                $value->last_name = User::where('id', $value->user_id)->value('last_name');
                $value->image = User::where('id', $value->user_id)->value('image');
                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
            }

            $user->review_requests = Property::orderBy('id', 'desc')
            ->where([
                ['status', 8], 
                ['agent_id', '<>', $user_id],
                ['second_agent', NULL],
                ['deleted_at', NULL],
                ['blocked_at', NULL]
            ])->orWhere([
                ['status', '<>', 3],
                ['second_agent', $user_id],
                ['deleted_at', NULL],
                ['blocked_at', NULL]
            ])->get();

            foreach($user->review_requests as $key => $value)
            {
                $value->first_name = User::where('id', $value->user_id)->value('first_name');
                $value->last_name = User::where('id', $value->user_id)->value('last_name');
                $value->image = User::where('id', $value->user_id)->value('image');
                $value->credits = User::where('id', $value->user_id)->value('credits');
                $value->prop_city = City::where('id', $value->city)->value('name');
                $value->prop_county = County::where('id', $value->county)->value('name');
                $value->homey_opinion = HomeyOpinion::where([
                    ['agent_id', $value->second_agent],
                    ['property_id', $value->id],
                    ['user_id', $value->user_id]
                ])->value('id');
            }
            
            
            return response()->json($user,200);             
            
        }

        /*---------------------------------
            * Version 2
        ----------------------------------*/

            //Api invite user
            public static function apiInviteUser(Request $request) {
                  
                //Check Validation          
                $validation = Validator::make($request->all(), User::$apiInviteUserRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                
                $user_id = $request->user_data->user_id;
                $email = $request->email;
                $url = $request->url;
                $name = $request->user_data->user()->select('first_name', 'last_name')->first();
                $user_name = $name->first_name." ".$name->last_name;
                $agent = $request->user_data->user()->value('email');
                $type = "Agent";
                // dd($user_name);

                $exist = User::where('email', $email)->first();

                if(!empty($exist)) {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'This user already exists.'], 400);
                }
                
                $invited = UserInvite::where('agent_id', $user_id)->where('user_email', $email)->first();

                if(!empty($invited)) {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You have already invited this User.'], 400);
                }

                //send invite email to agent
                $user = User::where('email', $email)->first();
                if(empty($user)) {

                    // Send Email
                    $mail = Mail::send("emails.invite-user", ['agent_email' => $email, 'url' => $url, 'user_name' => $user_name, 'type' => $type, 'agent' => $agent], function ($m) use ($email) {
                        $m->to($email)->subject("Invite - HomeEase");
                    });

                }

                $invite = new UserInvite;
                $invite->agent_id = $user_id;
                $invite->user_email = $email;
                $invite->save();

                //Send JSON response
                return response()->json('User Invitation Sent.',200); 
            }

            //Api delete invite
            public static function apiDeleteInvite(Request $request) {
                  
                //Check Validation          
                $validation = Validator::make($request->all(),['invite_id' => 'required']);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
                $user_id = $request->user_data->user_id;

                UserInvite::where('id', $request->invite_id)->delete();

                //Send JSON response
                return response()->json('User Invitation Deleted.',200); 
            }

        /*---------------------------------
            * Version 3
        ----------------------------------*/

            //Api invite user
            public static function apiAddFile(Request $request) {
                  
                //Check Validation          
                $validation = Validator::make($request->all(),[
                    'state_id' => 'required',
                    'county_id' => 'required',
                    'city_id' => 'required',
                    'file' => 'required'
                ]);

                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                
                $user_id = $request->user_data->user_id;

                $upload = new Upload;
                $upload->user_id = $user_id;
                $upload->state_id = $request->state_id;
                $upload->county_id = $request->county_id;
                $upload->city_id = $request->city_id;
                $upload->file = $request->file;
                $upload->save();

                $upload->city_name = City::where('id', $request->city_id)->value('name');
                $upload->county_name = County::where('id', $request->county_id)->value('name');
                $upload->state_name = State::where('id', $request->state_id)->value('name');

                //Send JSON response
                return response()->json($upload,200); 
            }

            //Api delete invite
            public static function apiDeleteFile(Request $request) {
                  
                //Check Validation          
                $validation = Validator::make($request->all(),['file_id' => 'required']);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
                // $user_id = $request->user_data->user_id;

                Upload::where('id', $request->file_id)->delete();

                //Send JSON response
                return response()->json('File Deleted.',200); 
            }

    /*-----------------------------------------------------
    -------------------------------------------------------
        * Subscription Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api Add Subscription
        public static function apiAddSubscription(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiAddSubscriptionRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);


            $user_id = $request->user_data->user_id;
            $current_time = Carbon::now();
            $current_time1 = Carbon::now();
            // $subscription = new Carbon('last day of this month');
            // $subscription = $subscription->toDateTimeString();
            $days = $current_time->daysInMonth;
            $start = new Carbon('first day of last month');
            $end = new Carbon('last day of last month');
            $start = $start->toDateTimeString();
            $end = $end->toDateTimeString();
            $day = $current_time->day;
            // dd($day);

            $user = User::where([['id', $user_id]])->first();
            $email = $user->email;

            $dbEmail = Email::where('id',1)->first();
            $client_email = $dbEmail->client_email;
            $test_email = $dbEmail->test_email;
            
            if(!empty($user)) {

                //count to check amount for subscription
                $count = Property::where([
                                ['is_subject_prop', 1],
                                ['agent_id', $user_id],
                                ['type', '<>', 4],
                                ['created_at', '>=', $start]
                            ])->orWhere([
                                ['is_subject_prop', 1],
                                ['agent_id', $user_id],
                                ['type', '<>', 4],
                                ['created_at', '<=', $end]
                            ])->count('id');
                
                
                $amount = 7500;
                $stripe = CommonFunction::addSubscriptionCard($request, $amount);
                if($stripe != 'payment_success') {
                    return $stripe;
                }
                //inserting in payment
                $payments = new Payment;
                $payments->agent_id = $user_id;
                $payments->payment_type = 6;
                $payments->amount = $amount/100;
                $payments->payment_status = 2;
                $payments->save();

                User::where('id', $user_id)->update(['subscribed_until' => $current_time1->addMonth()]);
                
                $mail = Mail::send("emails.user-subscription",['request'=>$user], function ($m) use ($email) {
                    $m->to($email)->subject("Monthly subscription successful");
                });
                $mail = Mail::send("emails.user-admin-subscription",['request'=>$user], function ($m) use ($client_email) {
                    $m->to($client_email)->subject("New agent subscriber");
                });
                if($test_email != ''){
                $mail = Mail::send("emails.user-admin-subscription",['request'=>$user], function ($m) use ($test_email) {
                    $m->to($test_email)->subject("New agent subscriber");
                });
                }
                return response()->json('subscription complete',200); 
            }
            else {
                return response()->json('error while subscription',400); 
            }
            
            
        }

        //Api Cancel Subscription
        public static function apiCancelSubscription(Request $request) {

            $user_id = $request->user_data->user_id;
            $userdata = User::where('id',$user_id)->first();
            $dbEmail = Email::where('id',1)->first();
            $testemail = $dbEmail->test_email;
            $client_email = $dbEmail->client_email;
            $useremail = $userdata->email;
            
            $mail = Mail::send("emails.user-cancel-subscription",['request'=>$userdata], function ($m) use ($useremail) {
                $m->to($useremail)->subject("Cancelled Subscription");
            });
            $mail = Mail::send("emails.admin-cancel-subscription",['request'=>$userdata], function ($m) use ($client_email) {
                $m->to($client_email)->subject("Cancelled Subscription");
            });
            $mail = Mail::send("emails.admin-cancel-subscription",['request'=>$userdata], function ($m) use ($testemail) {
                $m->to($testemail)->subject("Cancelled Subscription");
            });

            User::where('id', $user_id)->update(['subscribed_until' => NULL]);
            $stripe_data =  UserStripe::where('user_id', $user_id)->first();

            
            //cancel subscription
            \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));

            $sub = \Stripe\Subscription::retrieve($stripe_data->subscription_id);
            // $date = $sub["current_period_end"];
            // $date = date("Y-m-d H:i:s", $date);
            // dd($date);
            $sub->cancel();

            UserStripe::where('user_id', $user_id)->update(['subscription_id' => NULL]);

            
            return response()->json('Subscription cancelled',200);     
                    
        }

        //Api Renew Subscription   
        public static function apiRenewSubscription() {

            $current_time = Carbon::now();
            
            $data = User::join('user_stripes', 'users.id', '=', 'user_stripes.user_id')
                        ->select('users.id', 'users.credits', 'user_stripes.customer_id', 'user_stripes.card_id', 'users.subscribed_until')
                        ->where('users.subscribed_until', '<=', $current_time)
                        ->get();

            $user_ids = User::where('subscribed_until', '<=', $current_time)->pluck('id');
            $start = new Carbon('first day of last month');
            $end = new Carbon('last day of last month');
            $start = $start->toDateTimeString();
            $end = $end->toDateTimeString();

            foreach($data as $key => $value) {
                
                //count to check amount for subscription
                $count = Property::where([
                    ['is_subject_prop', 1],
                    ['agent_id', $value->id],
                    ['type', '<>', 4],
                    ['created_at', '>=', $start]
                ])->orWhere([
                    ['is_subject_prop', 1],
                    ['agent_id', $value->id],
                    ['type', '<>', 4],
                    ['created_at', '<=', $end]
                ])->count('id');
                    
                $amount = 7500;
                // if($count >= 10) {
                //     // $amount = 4500;
                //     User::where('id', $value->id)->update(['credits' => $value->credits + 5]);
                // }    

                $stripe_data =  UserStripe::where('user_id', $value->id)->first();

                if($stripe_data->subscription_id != NULL){            
                    //cancel subscription
                    \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
        
                    $sub = \Stripe\Subscription::retrieve($stripe_data->subscription_id);
                    $date = $sub["current_period_end"];
                    $date = date("Y-m-d H:i:s", $date);
                    // dd($date);

                    if($value->subscribed_until != $date) {

                        $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $value->subscribed_until)->subMonth()->toDateTimeString();

                        //inserting in payment
                        $payments = new Payment;
                        $payments->agent_id = $value->id;
                        $payments->payment_type = 6;
                        $payments->amount = $amount/100;
                        $payments->save();

                        $data1 = User::join('user_stripes', 'users.id', '=', 'user_stripes.user_id')
                            ->select('users.*', 'user_stripes.customer_id', 'user_stripes.card_id')
                            ->where('users.id', $value->id)
                            ->first();
                        
                        $payment_data = Payment::orderBy('payment_type', 'desc')->where('agent_id', $value->id)->where('payment_status', 1)->whereIn('payment_type', [4,6])->get();
                        $total_amount = Payment::where('agent_id', $value->id)->where('payment_status', 1)->whereIn('payment_type', [4,6])->sum('amount');

                        $amount = $total_amount*100;
                        
                        // Send Email
                        $mail = Mail::send("emails.agent-payment", ['data1' => $data1, 'payment_data' => $payment_data, 'total_amount' => $total_amount, 'start' => $start_date, 'end' => $value->subscribed_until], function ($m) use ($data1) {
                            $m->to($data1->email)->subject("Payment Invoice - HomeEase");
                        });


                        Payment::where('payment_status', 1)->where('agent_id', $value->id)->whereIn('payment_type', [4,6])->update(['payment_status' => 2]);
                    }

                    User::where('id', $value->id)->update(['subscribed_until' => $date]);
                }
            }

            return 'success';
            
        }

    /*-----------------------------------------------------
    -------------------------------------------------------
        * User & Agent Functions
    -------------------------------------------------------
    ------------------------------------------------------- */


        // Api Profile User
        public static function apiProfileUser(Request $request) { 

            $user_id = $request->user_data->user_id;

            // Get User Details
            $user_details = User::getUserDetails($user_id, $request->fcm_id);
            //Send JSON response   
            return response()->json($user_details,200);                    
        } 

        public static function specialLogin(Request $request) {
            
            //Check Validation
            $input = $request->all();
            $validation = Validator::make($input, User::$apiLoginRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
            $email = $request->email;
            $password = $request->password;          
            
            
            // Check if Fcm Id provided
            if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
                $fcm_id = $request->fcm_id;
                            
                UserDevice::where('fcm_id', $fcm_id)->delete();
            }   
            else {
                $fcm_id = NULL;
            }               
    
            $user_data = User::where('email', $email)->first();            
    
            if(empty($user_data)) 
                return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid Email id'], 400);
    
            if($user_data->deleted_at!=null)
                return response()->json(['error' => 'bad_request', 'error_description' => 'This user is deactivated'], 400);
            elseif($user_data->blocked_at!=null)
                return response()->json(['error' => 'bad_request', 'error_description' => 'This user is blocked by admin'], 400);
            else {
                if((Hash::check($password, $user_data->password)) && $user_data->id == 1) {
                    
                    // Add Edit User Device
                        $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $user_data->id);
                        if($add_edit_user_device_response!="add_edit_user_device_success")
                            return $add_edit_user_device_response;  
                    // Get User Details
                    $user_details = User::getUserDetails($user_data->id, $fcm_id);
                        
                    //Send JSON response
                    return response()->json($user_details,200);     
                }
                else
                    return response()->json(['error' => 'bad_request', 'error_description' => 'Password incorrect'], 400);
            }                           
                
        }   

      
        // Api Login
        public static function apiLogin(Request $request) {
            //Check Validation

            $input = $request->all();
            $validation = Validator::make($input, User::$apiLoginRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
            $email = $request->email;
            $password = $request->password;          
           
            
            
            // Check if Fcm Id provided
            if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
                $fcm_id = $request->fcm_id;
                            
                UserDevice::where('fcm_id', $fcm_id)->delete();
            }   
            else {
                $fcm_id = NULL;
            }               
    
            $user_data = User::where('email', $email)->first();            
    
            if(empty($user_data)) 
                return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid Email id'], 400);
    
            if($user_data->deleted_at!=null)
                return response()->json(['error' => 'bad_request', 'error_description' => 'This user is deactivated'], 400);
            elseif($user_data->blocked_at!=null)
                return response()->json(['error' => 'bad_request', 'error_description' => 'This user is blocked by admin'], 400);
            else {
                
                if((Hash::check($password, $user_data->password)) && $user_data->id != 1) {
                    
                    // Add Edit User Device
                        $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $user_data->id);
                        if($add_edit_user_device_response!="add_edit_user_device_success")
                            return $add_edit_user_device_response;  
                    // Get User Details
                    $user_details = User::getUserDetails($user_data->id, $fcm_id);
                        
                    //Send JSON response
                    return response()->json($user_details,200);     
                }
                else
                    return response()->json(['error' => 'bad_request', 'error_description' => 'Password incorrect'], 400);
            }                           
                
        }   

        // Api Change Password
        public static function apiChangePassword(Request $request){   

            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiChangePasswordRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            $user_id = $request->user_data->user_id;            
            $password = Hash::make($request->password);
     
            User::where('id', $user_id)->update(['password' => $password]);

            return response()->json(['message' => 'Password Updated'], 200);
 
        }

        // Api Logout
        public static function apiLogout(Request $request){        
            $user_id = $request->user_data->user_id;
            $fcm_id = $request->user_data->fcm_id;

            UserDevice::where('user_id', $user_id)->where('fcm_id', $fcm_id)->delete();
                    
            return response()->json(['message' => 'Successfully logged out'], 200);
        }
        
        // Api Deactivate Account
        public static function apiDeactivateAccount(Request $request) {   

            $user_id = $request->user_data->user_id;   
            $current_time = Carbon::now();

            User::where('id', $user_id)->update(['deleted_at' => $current_time]);
            
            //Send JSON response
            return response()->json(array('data' => 'Account Deleted'),200);                    
        }

        // Api Forgot Password
        public static function apiForgotPassword(Request $request){    

            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiForgotPasswordRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            $email = $request->email;
            $url = $request->url;
        
            $user_data = User::where('email', $email)->first();
            if(empty($user_data))
                return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid Email'], 400);
        
            $token = str_random(30);
            $user_id = $user_data->id;
        
            // Clear old record for this email
            PasswordReset::where('email', $email)->delete();
        
            // Inserting new record
            $new_password_reset = new PasswordReset;
            $new_password_reset->user_id = $user_id;
            $new_password_reset->email = $email;
            $new_password_reset->token = $token;           
            $new_password_reset->save();
        
            // Send Email
            $mail = Mail::send("emails.forgot-password", ['name' => $user_data, 'token' => $token, 'url' => $url], function ($m) use ($email) {
                $m->to($email)->subject("Forgot Password - HomeEase");
            });
                            
            return response()->json(['data' => 'Email has been sent on your email id.'], 200);
        } 
        
        // Api Reset Password
        public function apiResetPassword(Request $request) {
        
            //Check Validation          
            $validation = Validator::make($request->all(), User::$apiResetPasswordRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                    
            $password_reset_data = PasswordReset::where('token', $request->token)->first();
            if(empty($password_reset_data))
                return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid Token'], 400);
        
            // Check if user is valid with valid token            
            if($request->password==$request->password_confirmation) {
                $password = Hash::make($request->password);                
                User::where('id', $password_reset_data->user_id)->update(['password' => $password]);
                PasswordReset::where('token', $request->token)->delete();
                return response()->json(['data' => 'Password Updated'], 200);
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'Password do not match'], 400);
            }                        
        
        }

        // Api Edit Profile
        public static function apiEditProfile(Request $request) {   

            $user_id = $request->user_data->user_id;  
            $url = $request->url;
            $user = User::where('id', $user_id)->first();
            $current_time = Carbon::now();
            $preferred_cities = [];
            $preferred_counties = [];
            $preferred_states = [];
            // $cities_insert = [];
           
            // Manage Image
            if(!empty($request->image)) 
                $image = CommonFunction::extractImageName($request->image);

            else {
                if(strpos($user->image, 'default.png') !== false) {
                    $image = "";
                }
                else{
                    $image = CommonFunction::extractImageName($user->image);
                }
            }

            if(isset($request->cities)) {
                $cities = json_decode($request->cities);        
                foreach ($cities as $key => $value) {
                    $state_id = 0;
                    $county_id = 0;
                    $state = State::where('name', $value->state_name)->first();
                    if($state) {
                        $state_id = $state->id;
                        $county = County::where('name', $value->county_name)->where('state_id', $state_id)->first();
                        if($county) {
                            $county_id = $county->id;
                        }
                    }
                    
                    if($state_id == 0 || $county_id == 0) {
                       
                            $already_added = ExtraCity::where('user_id', $user_id)->where('county', $value->county_name)->where('state', $value->state_name)->first();
                            if(!$already_added) {
                                $cities_insert[] = array(
                                    'user_id' => $user_id, 
                                    'state' => $value->state_name,
                                    'county' => $value->county_name,
                                    'created_at' => $current_time,
                                    'updated_at' => $current_time,
                                );
                            }
                    }
                    else {
                        $preferred_counties[] = $county_id;
                        $preferred_states[] = $state_id;

                    }
                 
                }
                if(!empty($cities_insert)) {
                    DB::table('extra_cities')->insert($cities_insert);
                }
            }

            $preferred_counties = array_unique($preferred_counties);
            $preferred_states = array_unique($preferred_states);


            if($request->user_data->user->user_type == 1) {

                $is_paid_user = isset($request->is_paid_user) ? $request->is_paid_user : $user->is_paid_user;
                $have_agent = isset($request->have_agent) ? $request->have_agent : $user->have_agent;
                $agent_email = isset($request->agent_email) ? $request->agent_email : ((isset($request->agent_email) == "") ? NULL : $user->agent_email); 
                $mls_id = NULL;
                $licence_type = NULL;
                $estate_type = NULL;      
                $pref_cities = NULL;
                $pref_counties = NULL;
                $pref_states = NULL;
                $all_city_data = NULL;      
            }
            elseif($request->user_data->user->user_type == 2) {
                $is_paid_user = NULL;
                $have_agent = NULL;
                $agent_email = NULL; 
                $mls_id = isset($request->mls_id) ? $request->mls_id : $user->mls_id;
                $licence_type = isset($request->licence_type) ? $request->licence_type : $user->licence_type;
                $estate_type = isset($request->estate_type) ? $request->estate_type : json_encode($user->estate_type);    
                $pref_cities = isset($request->cities) ? json_encode($preferred_cities) : json_encode($user->preferred_cities);
                $pref_counties = isset($request->cities) ? json_encode($preferred_counties) :json_encode($user->preferred_counties);
                $pref_states = isset($request->cities) ? json_encode($preferred_states) :json_encode($user->preferred_states);
                $all_city_data = isset($request->cities) ? $request->cities :json_encode($user->cities);       
            }


            User::where('id', $user_id)->update([
                'first_name' => isset($request->first_name) ? $request->first_name : $user->first_name,
                'last_name' => isset($request->last_name) ? $request->last_name : $user->last_name,
                'image' => $image,
                'mls_id' =>$mls_id,
                'licence_type' => $licence_type,
                'estate_type' => $estate_type,      
                'preferred_cities' => $pref_cities,
                'preferred_counties' => $pref_counties,
                'preferred_states' => $pref_states,
                'cities' => $all_city_data,
                'credits' => isset($request->credits) ? $request->credits : $user->credits,
                'is_paid_user' => $is_paid_user,
                'have_agent' => $have_agent,
                'agent_email' =>  $agent_email              
            ]);
            $user1 = User::where('id', $user_id)->first();
            $user_name = $user1->first_name." ".$user1->last_name;

            // Get User Details
            $user_details = User::getUserDetails($user_id);

            //Send JSON response
            return response()->json($user_details,200);                    
        }

        /*=======Apartment v3 Connect button======*/

        public function apartmentConnect(Request $request)
        {
            $validation = Validator::make($request->all(), array('first_name' => 'required','last_name' => 'required', 'email' => 'required|email','phone_no'=> 'required','property_address'=> 'required','how_soon' => 'required', 'question' => 'required', 'pref_communication' => 'required'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);  
                
            $dbEmail = Email::where('id',1)->first();
     
            $client_email = $dbEmail->client_email;
            $test_email = $dbEmail->test_email;
            // Send Email
            if($test_email != ''){
            $mail = Mail::send("emails.apartment-connect", [ 'request' => $request ], function ($m) use ($test_email) {
                $m->to($test_email)->subject("Apartment Connect");
            });
        }
            $mail = Mail::send("emails.apartment-connect", [ 'request' => $request ], function ($m) use ($client_email) {
                $m->to($client_email)->subject("Apartment Connect");
            });
        
            return response()->json(['data' => "We are excited to help you succeed in real estate! We will contact you soon, so keep an eye out for HomeEase on
            your phone and email."], 200);
        
        }

        /*=======Apartment v3 apartment Join Contact List=========*/

        public function apartmentJoinContactList(Request $request)
        {
            $validation = Validator::make($request->all(), array('first_name' => 'required','last_name' => 'required', 'email' => 'required|email','phone_no'=> 'required','city'=>'required','state'=> 'required','how_soon' => 'required', 'price_range' => 'required','credit_score'=>'required','pref_communication' => 'required'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);   
            
            $dbEmail = Email::where('id',1)->first();
            $client_email = $dbEmail->client_email;
            $test_email = $dbEmail->test_email;
                    
            // Send Email
            $mail = Mail::send("emails.join-contact-list", [ 'request' => $request ], function ($m) use ($client_email) {
                $m->to($client_email)->subject("Join Contact List");
            });
if($test_email != ''){
            $mail = Mail::send("emails.join-contact-list", [ 'request' => $request ], function ($m) use ($test_email) {
                $m->to($test_email)->subject("Join Contact List");
            });
        }
            return response()->json(['data' => "We are excited to help you succeed in real estate! We will be in touch when opportunities come available, so keep
            an eye out for HomeEase on your phone and email."], 200);
        
        }
        /*=======Apartment v5 apartment start pricing=========*/

        public function apartmentStartPricing(Request $request)
        {
            
            $email = $request->email;
            if($request->user_id == '')
            {
                $checkEmail = User::where('email',$request->email)->first();
                if($checkEmail != '')
                {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'This email already exist. Please login to start pricing'], 400);
                    
                }
                else
                {  
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr( str_shuffle( $chars ), 0, 8 ); 
                    $url = 'https://homeease.pro/broker/agent'; 

                    if(isset($request->fcm_id) && !empty($request->fcm_id)) {                    
                        $fcm_id = $request->fcm_id;
                    
                        UserDevice::where('fcm_id', $fcm_id)->delete();
                    }   
                    else {
                        $fcm_id = NULL;
                    }
                    $data['new_signup']   = true;  
                    $new_user = new User;
                    $new_user->email = $request->email;
                    $new_user->first_name = $request->first_name;
                    $new_user->last_name = $request->last_name;
                    $new_user->user_type = 1; // 1 - User, 2 - Agent
                    $new_user->password = Hash::make($password);
                    $new_user->image = isset($request->image) ? $request->image : "";
                    $new_user->have_agent = 0;
                    $new_user->agent_email = NULL;
                    $new_user->save();
                    
                    //send email to new user start apartment pricing
                    $mail = Mail::send("emails.apartment-new-user", [ 'request' => $request,'password'=>$password ], function ($m) use ($email) {
                        $m->to($email)->subject("Welcome to HomeEase!");
                    });
                    //send sms to new user start apartment pricing
                    if($request->country_code && $request->phone_number) {
                        $content = 'Your HomeEase Login Email : '.$request->email.' Password : '.$password;
                        CommonFunction::sendSms($request->country_code,$request->phone_number,$content);
                    }
                    $add_edit_user_device_response = CommonFunction::addEditUserDevice($request, $new_user->id);
                    $user_id = $new_user->id;
                }
            }
            else
            {
                $user_id = $request->user_id;
                $getFcm = UserDevice::where('user_id',$user_id)->first();
                if($getFcm == '')
                {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'Session Expired!'], 400);
                }
                else
                {
                    $fcm_id = $getFcm->fcm_id;
                }
                $getCoupon = couponDiscount::where('user_id',$request->user_id)->get();
                if(count($getCoupon) == 0){
                    $data['new_signup']   = true;
                }
            }
            $user = User::where('id', $user_id)->first();
            $client_email = 'gurjeevan.kaur.henceforth@gmail.com';
            $city = City::where('name', 'like', $request->city)->value('id');
            $county = County::where('name', 'like', $request->county)->value('id');
            $state = State::where('name', 'like', $request->state)->value('id');
            if($user->user_type == 2) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'Agent can not start pricing.'], 400);
            }
            $agent_id = NULL;
            if($request->lat == NULL && $request->lng == NULL) {
                $lat = 0; 
                $lng = 0;
            }
            else {
                $lat = $request->lat;
                $lng = $request->lng;
            }
            if($request->type !=4) {
                $validation = Validator::make($request->all(), User::$apiAgencyTypeRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            }
            if($state && $county) {
                if(!$city) {
                    // new code 
                    $city1 = new City;
                    $city1->county_id = $county;
                    $city1->name = $request->city;
                    $city1->above_sqft = 14;
                    $city1->below_sqft = 7;
                    $city1->year_build = 2000;
                    $city1->garage1 = 5000;
                    $city1->garage2 = 2500;
                    $city1->heat = 1000;
                    $city1->ac = 2500;
                    $city1->fire_place = 2000;
                    $city1->porch = 1000;
                    $city1->bedroom_above = 5000;
                    $city1->bedroom_below = 3000;
                    $city1->bathroom_above = 4000;
                    $city1->bathroom_below = 2500;
                    $city1->quality = 7;
                    $city1->condition_value = 3;
                    $city1->save();

                    $city = $city1->id;

                }
            }
                
            $type = 4;
            $new_property = new Property;
            $new_property->user_id = $user->id;
            $new_property->is_subject_prop = 1;
            $new_property->type = $type;
            $new_property->phone_number = $request->phone_no;
            $new_property->preferred_communication = $request->preferred_communication;
            $new_property->address_1 = $request->address_1;
            $new_property->address_2 = isset( $request->address_2) ? $request->address_2 : NULL;
            $new_property->city = $request->city;
            $new_property->county = $request->county;
            $new_property->state = $state;
            $new_property->zip_code = $request->zip_code;
            $new_property->country = $request->country;
            $new_property->lat = $lat;
            $new_property->lng = $lng;
            $new_property->country_code = $request->country_code;
            $new_property->country_abv = $request->country_abv;
            $new_property->how_soon = $request->how_soon;
            $new_property->credit_score = $request->credit_score;
            $new_property->prequalified = $request->prequalified;
            $new_property->progress_status = 0;
            $new_property->selling_status = $request->selling_status;
            $new_property->status = 0;
            $new_property->step1 = 1;
            $new_property->save();

            if($type==4) {
                $apartment = new Apartment;
                $apartment->id = $new_property->id;
                $apartment->save();

                Property::where('id', $new_property->id)->update([
                    'status' => 7,
                    'agency_type' => $request->agency_type,
                    'agent_id'=>1
                ]);
            }
            if($request->agency_type == 4) {
                User::where('id', $user->id)->update([
                    'is_agent' => 1
                ]);
            }
            $property = Property::where('id', $new_property->id)->first();
            $property->user_detail = User::getUserDetails($new_property->user_id,$fcm_id);
            

            $data['property'] = $property;
            $data['property_id'] = $new_property->id;
            $data['p_type'] = 'Property';
            
            // //Start email for apartment start pricing
            // $mail = Mail::send("emails.apartment-pricing",['request'=>$request,'userData'=>$user], function ($m) use ($email) {
            //     $m->to($email)->subject("Let’s price your property!");
            // });
            // //End email for apartment start pricing
            
            return response()->json($data,200); 
        }

        /*===========Send email if start pricing step incomplete============*/

        public function incompletePricing(Request $request){
            $property_id = $request->property_id;
            $p_type = $request->p_type;
            $dbEmail = Email::where('id',1)->first();
            if($p_type == 'Extra'){
                $property = ExtraProperty::orderBy('id', 'desc')->where('id',$property_id)->first();
            }
            else{
                $property = Property::orderBy('id', 'desc')->where('id',$property_id)->first();
            }
            $user = User::where('id',$property->user_id)->first();
            $client_email = $dbEmail->client_email;
            $test_email = $dbEmail->test_email;
            $email = $user->email;
            
            //incomplete start pricing email
            $mail = Mail::send("emails.incomplete-start-pricing", ['user' => $user], function ($m) use ($email) {
                $m->to($email)->subject("We are waiting to help you price your property!");
            });
            // $mail = Mail::send("emails.incomplete-start-pricing", ['user' => $user], function ($m) use ($test_email) {
            //     $m->to($test_email)->subject("We are waiting to help you price your property!");
            // });
            // $mail = Mail::send("emails.incomplete-start-pricing", ['user' => $user], function ($m) use ($client_email) {
            //     $m->to($client_email)->subject("We are waiting to help you price your property!");
            // });
            if($test_email != ''){
            $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$user], function ($m) use ($test_email) {
                $m->to($test_email)->subject("Incomplete pricing!");
            });
            $mail = Mail::send("emails.apartment-start-pricing",['request'=>$property,'userData'=>$user], function ($m) use ($client_email) {
                $m->to($client_email)->subject("Incomplete pricing!");
            });
            }
            //incomplete start pricing sms
            if($property->country_code && $property->phone_number) {
                $getContent = CommonFunction::smsContent();
                $content = $getContent['incompletePricing'];
                CommonFunction::sendSms($property->country_code,$property->phone_number,$content);
            }
            
            Property::where('id', $property_id)->update([
                'cron_status' => 1,
            ]);
            return response()->json('Email sent successfully',200); 
        }  
        public function incompleteCalculation(Request $request){
            $property_id = $request->property_id;
            $type = $request->type;
            $property = Property::where('id', $property_id)->first();
            $user = User::where('id',$property->user_id)->first();
            //incomplete calculation sms
            if($property->country_code && $property->phone_number) {
                $getContent = CommonFunction::smsContent();
                $content = $getContent['incompleteCal'];
                CommonFunction::sendSms($property->country_code,$property->phone_number,$content);
            }
            if($type == 1){
                Property::where('id', $property_id)->update([
                    'step1_status' => 1,
                ]);
            }
            if($type == 2){
                Property::where('id', $property_id)->update([
                    'step2_status' => 1,
                ]);
            }
            if($type == 3){
                Property::where('id', $property_id)->update([
                    'step3_status' => 1,
                ]);
            }
            if($type == 4){
                Property::where('id', $property_id)->update([
                    'step4_status' => 1,
                ]);
            }
            
            return response()->json('Sms sent successfully',200); 
        } 

        /*==============Cron for incomplete property==============*/

        public function sendEmail(Request $request)
        {
            $property = Property::orderBy('id', 'desc')->where('status', '<>', 3)->where('cron_status',0)->where('agency_type', NULL)->where('deleted_at', NULL)->where('blocked_at', NULL)->get();
            $dbEmail = Email::where('id',1)->first();
            foreach($property as $val)
            {
                $user_id = $val->user_id;
                
                $user = User::where('id',$user_id)->first();
                if($user!= '')
                {
                    $email = $user->email;
                    $from = "homeease@gmail.com";
                    
                    $client_email = $dbEmail->client_email;
                    $test_email = $dbEmail->test_email;

                    $message = "We will contact you soon!";

                    //incomplete start pricing email
                    $mail = Mail::send("emails.incomplete-start-pricing", ['user' => $user], function ($m) use ($client_email) {
                        $m->to($client_email)->subject("We are waiting to help you price your home!");
                    });
                    if($test_email != ''){
                    $mail = Mail::send("emails.incomplete-start-pricing", ['user' => $user], function ($m) use ($test_email) {
                        $m->to($test_email)->subject("We are waiting to help you price your home!");
                    });
}
                    //incomplete start pricing sms
                    if($val->country_code && $val->phone_number) {
                        $getContent = CommonFunction::smsContent();
                        $content = $getContent['incompletePricing'];
                        CommonFunction::sendSms($val->country_code,$val->phone_number,$content);
                    }

                    Property::where('id', $val->id)->update([
                        'cron_status' => 1,
                    ]);
                    echo "The email message was sent.";
                }
            }
        }
}