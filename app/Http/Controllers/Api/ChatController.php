<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Library\CommonFunction;
use App\Models\Property;
use App\Models\Chat;
use App\Models\Payment;
use Carbon\Carbon;
use App\Models\User;

class ChatController extends Controller
{
    /*-----------------------------------------------------
    -------------------------------------------------------
        * Chat Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        // Api Send Message
        public static function apiSendMessage(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiSendMessageRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $send = 0;
            $property = Property::where('id', $request->property_id)->first();
            $sent_to = $request->sent_to;
            $sent_by_admin = 0;            
            $current_time = Carbon::now();
            $admin_id = NULL;
            if($property->type == 4) {
                $admin_id = 1;
                if(!empty($request->header('Authorization'))) {
                    $access_token = $request->header('Authorization');
                    $user_data = CommonFunction::validateAccessToken($access_token);
                    if($user_data == NULL) {
                        $admin_data = CommonFunction::validateAdminAccessToken($access_token);
                        if($admin_data == NULL) {
                            return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                        }
                        $admin_id = $admin_data->id;

                        //Check Validation          
                        $validation = Validator::make($request->all(), Chat::$apiSendByAdminRules);
                        if($validation->fails())
                            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                            
                        $user_id = NULL;
                        $sent_by_admin = 1;
                    }
                    else {

                        $user_id = $user_data->user_id;
                        $sent_by_admin = 0;
                        $sent_to = NULL;

                        $auto_text = Chat::orderBy('id', 'desc')->where('admin_id', $admin_id)->where('sent_by', $user_id)->where('sent_to', $sent_to)->where('property_id', $request->property_id)->where('sent_by_admin', 0)->first();

                        if(empty($auto_text)) {
                            $send = 1;
                        }
                        else {
                            $send = 0;
                        }
                    }
                    
                }
                else {
                    return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                }
            }
            else {

                //Check Validation          
                $validation = Validator::make($request->all(), Chat::$apiSendByAdminRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                if(empty($request->header('Authorization'))) {
                    return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                }
                else {

                    $access_token = $request->header('Authorization');
                    $user_data = CommonFunction::validateAccessToken($access_token);

                    if($user_data == NULL) {
                        return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                    }
                }
                $user_id = $user_data->user_id;
                
                // $paid = Payment::where([
                //     ['user_id', $request->sent_to],
                //     ['agent_id', $user_id],
                //     ['property_id', $request->property_id],
                //     ['payment_type', 4]
                // ])->orWhere([
                //     ['user_id', $user_id],
                //     ['agent_id', $request->sent_to],
                //     ['property_id', $request->property_id],
                //     ['payment_type', 4]
                // ])->first();
    
                // if(empty($paid)) {
                //     return response()->json(['error' => 'bad_request', 'error_description' => 'You need to complete payment to start a conversation.'], 400); 
                // }
            }
            
            $msg_type = $request->msg_type;    //0-msg, 1-file
            $zone = isset($request->zone) ? $request->zone : "-04:00";

            //message
            if($msg_type==0) {

                $msg = $request->msg;
                if(empty($msg)){
                    return response()->json(['error' => 'bad_request', 'error_description' => 'message is required'], 400);
                }
            }
            else {
                $msg = NULL;
            }

            //file
            if($msg_type==1) {

                if(!isset($request->file) || empty($request->file))
                    return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid File'], 400);
                else {
                    $files_json_arr = json_decode($request->file);
                    foreach($files_json_arr as $key => $value) {
                        $value->file = CommonFunction::extractFileName($value->file);
                    }
                    $file_data = json_encode($files_json_arr);
                }
                                                 
            }  
            else {
                $file_data = NULL;
            }



            $new_chat = new Chat;
            $new_chat->admin_id = $admin_id;
            $new_chat->sent_by = $user_id;
            $new_chat->sent_to = $sent_to;
            $new_chat->property_id = $request->property_id;
            $new_chat->sent_by_admin = $sent_by_admin;
            $new_chat->msg_type = $request->msg_type;
            $new_chat->msg = $msg; 
            $new_chat->file = $file_data; 
            $new_chat->save();

            $messages = Chat::getSentMessage($new_chat->id, $user_id, $sent_to, $request->property_id, $zone);      

            $recent = Chat::orderBy('id', 'desc')->skip(1)->selectRaw("*, if(TIMESTAMPDIFF(MINUTE, chats.created_at, '$current_time')>10, 1, 0) as away")
            ->where([
                ['admin_id', $messages->admin_id],
                ['sent_by', $messages->sent_by],
                ['sent_to', $messages->sent_to],
                ['property_id', $messages->property_id]
            ])->orWhere([
                ['admin_id', $messages->admin_id],
                ['sent_by', $messages->sent_to],
                ['sent_to', $messages->sent_by],
                ['property_id', $messages->property_id]
            ])->first();
            // ->where('admin_id', $messages->admin_id)->where('sent_by', $messages->sent_by)->where('sent_to', $messages->sent_to)->where('property_id', $messages->property_id)->first();

            // return $recent;

            if(!empty($recent)) {
                if($recent->away == 1) {
                    if($messages->sent_to != null) {
                        $user = User::where('id',$messages->sent_to)->first();
                        $name = $user->first_name." ".$user->last_name;
                    }
                    else {
                        $user = Admin::where('id',$messages->admin_id)->first();
                        $name = "Admin";
                    }

                    // Send Email
                    $mail = Mail::send("emails.msg-notif", ['name' => $name, 'recent' => $messages], function ($m) use ($user, $messages) {
                        $m->to($user->email)->subject("Message Received");
                    });
                }
            }
            else {
                if($messages->sent_to != null) {
                    $user = User::where('id',$messages->sent_to)->first();
                    $name = $user->first_name." ".$user->last_name;
                }
                else {
                    $user = Admin::where('id',$messages->admin_id)->first();
                    $name = "Admin";
                }

                // Send Email
                $mail = Mail::send("emails.msg-notif", ['name' => $name, 'recent' => $messages], function ($m) use ($user, $recent) {
                    $m->to($user->email)->subject("Message Received");
                });
            }

            if($send == 1) {

                $auto = new Chat;
                $auto->admin_id = 1;
                $auto->sent_by = null;
                $auto->sent_to = $user_id;
                $auto->property_id = $request->property_id;
                $auto->sent_by_admin = 1;
                $auto->msg_type = 0;
                $auto->msg = "Ben is away from the computer right now...probably out working hard to close a deal! He just received an email that you would like to talk with him. Surely, he will respond soon! You will receive a email notification when he does. If you would like to receive a call or text, just type in your phone number and mention 'call' or 'text'. Thank you!"; 
                $auto->save();
            }


            // $orders = Order::selectRaw("orders.id, orders.assigned_at, if(TIMESTAMPDIFF(HOUR, orders.assigned_at, '$current_time')>'$status->withdraw_hours', 1, 0) as withdraw")->where('confirm_received', 0)->where('assigned_at', '<>', NULL)->where('status', 1)->where('finished_at', NULL)->where('draft', NULL)->having('withdraw', 1)->get();

      
            return response()->json($messages, 200);
        }

        // Api Get Recent Messages
        public static function apiGetRecentMessages(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiGetRecentMessagesRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            if(empty($request->header('Authorization'))) {

                return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
            }
            else {

                $access_token = $request->header('Authorization');
                $user_data = CommonFunction::validateAccessToken($access_token);

                if($user_data == NULL) {
                    $admin_data = CommonFunction::validateAdminAccessToken($access_token);
                    if($admin_data == NULL) {
                        return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                    }
                    else {
                        $admin_id = $admin_data->id;

                        //Check Validation          
                        $validation = Validator::make($request->all(), Chat::$apiSendByAdminRules);
                        if($validation->fails())
                            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                        $user_id = NULL;
                        $sent_to = $request->sent_to;
                    }
                    
                }
                else {
                    $user_id = $user_data->user_id;      
                }

                if(empty($request->sent_to)) {
                    $sent_to = NULL;
                }
                else {
                    $sent_to = $request->sent_to;
                }
            }
        
                    
            $zone = isset($request->zone) ? $request->zone : "+05:30";    
                    
        
            $messages = Chat::getRecentMessages($user_id, $sent_to, $request->property_id, $zone);            
            return response()->json($messages, 200);
        
        }

        // Api Previous Messages
        public static function apiPreviousMessages(Request $request) {
    
            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiPreviousMessagesRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        
            if(empty($request->header('Authorization'))) {

                return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
            }
            else {

                $access_token = $request->header('Authorization');
                $user_data = CommonFunction::validateAccessToken($access_token);

                if($user_data == NULL) {
                    $admin_data = CommonFunction::validateAdminAccessToken($access_token);
                    if($admin_data == NULL) {
                        return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                    }
                    else {
                        $admin_id = $admin_data->id;

                        //Check Validation          
                        $validation = Validator::make($request->all(), Chat::$apiSendByAdminRules);
                        if($validation->fails())
                            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                        $user_id = NULL;
                        $sent_to = $request->sent_to;
                    }
                    
                }
                else {
                    $user_id = $user_data->user_id;      
                }

                if(empty($request->sent_to)) {
                    $sent_to = NULL;
                }
                else {
                    $sent_to = $request->sent_to;
                }
            }

            $zone = isset($request->zone) ? $request->zone : "-04:00";     

            $messages = Chat::getPreviousMessages($user_id, $request->sent_to, $request->property_id, $request->last_msg_id, $zone); 

            return response()->json($messages, 200);

        }

        // Api Conversations
        public static function apiConversations(Request $request) {

            $keyword = "";
            $keyword_q = "";
            $keyword_q1 = "";
            $img_base_path = url(CommonFunction::PICTURE_PATH).'/';
            $img_base_path1 = url(CommonFunction::PICTURE_PATH_D).'/default.png';
            $name = "Admin";

            if(empty($request->header('Authorization'))) {
                return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403); 

            }
            else {
                $access_token = $request->header('Authorization');
                $user_data = CommonFunction::validateAccessToken($access_token);

                if($user_data == NULL) {
                    $admin_data = CommonFunction::validateAdminAccessToken($access_token);
                    if($admin_data == NULL) {
                        return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                    }
                    else {
                        $user_id = NULL;
                        $admin_id = $admin_data->id;

                        if(!empty($request->keyword)){
                            $keyword = $request->keyword;
                            $keyword_q = "AND ((IF(`sent_by` IS NULL , (SELECT `first_name` from `users` where `id` = `sent_to`) , (SELECT `first_name` from `users` where `id` = `sent_by`))) like '%$keyword%')";
                            $keyword_q1 = "AND (SELECT `first_name` from `users` where `id` = properties.user_id) like '%$keyword%'";
                        }

                        $message_listing = Chat::selectRaw("chats.*, (SELECT `address_1` from `properties` where `id` = `property_id`) AS  prop_address, (SELECT                                                   `address_2` from `properties` where `id` =                                  `property_id`) AS prop_address2, 
                                (SELECT `type` from `properties` where `id` = `property_id`) AS prop_type,
                                IF(`sent_by` IS NULL, (SELECT `first_name` from `users` where `id` = `sent_to`), (SELECT `first_name` from `users` where `id` = `sent_by`)) AS `first_name`,
                                IF(`sent_by` IS NULL, (SELECT `last_name` from `users` where `id` = `sent_to`), (SELECT `last_name` from `users` where `id` = `sent_by`)) AS `last_name`,
                                IF(`sent_by` IS NULL,(CASE 
                                WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_to`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_to`)) = 0 THEN (SELECT '$img_base_path1')
                                ELSE 
                                    CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_to`)) 
                                END) , (CASE 
                                WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) = 0 THEN (SELECT '$img_base_path1')
                                ELSE 
                                    CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_by`)) 
                                END))  AS `image`
                            ")
                            ->whereRaw("`chats`.`id` in (select max(chats.id) from chats where ((sent_by=? or sent_to=? or admin_id = $admin_id))
                                        GROUP BY property_id)  $keyword_q",[$user_id,$user_id])
                                        ->orderBy('chats.id',"desc")
                                        ->get();

                        $prop_ids = [];
                        foreach($message_listing as $key => $value) {
                            $prop_ids[] = $value->property_id;
                        }

                        $apartment = Property::where([
                            ['type', 4],
                            ['status', 5]
                        ])->whereNotIN('id', $prop_ids)->selectRaw("properties.id, properties.user_id, properties.address_1 as prop_address, properties.address_2 as prop_address2, properties.type as prop_type, (SELECT `first_name` from `users` where `id` = properties.user_id) AS `first_name`, (SELECT `last_name` from `users` where `id` = properties.user_id) AS `last_name`, (CASE 
                        WHEN ((SELECT `image` FROM `users` WHERE `id` = properties.user_id)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = properties.user_id)) = 0 THEN (SELECT '$img_base_path1')
                        ELSE 
                            CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = properties.user_id)) 
                        END)  AS `image`")->whereRaw("1 $keyword_q1")->get();
                        
                        $response = [
                            'message_listing' => $message_listing,
                            'apartment' => $apartment
                        ];

                        return $response;
                    }
                    
                }
                else {
                    $user_id = $user_data->user_id;  
                        
                }

            }


            if(!empty($request->keyword)){
                $keyword = $request->keyword;
                $keyword_q = "AND (IFNULL((IF(`sent_by` = $user_id , (SELECT `first_name` from `users` where `id` = `sent_to`) , (SELECT `first_name` from `users` where `id` = `sent_by`))), '$name') like '%$keyword%')";
            }
            $zone = isset($request->zone) ? $request->zone : "-04:00";  


            $message_listing = Chat::selectRaw("chats.*, (SELECT `address_1` from  `properties` where `id` = `property_id`) AS prop_address, (SELECT                     `address_2` from `properties` where `id` = `property_id`) AS                            prop_address2, 
                        (SELECT `type` from `properties` where `id` = `property_id`) AS prop_type,
                        IF(`sent_by` = '$user_id', (SELECT `first_name` from `users` where `id` = `sent_to`), (SELECT `first_name` from `users` where `id` = `sent_by`)) AS `first_name`,
                        IF(`sent_by` = '$user_id', (SELECT `last_name` from `users` where `id` = `sent_to`), (SELECT `last_name` from `users` where `id` = `sent_by`)) AS `last_name`,
                        IF(`sent_by` = '$user_id',(CASE 
                        WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_to`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_to`)) = 0 THEN (SELECT '$img_base_path1')
                        ELSE 
                            CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_to`)) 
                        END) , (CASE 
                        WHEN ((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) IS NULL OR length((SELECT `image` FROM `users` WHERE `id` = `sent_by`)) = 0 THEN (SELECT '$img_base_path1')
                        ELSE 
                            CONCAT('$img_base_path', (SELECT `image` FROM `users` WHERE `id` = `sent_by`)) 
                        END))  AS `image`
                    ")
                    ->whereRaw("`chats`.`id` in (select max(chats.id) from chats                       where ((sent_by=? or sent_to=? or (admin_id IS NOT NULL AND                             (sent_by=? or sent_to=?))))
                                GROUP BY property_id, greatest(sent_by, sent_to), least(sent_by, sent_to))  $keyword_q",[$user_id,$user_id,$user_id,$user_id])
                                ->orderBy('chats.id',"desc")
                                ->get();

            return $message_listing;

        }

        // Api Previous Messages
        public static function apiMessages(Request $request) {
            
            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiPreviousMessagesRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id; 
            $zone = isset($request->zone) ? $request->zone : "-04:00";   
            

            $messages = Chat::getMessages($user_id, $request->sent_to, $request->property_id, $request->last_msg_id, $zone);  
                      
            return response()->json($messages, 200);

        }

        // Api Delete Conversation
        public static function apiDeleteConversation(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiDeleteConversationsRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            $user_id = $request->user_data->user_id;
        
            Chat::where([
                ['sent_by', $user_id],
                ['sent_to', $request->sent_to],
                ['property_id', $request->property_id]
            ])->orWhere([
                ['sent_by', $request->sent_to],
                ['sent_to', $user_id],
                ['property_id', $request->property_id]
            ])->delete();
  
            //Send JSON response
            return response()->json(['data' => 'Chat Deleted'], 200);   
        }

        //Api payment check
        public static function apiPaymentCheck(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiChatPaymentRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;
            $amount = 2500;
            $user = User::where('id', $user_id)->first();
            $msg = "success";
            $property = Property::where('id', $request->property_id)->first(); //second chat

            $shortcut_payment = Payment::where([
                ['user_id', $request->user_id_1],
                ['agent_id', $user_id],
                ['property_id', $request->property_id],
                ['payment_type', 1]
            ])->orWhere([
                ['user_id', $request->user_id_1],
                ['agent_id', $user_id],
                ['property_id', $request->property_id],
                ['payment_type', 3]
            ])->first();


            if(empty($shortcut_payment)) {
                
                return 1;
            }            

            //inserting in payment
            $chat_payment = new Payment;
            $chat_payment->user_id = $request->user_id_1;
            $chat_payment->agent_id = $user_id;
            $chat_payment->property_id = $request->property_id;
            $chat_payment->payment_type = 4;
            $chat_payment->amount = 25;
            $chat_payment->payment_status = 2;
            $chat_payment->save();

            //second chat
            if($property->second_agent == $user_id) {
                Property::where('id', $request->property_id)->update([
                    'second_chat' => 1,
                    'chat_request' => 0,
                ]);
            }
            elseif($property->agent_id == $user_id) {
                Property::where('id', $request->property_id)->update([
                    'chat_payment' => 1,
                    'chat_request' => 0,
                ]);
            }

            // $msg = "You don't have to pay anything since you did all the calculations or gave a second opinion for this property.";

            return response()->json($msg, 200);
        }

        //Api chat payment
        public static function apiChatPayment(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiChatPaymentRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;
            $amount = 2500;
            $user = User::where('id', $user_id)->first();
            $msg = "success";
            $property = Property::where('id', $request->property_id)->first(); //second chat

            $shortcut_payment = Payment::where([
                ['user_id', $request->user_id_1],
                ['agent_id', $user_id],
                ['property_id', $request->property_id],
                ['payment_type', 1]
            ])->orWhere([
                ['user_id', $request->user_id_1],
                ['agent_id', $user_id],
                ['property_id', $request->property_id],
                ['payment_type', 3]
            ])->first();


            if(empty($shortcut_payment)) {

                // $validation = Validator::make($request->all(),[
                //     'token' => 'required'
                // ]);
        
                // if ($validation->fails())
                //     return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
                //payment using credits or stripe
                if($user->credits != NULL) {
                    $payment = (($user->credits * 100) - $amount);
                    if($payment >= 0) {
                        $credits = $payment/100;
                        User::where('id', $user_id)->update(['credits' => $credits]);

                        // $msg = "Thanks for the Payment.The complete amount $25 was deducted from your credits.";

                    }
                    else {
                        $amount = ($amount - ($user->credits*100));

                        $stripe = CommonFunction::makeInvoice($request, $amount);
                        if($stripe != 'payment_success') {
                            return $stripe;
                        }

                        User::where('id', $user_id)->update(['credits' => 0]);

                        // $msg = "Thanks for the Payment.You only had to pay ".$amount." the rest was deducted from your credits.";
                    }
    
                }
                else {
                    $stripe = CommonFunction::makeInvoice($request, $amount);
                    if($stripe != 'payment_success') {
                        return $stripe;
                    }

                    // $msg = "Thanks for the Payment.Your transaction for $25 was successful";
                }
                $msg = "Payment Successful.The amount will be debited at the beginning of next month.";  
                    
                //inserting in payment
                $chat_payment = new Payment;
                $chat_payment->user_id = $request->user_id_1;
                $chat_payment->agent_id = $user_id;
                $chat_payment->property_id = $request->property_id;
                $chat_payment->payment_type = 4;
                $chat_payment->amount = 25;
                $chat_payment->save();
            }
            else {
                $msg = "You don't have to pay anything since you did all the calculations or gave a second opinion for this property.";
                    
                //inserting in payment
                $chat_payment = new Payment;
                $chat_payment->user_id = $request->user_id_1;
                $chat_payment->agent_id = $user_id;
                $chat_payment->property_id = $request->property_id;
                $chat_payment->payment_type = 4;
                $chat_payment->amount = 25;
                $chat_payment->payment_status = 2;
                $chat_payment->save();
            }


            //second chat
            if($property->second_agent == $user_id) {
                Property::where('id', $request->property_id)->update([
                    'second_chat' => 1,
                    'chat_request' => 0,
                ]);
            }
            elseif($property->agent_id == $user_id) {
                Property::where('id', $request->property_id)->update([
                    'chat_payment' => 1,
                    'chat_request' => 0,
                ]);
            }


            return response()->json($msg, 200);
        }

        //Api send chat request
        public static function apiChatRequest(Request $request) {

            //Check Validation          
            $validation = Validator::make($request->all(), Chat::$apiChatRequestRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;
            $property = Property::where('id', $request->property_id)->value('chat_request');

            Property::where('id', $request->property_id)->update([
                'chat_request' => $property + 1,
                'chat_with' => $request->agent_id
            ]);

            return response()->json('Agent requested to connect.', 200);

        }

    /*-----------------------------------------------------
    -------------------------------------------------------
        * version 3
    -------------------------------------------------------
    ------------------------------------------------------- */

        // unread chat notification
        public static function unreadChatNotif() {
            $unread = Chat::where('is_read', 0)->where('read_notif', 0)->where('sent_to', '!=', NULL)->groupby('sent_to')->get();

            foreach($unread as $key => $value) {
                $user = User::where('id',$value->sent_to)->first();

                // Send Email
                $mail = Mail::send("emails.unread-msg-notif", ['user' => $user], function ($m) use ($user) {
                    $m->to($user->email)->subject("Unread Chat");
                });
            }

            Chat::where('is_read', 0)->where('read_notif', 0)->where('sent_to', '!=', NULL)->update(['read_notif' => 1]);

            return 'success';
        }

}
