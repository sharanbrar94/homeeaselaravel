<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Property;
use Carbon\Carbon;
use App\Models\Payment;
use App\Models\City;
use App\Models\Chat;
use App\Models\Admin;
use App\Models\CompProperty;
use App\Models\Multiplier;
use App\Library\CommonFunction;


class PropertyController extends Controller
{
    /*-----------------------------------------------------
    -------------------------------------------------------
        * Property Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

    //Api edit property
    public static function apiEditProperty(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), Property::$apiEditPropertyRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_id = $request->user_data->user_id;  

        $property_id = $request->property_id;

        $edit = Property::where('id', $property_id)->first();
        $msg = "";

        $status = $request->status;
        if(isset($status)) {
            if($status == 1) {
                $send_to = $edit->user()->value('email');
                $name = $edit->user()->value('first_name');
                $first_name = $edit->user()->value('first_name');
                $last_name = $edit->user()->value('last_name');
                $full_name = $first_name.' '.$last_name;
                $msg = "You've been Comp'd. You can begin the Calculations.";
                $subject = "Comps Received";
                $url = "https://homeease.pro/dashboard/user";

                if($edit->country_code && $edit->phone_number) {
                    $getContent = CommonFunction::smsContent();
                    $content = $getContent['comp-recieved'];
                    CommonFunction::sendSms($edit->country_code,$edit->phone_number,$content);
                }
            }
            elseif($status == 6) {
                $send_to = $edit->user()->value('email');
                $name = $edit->user()->value('first_name');
                $first_name = $edit->user()->value('first_name');
                $last_name = $edit->user()->value('last_name');
                $full_name = $first_name.' '.$last_name;
                $msg = "Your Calculations have been reviewed by an Agent.";
                $subject = "Agent Reviewed";
                $url = "https://homeease.pro/dashboard/user";
            }
            elseif($status == 4) {
                $send_to = $edit->agent()->value('email');
                $name = $edit->agent()->value('first_name');
                $first_name = $edit->agent()->value('first_name');
                $last_name = $edit->agent()->value('last_name');
                $full_name = $first_name.' '.$last_name;
                $msg = "Your Client has requested you to review their Calculation.";
                $subject = "Review Requested";
                $url = "https://homeease.pro/dashboard/agent";
            }
            if($edit->type == 4) {
                if($status == 3 || $status == 5) {
                    $exist = Chat::where('admin_id', 1)->where('sent_by', null)->where('sent_to', $edit->user_id)->where('property_id', $request->property_id)->first();
                    if(empty($exist)) {
                        $new_chat = new Chat;
                        $new_chat->admin_id = 1;
                        $new_chat->sent_by = null;
                        $new_chat->sent_to = $edit->user_id;
                        $new_chat->property_id = $request->property_id;
                        $new_chat->sent_by_admin = 1;
                        $new_chat->msg_type = 0;
                        $new_chat->msg = "Thank you for connecting with us! Our focus 
                        is to help you get the right price for your property with the smoothest possible sale process. We have the combine experience of 60 years of completing apartment transactions and hands on property management. There's not much that surprises us anymore. How can we serve you?"; 
                        $new_chat->save();

                        //$admin = Admin::where('id',$messages->admin_id)->first();
                        $admin = User::where('id',1)->first();
                        $admin_name = "Investor Admin";


                        // Send Email
                        $mail = Mail::send("emails.edit-property", ['name' => $admin_name, 'recent' => $new_chat], function ($m) use ($admin) {
                            $m->to($admin->email)->subject("Message Received");
                        });
                    }

                }
            }
        }
        if(isset($request->images)){
            if($request->images == '[]') {
                $images_json = NULL;
            }
            else {
                //image json
                $images_arr = json_decode($request->images);
                foreach($images_arr as $key => $value) {
                    $img_arr[] = CommonFunction::extractImageName($value);
                }
                $images_json = json_encode($img_arr);
            }
            
        }
        else {
            if($edit->images == []) {
                $images_json = NULL;
            }
            else {
                //image json
                $images_arr = ($edit->images);
                foreach($images_arr as $key => $value) {
                    $img_arr[] = CommonFunction::extractImageName($value);
                }
                $images_json = json_encode($img_arr);
            }
        }
        if($edit->type == 3) {

            $condo_fees = isset($request->condo_fees) ? $request->condo_fees : $edit->condo_fees;
            $floor = isset($request->floor) ? $request->floor : $edit->floor;

        }
        else {
            $condo_fees = NULL;
            $floor = NULL;
        }
        if($status == 3){
            if($edit->country_code && $edit->phone_number) {
                $getContent = CommonFunction::smsContent();
                $content = $getContent['paid-e-appraisal'];
                CommonFunction::sendSms($edit->country_code,$edit->phone_number,$content);
            }
        }

        
            Property::where('id', $property_id)->update([
                'agent_id' =>isset($request->agent_id) ? $request->agent_id : $edit->agent_id,
                'address_1' =>isset($request->address_1) ? $request->address_1 : $edit->address_1,
                'address_2' =>isset($request->address_2) ? $request->address_2 : $edit->address_2,
                'city' =>isset($request->city) ? $request->city : $edit->city,
                'county' =>isset($request->county) ? $request->county : $edit->county,
                'state' =>isset($request->state) ? $request->state : $edit->state,
                'zip_code' =>isset($request->zip_code) ? $request->zip_code : $edit->zip_code,
                'country' =>isset($request->country) ? $request->country : $edit->country,
                'lat' =>isset($request->lat) ? $request->lat : $edit->lat,
                'lng' =>isset($request->lng) ? $request->lng : $edit->lng,
                'images' => $images_json,
                'status' =>isset($request->status) ? $request->status : $edit->status, 
                'description' =>isset($request->description) ? $request->description : $edit->description,           
                'sale_price' =>isset($request->sale_price) ? $request->sale_price : $edit->sale_price,
                'sale_type' =>isset($request->sale_type) ? $request->sale_type : $edit->sale_type,
                'sqft_above' =>isset($request->sqft_above) ? $request->sqft_above : $edit->sqft_above,
                'sqft_below' =>isset($request->sqft_below) ? $request->sqft_below : $edit->sqft_below,
                'total_sqft' =>isset($request->total_sqft) ? $request->total_sqft : $edit->total_sqft,
                'date_sold' =>isset($request->date_sold) ? $request->date_sold : $edit->date_sold,
                'year_build' =>isset($request->year_build) ? $request->year_build : $edit->year_build,
                'bedroom_above' =>isset($request->bedroom_above) ? $request->bedroom_above : $edit->bedroom_above,
                'bathroom_above' =>isset($request->bathroom_above) ? $request->bathroom_above : $edit->bathroom_above,
                'total_rooms' =>isset($request->total_rooms) ? $request->total_rooms : $edit->total_rooms,
                'bedroom_below' =>isset($request->bedroom_below) ? $request->bedroom_below : $edit->bedroom_below,
                'bathroom_below' =>isset($request->bathroom_below) ? $request->bathroom_below : $edit->bathroom_below,
                'other' =>isset($request->other) ? $request->other : $edit->other,
                'heat_type' =>isset($request->heat_type) ? $request->heat_type : $edit->heat_type,
                'ac' =>isset($request->ac) ? $request->ac : $edit->ac,
                'garage_space' =>isset($request->garage_space) ? $request->garage_space : $edit->garage_space,
                'porch' =>isset($request->porch) ? $request->porch : $edit->porch,
                'fire_place' =>isset($request->fire_place) ? $request->fire_place : $edit->fire_place,
                'mls_id' =>isset($request->mls_id) ? $request->mls_id : $edit->mls_id,
                'acerage' =>isset($request->acerage) ? $request->acerage : $edit->acerage,
                'floor' => $floor,
                'condo_fees' => $condo_fees,
                'unit1' => isset($request->unit1) ? $request->unit1 : $edit->unit1,
                'unit2' => isset($request->unit2) ? $request->unit2 : $edit->unit2,
                'final_price' => isset($request->final_price) ? $request->final_price : $edit->final_price,
                'listing_number' => isset($request->listing_number) ? $request->listing_number : $edit->listing_number,
            ]);
       

        // Get Property Details
        $property_details = Property::getPropertyDetails($property_id);

        if($msg != "") {
            // Send Email
            $mail = Mail::send("emails.update", ['name' => $full_name, 'msg' => $msg, 'url' => $url], function ($m) use ($send_to, $subject) {
                $m->to($send_to)->subject($subject);
            });
        }

        
        //Send JSON response
        return $property_details;
    }
     //Api edit property
     public static function apiEditPropertyFreeEAppraisal(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), Property::$apiEditPropertyRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_id = $request->user_data->user_id;  

        $property_id = $request->property_id;

        $edit = Property::where('id', $property_id)->first();
     
        $status = $request->status;
       
        
        if($edit->type == 3) {
            $condo_fees = isset($request->condo_fees) ? $request->condo_fees : $edit->condo_fees;
            $floor = isset($request->floor) ? $request->floor : $edit->floor;
            

        }
        else {
            $condo_fees = NULL;
            $floor = NULL;
        }
        

        //edit property
        Property::where('id', $property_id)->update([
            'agent_id' =>isset($request->agent_id) ? $request->agent_id : $edit->agent_id,
            'address_1' =>isset($request->address_1) ? $request->address_1 : $edit->address_1,
            'address_2' =>isset($request->address_2) ? $request->address_2 : $edit->address_2,
            'city' =>isset($request->city) ? $request->city : $edit->city,
            'county' =>isset($request->county) ? $request->county : $edit->county,
            'state' =>isset($request->state) ? $request->state : $edit->state,
            'zip_code' =>isset($request->zip_code) ? $request->zip_code : $edit->zip_code,
            'country' =>isset($request->country) ? $request->country : $edit->country,
            'lat' =>isset($request->lat) ? $request->lat : $edit->lat,
            'lng' =>isset($request->lng) ? $request->lng : $edit->lng,
            // 'images' => '',
            'status' =>isset($request->status) ? $request->status : $edit->status, 
            'description' =>isset($request->description) ? $request->description : $edit->description,           
            'sale_price' =>isset($request->sale_price) ? $request->sale_price : $edit->sale_price,
            'sale_type' =>isset($request->sale_type) ? $request->sale_type : $edit->sale_type,
            'sqft_above' =>isset($request->sqft_above) ? $request->sqft_above : $edit->sqft_above,
            'sqft_below' =>isset($request->sqft_below) ? $request->sqft_below : $edit->sqft_below,
            'total_sqft' =>isset($request->total_sqft) ? $request->total_sqft : $edit->total_sqft,
            'date_sold' =>isset($request->date_sold) ? $request->date_sold : $edit->date_sold,
            'year_build' =>isset($request->year_build) ? $request->year_build : $edit->year_build,
            'bedroom_above' =>isset($request->bedroom_above) ? $request->bedroom_above : $edit->bedroom_above,
            'bathroom_above' =>isset($request->bathroom_above) ? $request->bathroom_above : $edit->bathroom_above,
            'total_rooms' =>isset($request->total_rooms) ? $request->total_rooms : $edit->total_rooms,
            'bedroom_below' =>isset($request->bedroom_below) ? $request->bedroom_below : $edit->bedroom_below,
            'bathroom_below' =>isset($request->bathroom_below) ? $request->bathroom_below : $edit->bathroom_below,
            'other' =>isset($request->other) ? $request->other : $edit->other,
            'heat_type' =>isset($request->heat_type) ? $request->heat_type : $edit->heat_type,
            'ac' =>isset($request->ac) ? $request->ac : $edit->ac,
            'garage_space' =>isset($request->garage_space) ? $request->garage_space : $edit->garage_space,
            'porch' =>isset($request->porch) ? $request->porch : $edit->porch,
            'fire_place' =>isset($request->fire_place) ? $request->fire_place : $edit->fire_place,
            'mls_id' =>isset($request->mls_id) ? $request->mls_id : $edit->mls_id,
            'acerage' =>isset($request->acerage) ? $request->acerage : $edit->acerage,
            'floor' => $floor,
            'condo_fees' => $condo_fees,
            'unit1' => isset($request->unit1) ? $request->unit1 : $edit->unit1,
            'unit2' => isset($request->unit2) ? $request->unit2 : $edit->unit2,
            'final_price' => isset($request->final_price) ? $request->final_price : $edit->final_price,
            'listing_number' => isset($request->listing_number) ? $request->listing_number : $edit->listing_number,
        ]);
       
        // Get Property Details
        $property_details = Property::getPropertyDetails($property_id);

        //Send JSON response
        return $property_details;
    }

    // Api Post Details
    public static function apiPropertyDetails(Request $request) {
        //Check Validation	        
        $validation = Validator::make($request->all(), Property::$apiPropertyRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $access_token = $request->header('Authorization');
        $user_data = CommonFunction::validateAccessToken($access_token);
        $user_id = isset($user_data->user_id) ? $user_data->user_id : NULL;

        $property_details = Property::getPropertyDetails($request->property_id);
        
        $payment = Payment::where('property_id',$property_details->id)->where('user_id',$property_details->user_id)->first();
        if($payment != '')
        {
            $property_details->pricing_payment = 1;
        }
        else
        {
            $property_details->pricing_payment = 0;
        }
        

        return response()->json($property_details, 200);
    }

    //Api Take Shortcut
    public static function apiTakeShortcut(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), Property::$apiTakeShortcutRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_id = $request->user_data->user_id;
        $property = Property::where('id', $request->property_id)->first();
        $msg = "";

        if($property->agent_id != NULL) {
            $agent_id = $property->agent_id;

            $send_to = $property->agent()->value('email');
            $name = $property->agent()->value('first_name');
            $msg = "Your Client has taken Shortcut for a Property.";
            $subject = "Shortcut";
            $url = "https://homeease.pro/dashboard/agent";
        }
        else {
            $agent_id = NULL;
        }

        if($property->user_id == $user_id) {
            if($property->agency_type!=1) {
                $amount = 7500;
                $stripe = CommonFunction::makeStripePayment($request, $amount);
                if($stripe != 'payment_success') {
                    return $stripe;
                }
            }
            

            Property::where('id', $request->property_id)->update(['shortcut' => 1]);

            //inserting in payments
            $payment = new Payment;
            $payment->user_id = $user_id;
            $payment->agent_id = $agent_id;
            $payment->property_id = $request->property_id;
            $payment->payment_type = 1;
            $payment->amount = 75;
            $payment->save();


            //payment update to user
            // Send Email
            $payment_by = $property->user()->value('first_name');
            $payment_by_email = $property->user()->value('email');
            $text = "Shortcut";
            $mail = Mail::send("emails.take-shortcut", ['name' => $payment_by, 'text' => $text, 'amount' => 75.00], function ($m) use ($payment_by_email) {
                $m->to($payment_by_email)->subject("Successful Payment");
            });

            if($msg != "") {
                // Send Email
                $mail = Mail::send("emails.update", ['name' => $name, 'msg' => $msg, 'url' => $url], function ($m) use ($send_to, $subject) {
                    $m->to($send_to)->subject($subject);
                });
            }
        }
        else {
            return response()->json(['error' => 'bad_request', 'error_description' => 'You can not take the shortcut for this property'], 400);
        }
            

        return response()->json('success', 200);
        
    }

    //Api remove property
    public static function apiRemoveProperty(Request $request) {

        
        $user_id = $request->user_data->user_id;

        $removed_prop = $request->removed_prop;
        $property = Property::where('id', $request->property_id)->first();
        if($property->agency_type == 1) {
            Property::where('id', $request->property_id)->update(['status' => 9]);
        }

        User::where('id', $user_id)->update(['removed_properties' => $removed_prop]);

        return response()->json(['Removed'], 200);
    }

    //Api delete property
    public static function apiDeleteProperty(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), Property::$apiPropertyRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_id = $request->user_data->user_id;
        $current_time = Carbon::now();

        $prop = Property::where('id', $request->property_id)->first();

        if($user_id != $prop->user_id) {
            return response()->json(['error' => 'bad_request', 'error_description' => 'You can not delete this property.'], 400);
        }
        else{
            Property::where('id', $request->property_id)->update(['deleted_at' => $current_time]);

            return response()->json(['Property deleted'], 200);
        }
    }


    /*-----------------------------------------------------
    -------------------------------------------------------
        * Comparison Property Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

    //Api add comparison property
    public static function apiAddCompProp(Request $request) {

        $user_id = $request->user_data->user_id;

        $prop_data = Property::where('id', $request->property_id)->first();
        $comp_properties = CompProperty::where('type', 1)->where('subject_prop_id', $request->property_id)->first();
        
        if(!empty($comp_properties)) {  

            $c1 = Property::where('id', $comp_properties->comp_prop1)->value('agent_id');

            if($user_id != $c1) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'This property has been compd by another agent'], 400);
            }        
        }
        else {
            if($prop_data->outside_area == 1){
                $city_multiplier = City::where('id',1)->first();
            }
            else{
                $city_multiplier = City::where('id', $prop_data->city)->first();
            }
            

            //inserting multipliers
            $multiplier = new Multiplier;
            $multiplier->type = 1;
            $multiplier->property_id = $request->property_id;
            $multiplier->above_sqft = $city_multiplier->above_sqft;
            $multiplier->below_sqft = $city_multiplier->below_sqft;
            $multiplier->year_build = $city_multiplier->year_build;
            $multiplier->garage1 = $city_multiplier->garage1;
            $multiplier->garage2 = $city_multiplier->garage2;
            $multiplier->heat = $city_multiplier->heat;
            $multiplier->ac = $city_multiplier->ac;
            $multiplier->fire_place = $city_multiplier->fire_place;
            $multiplier->porch = $city_multiplier->porch;
            $multiplier->bedroom_above = $city_multiplier->bedroom_above;
            $multiplier->bedroom_below = $city_multiplier->bedroom_below;
            $multiplier->bathroom_above = $city_multiplier->bathroom_above;
            $multiplier->bathroom_below = $city_multiplier->bathroom_below;
            $multiplier->quality = $city_multiplier->quality;
            $multiplier->condition_value = $city_multiplier->condition_value;
            $multiplier->save();

        }

        Property::where('id', $request->property_id)->update(['agent_id' => $user_id]);
        
        $subject = PropertyController::apiEditProperty($request);
        $after_edit = Property::where('id', $request->property_id)->first();
        if($subject != $after_edit) {
            return $subject;
        }


        $comp1 = $request->comp1;
        $comp_data1 = json_decode($comp1, true);
        $validation = Validator::make($comp_data1, Property::$someRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => 'For comp1 property '.$validation->getMessageBag()->first()], 400);


        $comp2 = $request->comp2;
        $comp_data2 = json_decode($comp2, true);
        $validation = Validator::make($comp_data2, Property::$someRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => 'For comp2 property '.$validation->getMessageBag()->first()], 400);


        $comp3 = $request->comp3;
        $comp_data3 = json_decode($comp3, true);
        $validation = Validator::make($comp_data3, Property::$someRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => 'For comp3 property '.$validation->getMessageBag()->first()], 400);
    
        //first comp prop

            $comp_prop1 = Property::addComp($comp1, $request->property_id, $user_id, $request->comp_id1);

        //second comp prop
            
            $comp_prop2 = Property::addComp($comp2, $request->property_id, $user_id, $request->comp_id2);

        //third comp prop

            $comp_prop3 = Property::addComp($comp3, $request->property_id, $user_id, $request->comp_id3);

        
        //Inserting in Comp Properties
        if(empty($request->comp_id1)) {
            $new_prop = new CompProperty;
            $new_prop->type = 1;
            $new_prop->subject_prop_id = $request->property_id;
            $new_prop->comp_prop1 = $comp_prop1->id;
            $new_prop->comp_prop2 = $comp_prop2->id;
            $new_prop->comp_prop3 = $comp_prop3->id;
            $new_prop->is_finished = 1;
            $new_prop->save();
        }
            

        $data[] = [
            "subject_prop" => $after_edit,
            "comp1" => $comp_prop1,
            "comp2" => $comp_prop2,
            "comp3" => $comp_prop3
        ];


        //Send JSON response
        return response()->json($data,200);

    }

    //Api Comp Data
    public static function apiCompData(Request $request) {
        //Check Validation	        
        $validation = Validator::make($request->all(), User::$apiUserCompdRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_id = $request->user_data->user_id;

        $subject_prop_id = $request->subject_prop_id;

        $subject_property = Property::where('id', $subject_prop_id)->first();

        $comp_properties = CompProperty::where('type', 1)->where('subject_prop_id', $subject_prop_id)->first();
        
        if(!empty($comp_properties)) {   
            if($user_id != $subject_property->agent_id) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'This property has been compd by another agent'], 400);
            }

            $compd = 1;
            $subject_property->c1 = Property::where('id', $comp_properties->comp_prop1)->first();
            $subject_property->c2 = Property::where('id', $comp_properties->comp_prop2)->first();
            $subject_property->c3 = Property::where('id', $comp_properties->comp_prop3)->first();
        }
        else {
            $compd = 0;
        }

        $subject_property->compd = $compd;

        return response()->json($subject_property,200);      
    }


}

