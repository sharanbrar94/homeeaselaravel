<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Apartment;
use App\Models\Property;
use App\Models\Payment;
use App\Models\City;
use App\Models\County;
use App\Library\CommonFunction;

class ApartmentController extends Controller
{
     /*-----------------------------------------------------
    -------------------------------------------------------
        * Apartment Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api apartment calculation
        public static function apiApartmentCalculation(Request $request) {

            $user_id = $request->user_data->user_id;
            $apartment_id = $request->apartment_id;
            $property = Property::where('id', $apartment_id)->first();
            
            if($property->type != 4) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'The property can only be an apartment'], 400); 
            }


            $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $property->user_id)->whereIn('payment_type', [5,8])->first(); //5-yearly, 8-monthly
            if(empty($payment)) {
                $apartment_payment = 0;
            }
            else {
                $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
                if($payment->payment_type == 8) {
                    if($diff > 30) {
                        $apartment_payment = 0;
                    }
                    else {
                        $apartment_payment = 1;
                    }
                }
                else {
                    if($diff > 365) {
                        $apartment_payment = 0;
                    }
                    else {
                        $apartment_payment = 1;
                    }
                }

                // dd($diff);
            }

            if($apartment_payment == 1) {
                //Check Validation	        
                $validation = Validator::make($request->all(), Apartment::$apiApartmentCalculationRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            }
            elseif($apartment_payment == 0) {
                //Check Validation	        
                $validation = Validator::make($request->all(), Apartment::$apiApartmentRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            }

            $apartment = Apartment::where('id', $apartment_id)->first();
            $repair_cost = 0;
            $total_rental_income = 0;
            $rental_income = 0;

            if($user_id == $property->user_id || $user_id == 1)
            {
                
                $revenue_details = $request->revenue_details;
                $revenue_arr = json_decode($request->revenue_details);
                
                foreach($revenue_arr as $key => $value) {
                    if($value->units == "" || $value->rent == "") {
                        $rate = 0;
                    }
                    else {
                        $rate = $value->units * $value->rent;
                    }
                    $total_rental_income = $rate + $total_rental_income;
                    $value->rate = $rate;
                }

                $revenue = json_encode($revenue_arr);
                $laundry = $request->revenue_laundry;
                $parking = $request->revenue_parking;
                $monthly_income = $total_rental_income + $laundry + $parking;
                $gross_income = round($monthly_income*12, 2);
                $vacancy_factor = round(($gross_income*5)/100, 2);
                $projected_year_income = $gross_income - $vacancy_factor;

                $expense_details = $request->expense_details;
                $expense_arr = json_decode($request->expense_details);
                
                foreach($expense_arr as $key => $value) {
                    if($value->price == "") {
                        $rental_income = 0 + $rental_income;
                    }
                    else {
                        $rental_income = $value->price + $rental_income;
                    }
                }

                if($apartment_payment == 1) {
                    $yearly_revenue = $projected_year_income;
                    $yearly_expense = $rental_income;
                    $percent = round(($yearly_expense * 100)/$yearly_revenue, 2);
                }
                elseif($apartment_payment == 0) {
                    $yearly_revenue = $request->yearly_revenue;
                    $percent = $request->percent;
                    $yearly_expense = round(($yearly_revenue*$percent)/100, 2);
                }


                $operating_income = round($yearly_revenue - $yearly_expense, 2);
                $cap_rate = $request->cap_rate;
                $income_approach_value = round(($operating_income)/$cap_rate, 2);
                $building_repairs = $request->building_repairs;
                $building_arr = json_decode($request->building_repairs);
                
                foreach($building_arr as $key => $value) {
                    if($value->cost == "") {
                        $repair_cost = 0 + $repair_cost;
                    }
                    else {
                        $repair_cost = $value->cost + $repair_cost;
                    }
                }

                $value_less_repairs = $income_approach_value - $repair_cost;

                // financing and ROI
                $purchase_price = $request->purchase_price ? $request->purchase_price : $value_less_repairs;
                $down_payment = $request->down_payment;
                $interest = $request->interest;
                $ammortized = $request->ammortized;
                $all_cash_offer = $request->all_cash_offer;

                // financing and ROI calculations
                // $loan


                Apartment::where('id', $apartment_id)->update([
                    'yearly_revenue' => $yearly_revenue,
                    'percent' => $percent,
                    'yearly_expense' => $yearly_expense,
                    'operating_income' => $operating_income,
                    'cap_rate' => $cap_rate,
                    'income_approach_value' => $income_approach_value,
                    'building_repairs' => $building_repairs,
                    'repair_cost' => $repair_cost,
                    'value_less_repairs' => $value_less_repairs,
                    'revenue_details' => $revenue,
                    'total_rental_income' => $total_rental_income,
                    'laundry' => $laundry,
                    'parking' => $parking,
                    'monthly_income' => $monthly_income,
                    'gross_income' => $gross_income,
                    'vacancy_factor' => $vacancy_factor,
                    'projected_year_income' => $projected_year_income,
                    'expense_details' => $expense_details,
                    'rental_income' => $rental_income,
                    'purchase_price' => $purchase_price,
                    'down_payment' => $down_payment,
                    'interest' => $interest,
                    'ammortized' => $ammortized,
                    'all_cash_offer' => $all_cash_offer
                ]);

                Property::where('id', $apartment_id)->update([
                    'status' => 2, 
                ]);

                $apartmeny_details = Apartment::where('id', $apartment_id)->first();

                //Send JSON response
                return response()->json($apartmeny_details,200);
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make these calculations'], 400);
            }
        }

        //Api Apartment Data
        public static function apiApartmentData(Request $request) {
            $user_id = $request->user_data->user_id;
            $apartment_id = $request->apartment_id;
            $property = Property::where('id', $apartment_id)->first();
            //Check Validation	        
            $validation = Validator::make($request->all(), Apartment::$apiApartmentDataRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            if($user_id == $property->user_id || $user_id == 1){
                $apartment = Apartment::where('id', $request->apartment_id)->first();
                $property = Property::where('id',$request->apartment_id)->first();

                $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $property->user_id)->whereIn('payment_type', [5,8])->first(); //5-yearly, 8-monthly
                $loan_amount = 0;
                $monthly_payment = 0;
                $invested = 0;
                $yearly_reduction = 0;
                $yearly_cash_flow = 0;
                $yearly_profit = 0;
                $return = 0;
                
                if(empty($payment)) {
                    $apartment_payment = 0;
                }
                else {
                    $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
                    if($payment->payment_type == 8) {
                        if($diff > 30) {
                            $apartment_payment = 0;
                        }
                        else {
                            $apartment_payment = 1;
                        }
                    }
                    else {
                        if($diff > 365) {
                            $apartment_payment = 0;
                        }
                        else {
                            $apartment_payment = 1;
                        }
                    }

                    // dd($diff);
                }
                if($apartment->purchase_price!= null && $apartment->down_payment != null && $apartment->interest != null && $apartment->ammortized != null) {
                    // financing and ROI calculations
                    $loan_amount = round(($apartment->purchase_price)*(1-($apartment->down_payment/100)),2);
                    $monthly_payment = round(((($apartment->interest/1200)*$loan_amount)/(1 - pow((1 + ($apartment->interest/1200)),-($apartment->ammortized*12)))),2);
                    if($apartment->all_cash_offer == 0) {
                        $invested1 = round(($apartment->purchase_price * $apartment->down_payment),2);
                        $invested = $invested1/100;
                    }
                    else {
                        $invested = $apartment->purchase_price;
                    }
                    $yearly_reduction = round((($monthly_payment*12) - ($loan_amount*($apartment->interest/100))),2);
                    $yearly_cash_flow = round($apartment->operating_income - ($monthly_payment*12),2);
                    $yearly_profit = round(($yearly_reduction + $yearly_cash_flow),2);
                    $return = round(($yearly_profit/$invested)*100,2);
                }

                // =(Monthly Pymt x 12) - (Loan Amount x Interest Rate)

                $apartment->selling_status = $property->selling_status;
                $apartment->apartment_payment = $apartment_payment;
                $apartment->address_1 = $property->address_1;
                $apartment->address_2 = $property->address_2;
                $apartment->city = City::where('id', $property->city)->value('name');
                $apartment->county = County::where('id', $property->county)->value('name');
                $apartment->zip_code = $property->zip_code;

                // financing and ROI
                $apartment->loan_amount = $loan_amount;
                $apartment->monthly_payment = $monthly_payment;
                $apartment->invested = $invested;
                $apartment->yearly_reduction = $yearly_reduction;
                $apartment->yearly_cash_flow = $yearly_cash_flow;
                $apartment->yearly_profit = $yearly_profit;
                $apartment->return = $return;



                //send json response 
                return response()->json($apartment,200);
            }
            else {
                return response()->json(['error' => 'forbidden', 'error_description' => 'Invalid Token'], 403);
                //return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make these calculations'], 400);
            }
        } 

        
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Version 2
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        //Api Take Shortcut
        public static function apiApartmentPayment(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), Property::$apiTakeShortcutRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;
            $property = Property::where('id', $request->property_id)->first();

            if($property->agent_id != NULL) {
                $agent_id = $property->agent_id;
            }
            else {
                $agent_id = NULL;
            }

            if($property->user_id != $user_id) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make payment for this apartment.'], 400);
            }

            if($request->type == 1) {   // monthly 
                $amount = 995;
                $payment_type = 8;
            }
            else {
                $amount = 4500;
                $payment_type = 5;
            }

            
            $stripe = CommonFunction::makeStripePayment($request, $amount);
            if($stripe != 'payment_success') 
                return $stripe;
                
            if($property->user_id == $user_id) {
                //inserting in payments
                $payment = new Payment;
                $payment->user_id = $user_id;
                $payment->agent_id = $agent_id;
                $payment->property_id = $request->property_id;
                $payment->payment_type = $payment_type;
                $payment->amount = $amount/100.00;
                $payment->save();

            }

            //payment update to user
            // Send Email
            $payment_by = $property->user()->value('first_name');
            $payment_by_email = $property->user()->value('email');
            $text = "Unlocking Apartment Calculations";
            $mail = Mail::send("emails.payment-update", ['name' => $payment_by, 'text' => $text, 'amount' => $amount/100.00], function ($m) use ($payment_by_email) {
                $m->to($payment_by_email)->subject("Successful Payment");
            });
            // else {
            //     return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make payment for this apartment.'], 400);
            // }
                

            return response()->json('success', 200);
            
        }
}
