<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Admin;
use App\Models\Property;
use App\Models\Adjustment;
use App\Models\CashFlow;
use App\Models\Payment;
use App\Models\Multiplier;
use App\Models\HomeyOpinion;
use App\Models\CompProperty;
use App\Models\City;
use App\Models\County;
use App\Library\CommonFunction;
use App\Models\Chat;

class AdjustmentController extends Controller
{
    /*-----------------------------------------------------
    -------------------------------------------------------
        * Calculation Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api Step1 Calculation
        public static function apiStep1Calculation(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep1Rules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;

            if(isset($request->agent)) {
                $user_id = $request->agent;
            }

            $subject_prop_id = $request->subject_prop_id;

            $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'address_1', 'address_2', 'city', 'county', 'zip_code', 'images', 'progress_status', 'shortcut', 'other', 'status', 'sqft_above', 'sqft_below', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'step1', 'step2', 'step3', 'step4','cal_status','agency_type')->where('id', $subject_prop_id)->first();

            $user->s1->city_name = City::where('id', $user->s1->city)->value('name');
            $user->s1->county_name = County::where('id', $user->s1->county)->value('name');

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();

            if($user_id == $user->s1->user_id) {
                $comp_properties = CompProperty::where('type', 2)->where('subject_prop_id', $subject_prop_id)->where('is_finished',1)->first();
                if($comp_properties != ''){
                    $type = 2;
                }
                else{
                    $type = 1;
                }
            }
            elseif($user_id == $user->s1->agent_id) {
                if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                    $type = 1;
                }
                elseif($payment != '' && $payment->amount == 0){
                    $type = 1;
                }
                else {
                    $type = 2;
                }
            }
            elseif($user->s1->second_agent != NULL) {
                if($user_id == $user->s1->second_agent) {
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
            }
            elseif($user->s1->second_agent == NULL) {
                Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                $type = 3;
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
            }
            
            $step1 = isset($request->step1) ? $request->step1 : $user->s1->step1;
            $progress_status = isset($request->progress_status) ? $request->progress_status : $user->s1->progress_status;

            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
            if(empty($comp_properties)) {
                Property::addProperties($subject_prop_id, $type, $user_id);
                $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
            }

            $user->c1 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            Multiplier::getMultipliers($request, $subject_prop_id, $type);
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();
            $user->multipliers = $multiplier;

            //Adjustment sqft above
            $user->c1->sqft_above1 = ($user->s1->sqft_above - $user->c1->sqft_above)*$multiplier->above_sqft;
            $user->c2->sqft_above2 = ($user->s1->sqft_above - $user->c2->sqft_above)*$multiplier->above_sqft;
            $user->c3->sqft_above3 = ($user->s1->sqft_above - $user->c3->sqft_above)*$multiplier->above_sqft;

            //Adjustment sqft below
            $user->c1->sqft_below1 = ($user->s1->sqft_below - $user->c1->sqft_below)*$multiplier->below_sqft;
            $user->c2->sqft_below2 = ($user->s1->sqft_below - $user->c2->sqft_below)*$multiplier->below_sqft;
            $user->c3->sqft_below3 = ($user->s1->sqft_below - $user->c3->sqft_below)*$multiplier->below_sqft;
            
            //Adjustment year build
            if(abs($user->s1->year_build - $user->c1->year_build) >= 10){
                $user->c1->year_build1 = (($user->s1->year_build - $user->c1->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c1->year_build1 = 0;
            }
            if(abs($user->s1->year_build - $user->c2->year_build) >= 10){
                $user->c2->year_build2 = (($user->s1->year_build - $user->c2->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c2->year_build2 = 0;
            }
            if(abs($user->s1->year_build - $user->c3->year_build) >= 10){
                $user->c3->year_build3 = (($user->s1->year_build - $user->c3->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c3->year_build3 = 0;
            }
       
            //Adjustment garage space
            // if($user->s1->garage_space > 0) {
            //     $user->c1->garage_space1 = ($user->s1->garage_space - $user->c1->garage_space)*$multiplier->garage2;
            //     $user->c2->garage_space2 = ($user->s1->garage_space - $user->c2->garage_space)*$multiplier->garage2;
            //     $user->c3->garage_space3 = ($user->s1->garage_space - $user->c3->garage_space)*$multiplier->garage2;
    
            // }
            // elseif($user->s1->garage_space == 0) {
            //     if(abs($user->s1->garage_space - $user->c1->garage_space) > 1) {
            //         $d1 = abs($user->s1->garage_space - $user->c1->garage_space) - 1;
            //         $user->c1->garage_space1 = -($multiplier->garage1 + $d1*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c1->garage_space1 = -($multiplier->garage1);
            //     }

            //     if(abs($user->s1->garage_space - $user->c2->garage_space) > 1) {
            //         $d2 = abs($user->s1->garage_space - $user->c2->garage_space) - 1;
            //         $user->c2->garage_space2 = -($multiplier->garage1 + $d2*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c2->garage_space2 = -($multiplier->garage1);
            //     }

            //     if(abs($user->s1->garage_space - $user->c3->garage_space) > 1) {
            //         $d3 = abs($user->s1->garage_space - $user->c3->garage_space) - 1;
            //         $user->c3->garage_space3 = -($multiplier->garage1 + $d3*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c3->garage_space3 = -($multiplier->garage1);
            //     }
            // }

            if($user->s1->garage_space == 0) {
                $c1_garage_absolute = abs( $user->s1->garage_space - $user->c1->garage_space );
                if ($c1_garage_absolute > 1) {
                  $c1_garage_difference = $c1_garage_absolute - 1;
                  $user->c1->garage_space1 = -( $multiplier->garage1 + $c1_garage_difference * $multiplier->garage2 );
                } else {
                  $user->c1->garage_space1 = -$multiplier->garage1;
                }
          
                $c2_garage_absolute = abs( $user->s1->garage_space - $user->c2->garage_space);
                if ($c2_garage_absolute > 1) {
                  $c2_garage_difference = $c2_garage_absolute - 1;
                  $user->c2->garage_space2 = -( $multiplier->garage1 + $c2_garage_difference * $multiplier->garage2 );
                } else {
                    $user->c2->garage_space2 = -$multiplier->garage1;
                }
          
                $c3_garage_absolute = abs( $user->s1->garage_space - $user->c3->garage_space);
                if ($c3_garage_absolute > 1) {
                   $c3_garage_difference = $c3_garage_absolute - 1;
                  $user->c3->garage_space3 = -( $multiplier->garage1 + $c3_garage_difference * $multiplier->garage2 );
                } else {
                  $user->c3->garage_space3 = -$multiplier->garage1;
                }
              
              }
              else if($user->s1->garage_space > 0) {
                if($user->s1->garage_space == $user->c1->garage_space){
                  $user->c1->garage_space1 = 0;
                }
                else if($user->c1->garage_space == 0){
                  
                   $c1_garage_absolute = abs( $user->s1->garage_space - $user->c1->garage_space );
                  if ($c1_garage_absolute > 0) {
                     $c1_garage_difference = $c1_garage_absolute - 1;
                    $user->c1->garage_space1 = ( $multiplier->garage1 + $c1_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c1->garage_space1 = $multiplier->garage1;
                  }
                }
                else{
                   $c1_garage_absolute = ( $user->s1->garage_space - $user->c1->garage_space );
                  $user->c1->garage_space1 = $c1_garage_absolute * $multiplier->garage2;
                }
          
                if($user->s1->garage_space == $user->c2->garage_space){
                    $user->c2->garage_space2 = 0;
                }
                else if($user->c2->garage_space == 0){
                  $c2_garage_absolute = abs( $user->s1->garage_space - $user->c2->garage_space );
                  if ($c2_garage_absolute > 0) {
                    $c2_garage_difference = $c2_garage_absolute - 1;
                    $user->c2->garage_space2 = ( $multiplier->garage1 + $c2_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c2->garage_space2 = $multiplier->garage1;
                  }
                }
                else{
                  $c2_garage_absolute = ( $user->s1->garage_space - $user->c2->garage_space );
                  $user->c2->garage_space2 = ($c2_garage_absolute) * $multiplier->garage2;
                }
          
                if($user->s1->garage_space == $user->c3->garage_space){
                  $user->c3->garage_space3 = 0;
                }
                else if($user->c3->garage_space == 0){
                   $c3_garage_absolute = ( $user->s1->garage_space - $user->c3->garage_space );
                  if ($c3_garage_absolute > 0) {
                    $c3_garage_difference = $c3_garage_absolute - 1;
                    $user->c3->garage_space3 = ( $multiplier->garage1 + $c3_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c3->garage_space3 = $multiplier->garage1;
                  }
                }
                else{
                   $c3_garage_absolute = ( $user->s1->garage_space - $user->c3->garage_space );
                  $user->c3->garage_space3 = ($c3_garage_absolute) * $multiplier->garage2;
                }
              }
              else if ($user->s1->garage_space < 0) {
                $user->c1->garage_space3 = ($user->s1->garage_space - $user->c1->garage_space) * $multiplier->garage2;
                $user->c2->garage_space3 = ($user->s1->garage_space - $user->c2->garage_space) * $multiplier->garage2;
                $user->c3->garage_space3 = ($user->s1->garage_space - $user->c3->garage_space) * $multiplier->garage2;
              } 


            //Adjustment heat & ac
            $ac1 = ($user->s1->ac - $user->c1->ac)*$multiplier->ac;
            $ac2 = ($user->s1->ac - $user->c2->ac)*$multiplier->ac;
            $ac3 = ($user->s1->ac - $user->c3->ac)*$multiplier->ac;
            if($user->s1->heat_type == 0) {
                if($user->c1->heat_type == 0) {
                    $heat1 = 0;
                }
                else {
                    $heat1 = $multiplier->heat;
                }
                if($user->c2->heat_type == 0) {
                    $heat2 = 0;
                }
                else {
                    $heat2 = $multiplier->heat;
                }
                if($user->c3->heat_type == 0) {
                    $heat3 = 0;
                }
                else {
                    $heat3 = $multiplier->heat;
                }

            }
            else {
                if($user->c1->heat_type == 0) {
                    $heat1 = -($multiplier->heat);
                }
                else {
                    $heat1 = 0;
                }
                if($user->c2->heat_type == 0) {
                    $heat2 = -($multiplier->heat);
                }
                else {
                    $heat2 = 0;
                }
                if($user->c3->heat_type == 0) {
                    $heat3 = -($multiplier->heat);
                }
                else {
                    $heat3 = 0;
                }
            }
            $user->c1->heat_ac1 = $ac1 + $heat1;
            $user->c2->heat_ac2 = $ac2 + $heat2;
            $user->c3->heat_ac3 = $ac3 + $heat3;


            //Adjustment fire place
            $user->c1->fire_place1 = ($user->s1->fire_place - $user->c1->fire_place)*$multiplier->fire_place;
            $user->c2->fire_place2 = ($user->s1->fire_place - $user->c2->fire_place)*$multiplier->fire_place;
            $user->c3->fire_place3 = ($user->s1->fire_place - $user->c3->fire_place)*$multiplier->fire_place;
            
            //Adjustment porch
            $user->c1->porch1 = ($user->s1->porch - $user->c1->porch)*$multiplier->porch;
            $user->c2->porch2 = ($user->s1->porch - $user->c2->porch)*$multiplier->porch;
            $user->c3->porch3 = ($user->s1->porch - $user->c3->porch)*$multiplier->porch;

            //comparison property 1 details
            $detail1 = ($user->c1->sqft_above1 + $user->c1->sqft_below1 + $user->c1->year_build1 + $user->c1->garage_space1 + $user->c1->heat_ac1 + $user->c1->fire_place1 + $user->c1->porch1);

            //comparison property 2 details
            $detail2 = ($user->c2->sqft_above2 + $user->c2->sqft_below2 + $user->c2->year_build2 + $user->c2->garage_space2 + $user->c2->heat_ac2 + $user->c2->fire_place2 + $user->c2->porch2);

            //comparison property 3 details
            $detail3 = ($user->c3->sqft_above3 + $user->c3->sqft_below3 + $user->c3->year_build3 + $user->c3->garage_space3 + $user->c3->heat_ac3 + $user->c3->fire_place3 + $user->c3->porch3);
            // dd($detail1);

            $user->running_c1 = $detail1;
            $user->running_c2 = $detail2;
            $user->running_c3 = $detail3;
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);


            Property::where('id', $comp_properties->comp_prop1)->update(['detail_result' => $detail1]);
            Property::where('id', $comp_properties->comp_prop2)->update(['detail_result' => $detail2]);
            Property::where('id', $comp_properties->comp_prop3)->update(['detail_result' => $detail3]);

            Property::where('id', $subject_prop_id)->update([
                'step1' => $step1, 
                'step1_status' => 1, 
                'progress_status' => $progress_status
            ]); 
            if($user->s1->step1 == 1){
                $user->s1->step1_status = 1;
            }
            else{
                $user->s1->step1_status = 0;
            }
            
            //return json response
            return response()->json($user,200);      
                 
        }

        // Api Step2 Data
        public static function apiStep2Data(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep2DataRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            
            if($request->user_data != NULL) {
                $user_id = $request->user_data->user_id;
        
                if(isset($request->agent)) {
                    $user_id = $request->agent;
                }
                $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            }
            else {
                $admin_id = $request->admin_data->id;
                $user = Admin::where('id', $admin_id)->first();
                $type = $request->type;
            }


            $subject_prop_id = $request->subject_prop_id;

            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'address_1', 'address_2', 'images', 'type', 'progress_status', 'shortcut', 'other', 'status', 'sqft_above', 'sqft_below', 'quality', 'conditions', 'step1', 'step2', 'step3', 'step4','step2_status', 'cal_status','agency_type')->where('id', $subject_prop_id)->first();

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();
            // || ($payment != '' && $payment->amount >= 0)

            if($request->user_data != NULL) {
                if($user_id == $user->s1->user_id) {
                    $comp_properties = CompProperty::where('type', 2)->where('subject_prop_id', $subject_prop_id)->where('is_finished',1)->first();
                    if($comp_properties != ''){
                        $type = 2;
                    }
                    else{
                        $type = 1;
                    }
                }
                elseif($user_id == $user->s1->agent_id) {
                    if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                        $type = 1;
                    }
                    elseif($payment != '' && $payment->amount == 0){
                        $type = 1;
                    }
                    else {
                        $type = 2;
                    }
                    $user->type = $type;
                }
                elseif($user->s1->second_agent != NULL) {
                    if($user_id == $user->s1->second_agent) {
                        $type = 3;
                    }
                    else {
                        return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                    }
                }
                elseif($user->s1->second_agent == NULL) {
                    Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
            }

            if(isset($request->type)) { 
                if($type == 2) {
                    $user->agent_details = User::where('id', $user->s1->agent_id)->first();
                }
                elseif($type == 3) {
                    $user->agent_details = User::where('id', $user->s1->second_agent)->first();
                }
            }

            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();

            $user->c1 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'other', 'sqft_above', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'other', 'sqft_above', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'other', 'sqft_above', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();
            $user->multipliers = $multiplier;

            //calculating quality adjustments
            $subject_quality = $user->s1->quality;
            $c1_quality = $user->c1->quality;
            $c2_quality = $user->c2->quality;
            $c3_quality = $user->c3->quality;

            $user->c1->quality_c1 = ($c1_quality - $subject_quality)*($user->c1->sqft_above + ($user->c1->sqft_below)/2)*$multiplier->quality;
            $user->c2->quality_c2 = ($c2_quality - $subject_quality)*($user->c2->sqft_above + ($user->c2->sqft_below)/2)*$multiplier->quality;
            $user->c3->quality_c3 = ($c3_quality - $subject_quality)*($user->c3->sqft_above + ($user->c3->sqft_below)/2)*$multiplier->quality;

            //calculating conditions adjustments
            $subject_condition = $user->s1->conditions;
            $c1_condition = $user->c1->conditions;
            $c2_condition = $user->c2->conditions;
            $c3_condition = $user->c3->conditions;

            $user->c1->condition_c1 = ($c1_condition - $subject_condition)*($user->c1->sqft_above + ($user->c1->sqft_below)/2)*$multiplier->condition_value;
            $user->c2->condition_c2 = ($c2_condition - $subject_condition)*($user->c2->sqft_above + ($user->c2->sqft_below)/2)*$multiplier->condition_value;
            $user->c3->condition_c3 = ($c3_condition - $subject_condition)*($user->c3->sqft_above + ($user->c3->sqft_below)/2)*$multiplier->condition_value;

            $user->c1->running_step1 = $user->c1->detail_result;
            $user->c2->running_step1 = $user->c2->detail_result;
            $user->c3->running_step1 = $user->c3->detail_result;

            $user->running_c1 = ($user->c1->detail_result + $user->c1->quality_c1 + $user->c1->condition_c1);
            $user->running_c2 = ($user->c2->detail_result + $user->c2->quality_c2 + $user->c2->condition_c2);
            $user->running_c3 = ($user->c3->detail_result + $user->c3->quality_c3 + $user->c3->condition_c3);
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);

            //return json response
            return response()->json($user,200);      
                 
        }

        //Api Step2 Calculation
        public static function apiStep2Calculation(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep2Rules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;

            $subject_prop_id = $request->subject_prop_id; 

            $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'address_1', 'address_2', 'second_agent', 'images', 'progress_status', 'status', 'sqft_above', 'other', 'shortcut', 'sqft_below', 'quality', 'conditions', 'step1', 'step2', 'step3', 'step4','cal_status', 'agency_type')->where('id', $subject_prop_id)->first();
            
            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();

            if($user_id == $user->s1->user_id) {
                $type = 1;
            }
            elseif($user_id == $user->s1->agent_id) {
                if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                    $type = 1;
                }
                elseif($payment != '' && $payment->amount == 0){
                    $type = 1;
                }
                else {
                    $type = 2;
                }
            }
            elseif($user->s1->second_agent != NULL) {
                if($user_id == $user->s1->second_agent) {
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
            }
            elseif($user->s1->second_agent == NULL) {
                Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                $type = 3;
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
            }

            $progress_status = isset($request->progress_status) ? $request->progress_status : $user->s1->progress_status;

            $step2 = isset($request->step2) ? $request->step2 : $user->s1->step2;
            
            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();

            $user->c1 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'sqft_above', 'other', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'sqft_above', 'other', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'progress_status', 'status', 'sqft_above', 'other', 'sqft_below', 'quality', 'conditions', 'sale_price', 'detail_result')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            Multiplier::getMultipliers($request, $subject_prop_id, $type);
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();
            $user->multipliers = $multiplier;

            //calculating quality adjustments
            $subject_quality = isset($request->subject_quality) ? $request->subject_quality : $user->s1->quality;
            $c1_quality = isset($request->c1_quality) ? $request->c1_quality : $user->c1->quality;
            $c2_quality = isset($request->c2_quality) ? $request->c2_quality : $user->c2->quality;
            $c3_quality = isset($request->c3_quality) ? $request->c3_quality : $user->c3->quality;


            $user->c1->quality_c1 = ($c1_quality - $subject_quality)*($user->c1->sqft_above + ($user->c1->sqft_below)/2)*$multiplier->quality;
            $user->c2->quality_c2 = ($c2_quality - $subject_quality)*($user->c2->sqft_above + ($user->c2->sqft_below)/2)*$multiplier->quality;
            $user->c3->quality_c3 = ($c3_quality - $subject_quality)*($user->c3->sqft_above + ($user->c3->sqft_below)/2)*$multiplier->quality;

            //calculating conditions adjustments
            $subject_condition = isset($request->subject_condition) ? $request->subject_condition : $user->s1->conditions;
            $c1_condition = isset($request->c1_condition) ? $request->c1_condition : $user->c1->conditions;
            $c2_condition = isset($request->c2_condition) ? $request->c2_condition : $user->c2->conditions;
            $c3_condition = isset($request->c3_condition) ? $request->c3_condition : $user->c3->conditions;

            $user->c1->condition_c1 = ($c1_condition - $subject_condition)*($user->c1->sqft_above + ($user->c1->sqft_below)/2)*$multiplier->condition_value;
            $user->c2->condition_c2 = ($c2_condition - $subject_condition)*($user->c2->sqft_above + ($user->c2->sqft_below)/2)*$multiplier->condition_value;
            $user->c3->condition_c3 = ($c3_condition - $subject_condition)*($user->c3->sqft_above + ($user->c3->sqft_below)/2)*$multiplier->condition_value;


            //comparison property 1 quality
            $quality1 = ($user->c1->quality_c1 + $user->c1->condition_c1);

            //comparison property 2 quality
            $quality2 = ($user->c2->quality_c2+ $user->c2->condition_c2);

            //comparison property 3 quality
            $quality3 = ($user->c3->quality_c3 + $user->c3->condition_c3);

            $user->running_c1 = ($user->c1->detail_result + $quality1);
            $user->running_c2 = ($user->c2->detail_result + $quality2);
            $user->running_c3 = ($user->c3->detail_result + $quality3);
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);

            Property::where('id', $comp_properties->comp_prop1)->update([
                'quality' => $c1_quality,
                'conditions' => $c1_condition,
                'quality_result' => $quality1
            ]);

            Property::where('id', $comp_properties->comp_prop2)->update([
                'quality' => $c2_quality,
                'conditions' => $c2_condition,
                'quality_result' => $quality2
            ]);

            Property::where('id', $comp_properties->comp_prop3)->update([
                'quality' => $c3_quality,
                'conditions' => $c3_condition,
                'quality_result' => $quality3
            ]);

            Property::where('id', $subject_prop_id)->update([
                'quality' => $subject_quality,
                'conditions' => $subject_condition,
                'step2' => $step2,
                'step2_status' => 1,
                'progress_status' => $progress_status,
            ]);
            $user->s1->step2_status = 1;
            
            //return json response
            return response()->json($user,200);      
                 
        }

        //Api Step3 Data
        public static function apiStep3Data(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep3DataRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            if($request->user_data != NULL) {
                $user_id = $request->user_data->user_id;
                if(isset($request->agent)) {
                    $user_id = $request->agent;
                }
    
                $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            }
            else {
                $admin_id = $request->admin_data->id;
                $user = Admin::where('id', $admin_id)->first();
                $type = $request->type;
            }
        
            $subject_prop_id = $request->subject_prop_id;
        
            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'address_1', 'address_2', 'type', 'lat', 'lng', 'images', 'other', 'shortcut', 'progress_status', 'status', 'location', 'acerage', 'acerage_value', 'step1', 'step2', 'step3', 'step4','step3_status', 'cal_status','agency_type')->where('id', $subject_prop_id)->first();

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();
            // || ($payment != '' && $payment->amount >= 0)

            if($request->user_data != NULL) {
                if($user_id == $user->s1->user_id) {
                    $comp_properties = CompProperty::where('type', 2)->where('subject_prop_id', $subject_prop_id)->where('is_finished',1)->first();
                    if($comp_properties != ''){
                        $type = 2;
                    }
                    else{
                        $type = 1;
                    }
                }
                elseif($user_id == $user->s1->agent_id) {
                    if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                        $type = 1;
                    }
                    elseif($payment != '' && $payment->amount == 0){
                        $type = 1;
                    }
                    else {
                        $type = 2;
                    }
                }
                elseif($user->s1->second_agent != NULL) {
                    if($user_id == $user->s1->second_agent) {
                        $type = 3;
                    }
                    else {
                        return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                    }
                }
                elseif($user->s1->second_agent == NULL) {
                    Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
    
            }

            
            if(isset($request->type)) { 
                if($type == 2) {
                    $user->agent_details = User::where('id', $user->s1->agent_id)->first();
                }
                elseif($type == 3) {
                    $user->agent_details = User::where('id', $user->s1->second_agent)->first();
                }
            }
        

            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
        
            $user->c1 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'location', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'location', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'location', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result')->where('id', $comp_properties->comp_prop3)->first();
            $l1 = 0;
            $l2 = 0;
            $l3 = 0;
            $a1 = 0;
            $a2 = 0;
            $a3 = 0;
            if($user->s1->step3 == 1)
            {   foreach($user->c1->location as $key => $value) {
                    $l1 = $value->adjustment;
                }
                foreach($user->c2->location as $key => $value) {
                    $l2 = $value->adjustment;
                }
                foreach($user->c3->location as $key => $value) {
                    $l3 = $value->adjustment;
                }

                foreach($user->c1->acerage_value as $key => $value) {
                    $a1 = $value->adjustment;
                }
                foreach($user->c2->acerage_value as $key => $value) {
                    $a2 = $value->adjustment;
                }
                foreach($user->c3->acerage_value as $key => $value) {
                    $a3 = $value->adjustment;
                }
            }
            // else {
            //     $l1 = 0;
            //     $l2 = 0;
            //     $l3 = 0;
            //     $a1 = 0;
            //     $a2 = 0;
            //     $a3 = 0;
            // }
           
            $user->c1->running_step1 = $user->c1->detail_result;
            $user->c2->running_step1 = $user->c2->detail_result;
            $user->c3->running_step1 = $user->c3->detail_result;

            $user->c1->running_step2 = $user->c1->quality_result;
            $user->c2->running_step2 = $user->c2->quality_result;
            $user->c3->running_step2 = $user->c3->quality_result;

            $user->running_c1 = ($user->c1->detail_result + $user->c1->quality_result + $l1 + $a1);
            $user->running_c2 = ($user->c2->detail_result + $user->c2->quality_result + $l2 + $a2);
            $user->running_c3 = ($user->c3->detail_result + $user->c3->quality_result + $l3 + $a3);
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);
                            
            //return json response
            return response()->json($user,200);      
                         
        }

        //Api Step3 Calculation
        public static function apiStep3Calculation(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep3Rules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            $user_id = $request->user_data->user_id;
        
            $subject_prop_id = $request->subject_prop_id;

            $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'address_1', 'address_2', 'lat', 'lng', 'images', 'shortcut', 'acerage', 'acerage_value', 'other', 'progress_status', 'status', 'step1', 'step2', 'step3', 'step4','cal_status', 'agency_type')->where('id', $subject_prop_id)->first();


            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();

            if($user_id == $user->s1->user_id) {
                $type = 1;
            }
            elseif($user_id == $user->s1->agent_id) {
                if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                    $type = 1;
                }
                elseif($payment != '' && $payment->amount == 0){
                    $type = 1;
                }
                else {
                    $type = 2;
                }
            }
            elseif($user->s1->second_agent != NULL) {
                if($user_id == $user->s1->second_agent) {
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
            }
            elseif($user->s1->second_agent == NULL) {
                Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                $type = 3;
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
            }

            $progress_status = isset($request->progress_status) ? $request->progress_status : $user->s1->progress_status;
        
            $step3 = isset($request->step3) ? $request->step3 : $user->s1->step3;
        
            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
        
            $user->c1 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'lat', 'lng', 'other', 'progress_status', 'status', 'acerage', 'acerage_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop3)->first();
        
            //calculating location & acerage of comp 1 adjustments
            
            $location_c1 = $request->location_c1;
            $user->c1->loc_arr1 = json_decode($request->location_c1);

            foreach($user->c1->loc_arr1 as $key => $value) {
                $loc1 = $value->adjustment;
            }

            $acerage_c1 = $request->acerage_c1;
            $user->c1->acerage_arr1 = json_decode($request->acerage_c1);

            foreach($user->c1->acerage_arr1 as $key => $value) {
                $acerage1 = $value->adjustment;
            }

            $adjustment1 = $loc1 + $acerage1;

            //calculating location & acerage of comp 2 adjustments
            
            $location_c2 = $request->location_c2;
            $user->c2->loc_arr2 = json_decode($request->location_c2);
            
            foreach($user->c2->loc_arr2 as $key => $value) {
                $loc2 = $value->adjustment;
            }
            
            $acerage_c2 = $request->acerage_c2;
            $user->c2->acerage_arr2 = json_decode($request->acerage_c2);
            
            foreach($user->c2->acerage_arr2 as $key => $value) {
                $acerage2 = $value->adjustment;
            }
            
            $adjustment2 = $loc2 + $acerage2;

            //calculating location & acerage of comp 3 adjustments
            
            $location_c3 = $request->location_c3;
            $user->c3->loc_arr3 = json_decode($request->location_c3);
            
            foreach($user->c3->loc_arr3 as $key => $value) {
                $loc3 = $value->adjustment;
            }
            
            $acerage_c3 = $request->acerage_c3;
            $user->c3->acerage_arr3 = json_decode($request->acerage_c3);
            
            foreach($user->c3->acerage_arr3 as $key => $value) {
                $acerage3 = $value->adjustment;
            }
        
            $adjustment3 = $loc3 + $acerage3;
        
            $user->running_c1 = ($user->c1->detail_result + $user->c1->quality_result + $adjustment1);
            $user->running_c2 = ($user->c2->detail_result + $user->c2->quality_result + $adjustment2);
            $user->running_c3 = ($user->c3->detail_result + $user->c3->quality_result + $adjustment3);
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);
        
        
            Property::where('id', $comp_properties->comp_prop1)->update([
                'location' => $request->location_c1,
                'acerage_value' => $request->acerage_c1,
                'acerage' => $request->acre_c1,
                'location_result' => $adjustment1
            ]);

            Property::where('id', $comp_properties->comp_prop2)->update([
                'location' => $request->location_c2,
                'acerage_value' => $request->acerage_c2,
                'acerage' => $request->acre_c2,
                'location_result' => $adjustment2
            ]);

            Property::where('id', $comp_properties->comp_prop3)->update([
                'location' => $request->location_c3,
                'acerage_value' => $request->acerage_c3,
                'acerage' => $request->acre_c3,
                'location_result' => $adjustment3
            ]);

            Property::where('id', $subject_prop_id)->update([
                'step3' => $step3,
                'step3_status' => 1,
                'progress_status' => $progress_status    
            ]);
            $user->s1->step3_status = 1;

                    
            //return json response
            return response()->json($user,200);      
                         
        }

        //Api Step4 Calculation
        public static function apiStep4Data(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep4DataRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            if($request->user_data != NULL) {
                $user_id = $request->user_data->user_id;
                if(isset($request->agent)) {
                    $user_id = $request->agent;
                }
    
                $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            }
            else {
                $admin_id = $request->admin_data->id;
                $user = Admin::where('id', $admin_id)->first();
                $type = $request->type;
            }

            $subject_prop_id = $request->subject_prop_id;

            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'type','address_1', 'address_2', 'images', 'type', 'other', 'progress_status', 'shortcut', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees_inclusions','condo_fees', 'condo_arr', 'condo_value', 'step1', 'step2', 'step3', 'step4','step4_status','cal_status', 'agency_type')->where('id', $subject_prop_id)->first();

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();
            // || ($payment != '' && $payment->amount >= 0)

            if($request->user_data != NULL) {
                if($user_id == $user->s1->user_id) {
                    $comp_properties = CompProperty::where('type', 2)->where('subject_prop_id', $subject_prop_id)->where('is_finished',1)->first();
                    if($comp_properties != ''){
                        $type = 2;
                    }
                    else{
                        $type = 1;
                    }
                }
                elseif($user_id == $user->s1->agent_id) {
                    if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                        $type = 1;
                    }
                    elseif($payment != '' && $payment->amount == 0){
                        $type = 1;
                    }
                    else {
                        $type = 2;
                    }
                }
                elseif($user->s1->second_agent != NULL) {
                    if($user_id == $user->s1->second_agent) {
                        $type = 3;
                    }
                    else {
                        return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                    }
                }
                elseif($user->s1->second_agent == NULL) {
                    Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
    
            }

            
            if(isset($request->type)) { 
                if($type == 2) {
                    $user->agent_details = User::where('id', $user->s1->agent_id)->first();
                }
                elseif($type == 3) {
                    $user->agent_details = User::where('id', $user->s1->second_agent)->first();
                }
            }

            
            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();

            $user->c1 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees_inclusions','condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees_inclusions','condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add','condo_fees_inclusions', 'condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();

            //Adjustment bedroom above
            $user->c1->bedroom_above1 = ($user->s1->bedroom_above - $user->c1->bedroom_above)*$multiplier->bedroom_above;
            $user->c2->bedroom_above2 = ($user->s1->bedroom_above - $user->c2->bedroom_above)*$multiplier->bedroom_above;
            $user->c3->bedroom_above3 = ($user->s1->bedroom_above - $user->c3->bedroom_above)*$multiplier->bedroom_above;

            //Adjustment bedroom below
            $user->c1->bedroom_below1 = ($user->s1->bedroom_below - $user->c1->bedroom_below)*$multiplier->bedroom_below;
            $user->c2->bedroom_below2 = ($user->s1->bedroom_below - $user->c2->bedroom_below)*$multiplier->bedroom_below;
            $user->c3->bedroom_below3 = ($user->s1->bedroom_below - $user->c3->bedroom_below)*$multiplier->bedroom_below;

            //Adjustment bathroom above
            $user->c1->bathroom_above1 = ($user->s1->bathroom_above - $user->c1->bathroom_above)*$multiplier->bathroom_above;
            $user->c2->bathroom_above2 = ($user->s1->bathroom_above - $user->c2->bathroom_above)*$multiplier->bathroom_above;
            $user->c3->bathroom_above3 = ($user->s1->bathroom_above - $user->c3->bathroom_above)*$multiplier->bathroom_above;
            
            //Adjustment bathroom below
            $user->c1->bathroom_below1 = ($user->s1->bathroom_below - $user->c1->bathroom_below)*$multiplier->bathroom_below;
            $user->c2->bathroom_below2 = ($user->s1->bathroom_below - $user->c2->bathroom_below)*$multiplier->bathroom_below;
            $user->c3->bathroom_below3 = ($user->s1->bathroom_below - $user->c3->bathroom_below)*$multiplier->bathroom_below;

            $user->multipliers = $multiplier;

            if($user->c1->room_result != NULL) {
                $room1 = $user->c1->room_result;
            }
            else {
                $room1 = ($user->c1->bedroom_above1 + $user->c1->bedroom_below1 + $user->c1->bathroom_above1 + $user->c1->bathroom_below1);
            }

            if($user->c2->room_result != NULL) {
                $room2 = $user->c2->room_result;
            }
            else {
                $room2 = ($user->c2->bedroom_above2 + $user->c2->bedroom_below2 + $user->c2->bathroom_above2 + $user->c2->bathroom_below2);
            }
            $user->ba1 = $user->c2->bedroom_above2;
            $user->bb1 = $user->c2->bedroom_below2;
            $user->ba2 = $user->c2->bedroom_above2;
            $user->bb2 = $user->c2->bathroom_below2;

            if($user->c3->room_result != NULL) {
                $room3 = $user->c3->room_result;
            }
            else {
                $room3 = ($user->c3->bedroom_above3 + $user->c3->bedroom_below3 + $user->c3->bathroom_above3 + $user->c3->bathroom_below3);
            }

            $user->c1->running_step1 = $user->c1->detail_result;
            $user->c2->running_step1 = $user->c2->detail_result;
            $user->c3->running_step1 = $user->c3->detail_result;

            $user->c1->running_step2 = $user->c1->quality_result;
            $user->c2->running_step2 = $user->c2->quality_result;
            $user->c3->running_step2 = $user->c3->quality_result;

            $user->c1->running_step3 = $user->c1->location_result;
            $user->c2->running_step3 = $user->c2->location_result;
            $user->c3->running_step3 = $user->c3->location_result;
            
            

            $user->running_c1 = ($user->c1->detail_result + $user->c1->quality_result + $user->c1->location_result + $room1);
            $user->running_c2 = ($user->c2->detail_result + $user->c2->quality_result + $user->c2->location_result + $room2);
            $user->running_c3 = ($user->c3->detail_result + $user->c3->quality_result + $user->c3->location_result + $room3);
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);

            //return json response
            return response()->json($user,200);      
                 
        }

        //Api Step4 Calculation
        public static function apiStep4Calculation(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiStep4Rules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $user_id = $request->user_data->user_id;

            $subject_prop_id = $request->subject_prop_id;

            $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'type','address_1', 'address_2', 'images', 'progress_status', 'other', 'status', 'shortcut', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees', 'condo_arr', 'condo_value', 'step1', 'step2', 'step3', 'step4', 'agency_type')->where('id', $subject_prop_id)->first();

            $payment = Payment::orderBy('created_at', 'desc')->where('property_id',$subject_prop_id)->where('user_id', $user->s1->user_id)->whereIn('payment_type', [7])->first();

            if($user_id == $user->s1->user_id) {
                $type = 1;
            }
            elseif($user_id == $user->s1->agent_id) {
                if($user->s1->shortcut == 1 || ($payment != '' && $payment->amount > 0)) {
                    $type = 1;
                }
                elseif($payment != '' && $payment->amount == 0){
                    $type = 1;
                }
                else {
                    $type = 2;
                }
            }
            elseif($user->s1->second_agent != NULL) {
                if($user_id == $user->s1->second_agent) {
                    $type = 3;
                }
                else {
                    return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
                }
            }
            elseif($user->s1->second_agent == NULL) {
                Property::where('id', $subject_prop_id)->update(['second_agent' => $user_id]);
                $type = 3;
            }
            else {
                return response()->json(['error' => 'bad_request', 'error_description' => 'You can not make calculations for this property.'], 400);
            }

            $progress_status = isset($request->progress_status) ? $request->progress_status : $user->s1->progress_status;

            $step4 = isset($request->step4) ? $request->step4 : $user->s1->step4;

            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();

            $user->c1 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'type','address_1', 'address_2', 'progress_status', 'other', 'status', 'bedroom_above', 'bedroom_below', 'bathroom_above', 'bathroom_below', 'total_rooms', 'basement_finish', 'total_rooms_value', 'basement_value', 'positive_add', 'negative_add', 'condo_fees', 'condo_arr', 'condo_value', 'sale_price', 'detail_result', 'quality_result', 'location_result', 'room_result')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            Multiplier::getMultipliers($request, $subject_prop_id, $type);
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();
            $user->multipliers = $multiplier;


            //Condo Fees
            if($user->s1->type == 3) {

                //Check Validation	        
                $validation = Validator::make($request->all(), CompProperty::$apiCondoRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);


                $subject_condo = $request->subject_condo;

                $subject_condo_fees_inclusions = $request->subject_condo_fees_inclusions;

                $condo1 = $request->condo1;
                $condo_fees_inclusions1 = $request->condo_fees_inclusions1;
                $user->c1->condo1_arr = json_decode($request->condo1);

                foreach($user->c1->condo1_arr as $key => $value) {
                    $user->c1->condo1 = $value->difference * 60;
                }

                $condo2 = $request->condo2;
                $condo_fees_inclusions2 = $request->condo_fees_inclusions2;
                $user->c2->condo2_arr = json_decode($request->condo2);

                foreach($user->c2->condo2_arr as $key => $value) {
                    $user->c2->condo2 = $value->difference * 60;
                }

                $condo3 = $request->condo3;
                $condo_fees_inclusions3 = $request->condo_fees_inclusions3;
                $user->c3->condo3_arr = json_decode($request->condo3);

                foreach($user->c3->condo3_arr as $key => $value) {
                    $user->c3->condo3 = $value->difference * 60;
                }

            }
            else {
                $user->s1->condo_arr = NULL;
                $user->c1->condo1_arr = NULL;
                $user->c2->condo2_arr = NULL;
                $user->c3->condo3_arr = NULL;
                $user->c1->condo1 = 0;
                $user->c2->condo2 = 0;
                $user->c3->condo3 = 0;
                $subject_condo = NULL;
                $condo1 = NULL;
                $condo2 = NULL;
                $condo3 = NULL;
                $subject_condo_fees_inclusions = NULL;
                $condo_fees_inclusions1= NULL;
                $condo_fees_inclusions2=NULL;
                $condo_fees_inclusions3=NULL;
            }

            //Adjustment bedroom above
            $user->c1->bedroom_above1 = ($user->s1->bedroom_above - $user->c1->bedroom_above)*$multiplier->bedroom_above;
            $user->c2->bedroom_above2 = ($user->s1->bedroom_above - $user->c2->bedroom_above)*$multiplier->bedroom_above;
            $user->c3->bedroom_above3 = ($user->s1->bedroom_above - $user->c3->bedroom_above)*$multiplier->bedroom_above;

            //Adjustment bedroom below
            $user->c1->bedroom_below1 = ($user->s1->bedroom_below - $user->c1->bedroom_below)*$multiplier->bedroom_below;
            $user->c2->bedroom_below2 = ($user->s1->bedroom_below - $user->c2->bedroom_below)*$multiplier->bedroom_below;
            $user->c3->bedroom_below3 = ($user->s1->bedroom_below - $user->c3->bedroom_below)*$multiplier->bedroom_below;

            //Adjustment bathroom above
            $user->c1->bathroom_above1 = ($user->s1->bathroom_above - $user->c1->bathroom_above)*$multiplier->bathroom_above;
            $user->c2->bathroom_above2 = ($user->s1->bathroom_above - $user->c2->bathroom_above)*$multiplier->bathroom_above;
            $user->c3->bathroom_above3 = ($user->s1->bathroom_above - $user->c3->bathroom_above)*$multiplier->bathroom_above;
            
            //Adjustment bathroom below
            $user->c1->bathroom_below1 = ($user->s1->bathroom_below - $user->c1->bathroom_below)*$multiplier->bathroom_below;
            $user->c2->bathroom_below2 = ($user->s1->bathroom_below - $user->c2->bathroom_below)*$multiplier->bathroom_below;
            $user->c3->bathroom_below3 = ($user->s1->bathroom_below - $user->c3->bathroom_below)*$multiplier->bathroom_below;


            //Adjustment total rooms
            $user->c1->total_rooms1 = $request->total_rooms1;
            $user->c2->total_rooms2 = $request->total_rooms2;
            $user->c3->total_rooms3 = $request->total_rooms3;
            
            //Adjustment basement finish
            $user->c1->basement_finish1 = $request->basement_finish1;
            $user->c2->basement_finish2 = $request->basement_finish2;
            $user->c3->basement_finish3 = $request->basement_finish3;

            //basement finish
            $user->c1->basement1 = $request->basement1;
            $user->c2->basement2 = $request->basement2;
            $user->c3->basement3 = $request->basement3;
    
            //Adjustment positive features
            $cost1 = 0;
            if(!empty($request->positive_s1))
            {
                $positive_s1 = $request->positive_s1;
                $positive_s1_arr = json_decode($request->positive_s1);

                foreach($positive_s1_arr as $key => $value) {
                    $cost1 = $cost1 + $value->cost;
                }
            }
                $user->s1->positive_cost = $cost1;
                $user->s1->positive_value = $positive_s1_arr;
            
            //comp 1
            $cost2 = 0;
            if(!empty($request->positive_c1))
            {
                $positive_c1 = $request->positive_c1;
                $positive_c1_arr = json_decode($request->positive_c1);

                foreach($positive_c1_arr as $key => $value) {
                    $cost2 = $cost2 + $value->cost;
                }
            }
            $user->c1->positive_cost_c1 = $cost2;
            $user->c1->positive_value = $positive_c1_arr;

            //comp 2
            $cost3 = 0;
            if(!empty($request->positive_c2))
            {    
                $positive_c2 = $request->positive_c2;
                $positive_c2_arr = json_decode($request->positive_c2);

                foreach($positive_c2_arr as $key => $value) {
                    $cost3 = $cost3 + $value->cost;
                }
            }
            $user->c2->positive_cost_c2 = $cost3;
            $user->c2->positive_value = $positive_c2_arr;

            //comp 3
            $cost4 = 0;
            if(!empty($request->positive_c3))
            {    
                $positive_c3 = $request->positive_c3;
                $positive_c3_arr = json_decode($request->positive_c3);

                foreach($positive_c3_arr as $key => $value) {
                    $cost4 = $cost4 + $value->cost;
                }
            }
            $user->c3->positive_cost_c3 = $cost4;
            $user->c3->positive_value = $positive_c3_arr;


            //Adjustment negative features
            //subject
            $cost1 = 0;
            if(!empty($request->negative_s1))
            {
                $negative_s1 = $request->negative_s1;
                $negative_s1_arr = json_decode($request->negative_s1);

                foreach($negative_s1_arr as $key => $value) {
                    $cost1 = $cost1 + $value->cost;
                }
            }
                $user->s1->negative_cost = $cost1;
                $user->s1->negative_value = $negative_s1_arr;
            
            //comp 1
            $cost2 = 0;
            if(!empty($request->negative_c1))
            {
                $negative_c1 = $request->negative_c1;
                $negative_c1_arr = json_decode($request->negative_c1);

                foreach($negative_c1_arr as $key => $value) {
                    $cost2 = $cost2 + $value->cost;
                }
            }
            $user->c1->negative_cost_c1 = $cost2;
            $user->c1->negative_value = $negative_c1_arr;

            //comp 2
            $cost3 = 0;
            if(!empty($request->negative_c2))
            {    
                $negative_c2 = $request->negative_c2;
                $negative_c2_arr = json_decode($request->negative_c2);

                foreach($negative_c2_arr as $key => $value) {
                    $cost3 = $cost3 + $value->cost;
                }
            }
            $user->c2->negative_cost_c2 = $cost3;
            $user->c2->negative_value = $negative_c2_arr;

            //comp 3
            $cost4 = 0;
            if(!empty($request->negative_c3))
            {    
                $negative_c3 = $request->negative_c3;
                $negative_c3_arr = json_decode($request->negative_c3);

                foreach($negative_c3_arr as $key => $value) {
                    $cost4 = $cost4 + $value->cost;
                }
            }
            $user->c3->negative_cost_c3 = $cost4;
            $user->c3->negative_value = $negative_c3_arr;


            //comparison property 1 room
            $room1 = ($user->c1->bedroom_above1 + $user->c1->bedroom_below1 + $user->c1->bathroom_above1 + $user->c1->bathroom_below1 + $user->c1->total_rooms1 + $user->c1->basement_finish1 + $user->c1->positive_cost_c1 + $user->c1->negative_cost_c1 + $user->c1->condo1) + $user->s1->positive_cost + $user->s1->negative_cost;

            //comparison property 2 room
            $room2 = ($user->c2->bedroom_above2 + $user->c2->bedroom_below2 + $user->c2->bathroom_above2 + $user->c2->bathroom_below2 + $user->c2->total_rooms2 + $user->c2->basement_finish2 + $user->c2->positive_cost_c2 + $user->c2->negative_cost_c2 + $user->c2->condo2) + $user->s1->positive_cost + $user->s1->negative_cost;

            //comparison property 3 room
            $room3 = ($user->c3->bedroom_above3 + $user->c3->bedroom_below3 + $user->c3->bathroom_above3 + $user->c3->bathroom_below3 + $user->c3->total_rooms3 + $user->c3->basement_finish3 + $user->c3->positive_cost_c3 + $user->c3->negative_cost_c3 + $user->c3->condo3) + $user->s1->positive_cost + $user->s1->negative_cost;
           
            $total1 = $user->c1->sale_price + $user->c1->detail_result + $user->c1->quality_result + $user->c1->location_result + $room1;
            $total2 = $user->c2->sale_price + $user->c2->detail_result + $user->c2->quality_result + $user->c2->location_result + $room2;
            $total3 = $user->c3->sale_price + $user->c3->detail_result + $user->c3->quality_result + $user->c3->location_result + $room3;

            $user->running_c1 = $total1;
            $user->running_c2 = $total2;
            $user->running_c3 = $total3;
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);
            
            if($request->user_data->user->user_type == 1) {
                $review_type = 1;
            }
            else { 
                $review_type = 2;
            }

            Property::where('id', $comp_properties->comp_prop1)->update([
                'positive_add' => $positive_c1,
                'negative_add' => $negative_c1,
                'total_rooms_value' => $request->total_rooms1,
                'basement_finish' => $request->basement1,
                'basement_value' => $request->basement_finish1,
                'condo_arr' => $condo1,
                'condo_fees_inclusions' => $condo_fees_inclusions1,
                'condo_value' => $user->c1->condo1,
                'room_result' => $room1, 
            ]);

            Property::where('id', $comp_properties->comp_prop2)->update([
                'positive_add' => $positive_c2,
                'negative_add' => $negative_c2,
                'total_rooms_value' => $request->total_rooms2,
                'basement_finish' => $request->basement2,
                'basement_value' => $request->basement_finish2,
                'condo_arr' => $condo2,
                'condo_fees_inclusions' => $condo_fees_inclusions2,
                'condo_value' => $user->c2->condo2,
                'room_result' => $room2, 
            ]);

            Property::where('id', $comp_properties->comp_prop3)->update([
                'positive_add' => $positive_c3,
                'negative_add' => $negative_c3,
                'total_rooms_value' => $request->total_rooms3,
                'basement_finish' => $request->basement3,
                'basement_value' => $request->basement_finish3,
                'condo_arr' => $condo3,
                'condo_fees_inclusions' => $condo_fees_inclusions3,
                'condo_value' => $user->c3->condo3,
                'room_result' => $room3, 
            ]);

            Property::where('id', $subject_prop_id)->update([
                'progress_status' => $progress_status,
                'positive_add' => $positive_s1,
                'negative_add' => $negative_s1,
                'condo_arr' => $subject_condo,
                'condo_fees_inclusions' => $subject_condo_fees_inclusions,
                'step4' => $step4,
                'step4_status' => 1,
                'cal_status' => 1
            ]);
            $user->s1->step4_status = 1;
            $user->s1->cal_status = 1;

            
            //return json response
            return response()->json($user,200);      
                 
        }

    /*-----------------------------------------------------
    -------------------------------------------------------
        * Review Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api Review
        public static function apiReviewPage(Request $request) {
            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiReviewPageRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $review_type = 0;
        
            if($request->user_data != NULL) {
                $user_id = $request->user_data->user_id;
                if(isset($request->agent)) {
                    $user_id = $request->agent;
                }
    
                $user = User::select('id', 'first_name', 'image')->where('id', $user_id)->first();
            }
            else {
                $admin_id = $request->admin_data->id;
                $user = Admin::where('id', $admin_id)->first();
                $user_id = 0;
                $review_type = $request->review_type;
            }
        
            $subject_prop_id = $request->subject_prop_id;
        
            $user->s1 = Property::select('id', 'address_1', 'user_id', 'agent_id', 'second_agent', 'type', 'city', 'county', 'country', 'zip_code', 'address_2', 'shortcut', 'status', 'sale_price', 'chat_payment', 'second_chat', 'unit1', 'unit2', 'final_price', 'agency_type', 'selling_status','comment')->where('id', $subject_prop_id)->first();

            $user->review1 = CompProperty::getReview($user_id, $subject_prop_id, $type = 1, $user->s1->sale_price, $user->s1->user_id, $review_type);
            $user->review2 = CompProperty::getReview($user_id, $subject_prop_id, $type = 2, $user->s1->sale_price, $user->s1->user_id, $review_type);
            $user->review3 = CompProperty::getReview($user_id, $subject_prop_id, $type = 3, $user->s1->sale_price, $user->s1->user_id, $review_type);

        
            if($user->s1->status != 3) {
                Property::where('id', $subject_prop_id)->update(['final_price' => $user->review1['total_avg']]);
            }

            $cash_flow = CashFlow::where('property_id', $subject_prop_id)->first();
            if(empty($cash_flow)) {
                $user->cash_flow = NULL;
            }
            else {
                $user->cash_flow = $cash_flow;
            }

            $homey_opinions = HomeyOpinion::where([
                ['user_id', '=', $user->s1->user_id],
                ['property_id', '=', $subject_prop_id],
            ])->pluck('id');

            if($request->user_data != NULL) {
                $opinion_id = HomeyOpinion::where([
                    ['user_id', '=', $user->s1->user_id],
                    ['property_id', '=', $subject_prop_id],
                    ['agent_id', '=', $user_id],
                ])->pluck('id');

                if(empty($opinion_id)) {
                    $user->opinion_id = NULL;
                }
                else {
                    $user->opinion_id = $opinion_id;
                }
            }     

            if(empty($homey_opinions)) {
                $user->homey_opinions = NULL;
            }
            else {
                $user->homey_opinions = $homey_opinions;
            }

            $user->communication1 = Chat::orderBy('id', 'desc')->where([
                ['property_id', $user->s1->id],
                ['sent_to', $user->s1->user_id],
                ['sent_by', $user->s1->agent_id]
            ])->orWhere([
                ['property_id', $user->s1->id],
                ['sent_by', $user->s1->user_id],
                ['sent_to', $user->s1->agent_id]
            ])->first();

            $user->communication2 = Chat::orderBy('id', 'desc')->where([
                ['property_id', $user->s1->id],
                ['sent_to', $user->s1->user_id],
                ['sent_by', $user->s1->second_agent]
            ])->orWhere([
                ['property_id', $user->s1->id],
                ['sent_by', $user->s1->user_id],
                ['sent_to', $user->s1->second_agent]
            ])->first();

            return $user;
        } 

        //Api cash flow
        public static function apiCashFlow(Request $request) {
    
            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiCashFlowRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $flow_entry = CashFlow::where('property_id', $request->property_id)->first();

            //loan calculator
            $pp = $request->purchase_price;
            $lt = $request->loan_term;
            $r = $request->rate;
            $dp = $request->down_payment_rate;

            $R = $r/1200;
            $L = $lt*12;
            $S = pow((1 + $R),$L);

            $down_payment = round(($pp * ($dp/100)), 2);
            $monthly_payment = round(((($pp - $down_payment)*$R*$S)/($S - 1)), 2);
            $first_principle = round(($monthly_payment - (($pp - $down_payment) * $R)), 2);
            
            //cash flow analysis
            $unit1 = $request->unit1;
            $unit2 = $request->unit2;
            $total = $unit1 + $unit2;
            $other_expenses = $request->other_expenses;

            $cost = 0;
            $expense = json_decode($other_expenses);
            foreach($expense as $key => $value) {
                $cost = $cost + $value->cost;
            }
            $sum = round(($cost + $monthly_payment), 2);
            $cash_flow = round(($total - $sum), 2);
            $income = round(($first_principle + $cash_flow), 2);
            $investment_return = round((($income*1200)/$down_payment), 2);
                
            $flow = CashFlow::updateOrCreate(
                ['id'=> $request->flow_id],
                [
                    "property_id" => $request->property_id,
                    "unit1" => $unit1,
                    "unit2" => $unit2,
                    "expenses" => $other_expenses,
                    "cash_flow" => $cash_flow,
                    "loan_term" => $lt,
                    "rate" => $r,
                    "monthly_payment" => $monthly_payment,
                    "first_payment_principle" => $first_principle,
                    "income" => $income,
                    "down_payment" => $down_payment,
                    "return_investment" => $investment_return,
                    "purchase_price" => $pp,
                    "down_payment_rate" => $dp,
                    "total" => $total,
                    "sum" => $sum,

                ]
            );


            return response()->json($flow, 200);

        } 

        /*-----------------------------------------------------
        * Version 2 Functions
        ------------------------------------------------------- */

            //Api Finish Calculation
            public static function apiFinishCalculation(Request $request) {
                //Check Validation	        
                $validation = Validator::make($request->all(), CompProperty::$apiReviewPageRules);
                if($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                CompProperty::where('subject_prop_id', $request->subject_prop_id)->where('is_finished', 0)->update(['is_finished' => 1]);
                $property = Property::where('id', $request->subject_prop_id)->first();
                $payment = Payment::where('property_id',$property->id)->first();
                $getUserDetail = User::where('id',$property->user_id)->first();
                $userEmail = $getUserDetail->email;
                if($payment != ''){
                    if($payment->payment_type == 7 && $payment->amount != 0){
                        $mail = Mail::send("emails.paid-calculation-completed",['userData'=>$getUserDetail], function ($m) use ($userEmail) {
                            $m->to($userEmail)->subject("Calculation completed");
                        });
                    }
                    else{
                        $mail = Mail::send("emails.calculation-completed",['userData'=>$getUserDetail], function ($m) use ($userEmail) {
                            $m->to($userEmail)->subject("Calculation completed");
                        });
                    }
                }
                else{
                    $mail = Mail::send("emails.calculation-completed",['userData'=>$getUserDetail], function ($m) use ($userEmail) {
                        $m->to($userEmail)->subject("Calculation completed");
                    });
                }
                
                return response()->json(['data' => "Calculation Finished"], 200);
            }

            //Api Add Comment
            public static function apiAddComment(Request $request) {
                
                //Check Validation	        
                $validation = Validator::make($request->all(),['property_id' => 'required']);
                if ($validation->fails())
                    return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

                Property::where('id', $request->property_id)->update(['comment' => $request->comment]);
                return response()->json(['data' => "Comment Added"], 200);
            }

            // Api comlete Calculation
            public static function apiCompleteCalculation(Request $request) {

                $step1 = AdjustmentController::apiStep1Calculation($request);
                $step2 = AdjustmentController::apiStep2Data($request);
                $step3 = AdjustmentController::apiStep3Data($request);
                $step4 = AdjustmentController::apiStep4Data($request);
                $review = AdjustmentController::apiReviewPage($request);

                $response = [
                    'step1' => $step1,
                    'step2' => $step2,
                    'step3' => $step3,
                    'step4' => $step4,
                    'review' => $review
                ];
                return $response;
            }

    /*-----------------------------------------------------
    -------------------------------------------------------
        * Homey Opinion Functions
    -------------------------------------------------------
    ------------------------------------------------------- */

        //Api Homey Opinion
        public static function apiHomeyOpinion(Request $request) {

            //Check Validation	        
            $validation = Validator::make($request->all(), CompProperty::$apiReviewPageRules);
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
            $user_id = $request->user_data->user_id;
        
            $subject_prop_id = $request->subject_prop_id;

            $property = Property::where('id', $subject_prop_id)->first();

            $opinion = HomeyOpinion::orderBy('id', 'desc')->where([
                ['user_id', '=', $property->user_id],
                ['property_id', '=', $subject_prop_id],
            ])->first();

            if(empty($opinion)) {
                // if($property->agency_type == 3 || $property->agency_type == 1 || $property->agency_type == 4) {
                //     return 2;
                // }
                // else {
                    Property::where('id', $subject_prop_id)->update(['status' => 4]);
                    $send_to = $property->agent()->value('email');
                    $name = $property->agent()->value('first_name');
                    $msg = "Your Client has requested you to review their Calculation.";
                    $subject = "Review Requested";
                    $url = "https://homeease.pro/dashboard/agent";

                     // Send Email
                    $mail = Mail::send("emails.update", ['name' => $name, 'msg' => $msg, 'url' => $url], function ($m) use ($send_to, $subject) {
                        $m->to($send_to)->subject($subject);
                    });

                    return 2;
                // }
                
            }
            else {

                return 1;
                // Property::where('id', $subject_prop_id)->update(['status' => 8]);
            }

             return response()->json(['data' => "Status updated"], 200);
        }

    /*===========Api Homey Opinion Payment==========*/
    public static function apiHomeyOpinionPayment(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), CompProperty::$apiReviewPageRules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
    
        $user_id = $request->user_data->user_id;
    
        $subject_prop_id = $request->subject_prop_id;

        $property = Property::where('id', $subject_prop_id)->first();

        $opinion = HomeyOpinion::orderBy('id', 'desc')->where([
            ['user_id', '=', $property->user_id],
            ['property_id', '=', $subject_prop_id],
        ])->first();

        // if(empty($opinion)) {
            $amount = 5500;
            $stripe = CommonFunction::makeStripePayment($request, $amount);
            if($stripe != 'payment_success') {
                return $stripe;
            }
            Property::where('id', $subject_prop_id)->update(['status' => 4]);

            $send_to = $property->agent()->value('email');
            $name = $property->agent()->value('first_name');
            $msg = "Your Client has requested you to review their Calculation.";
            $subject = "Review Requested";
            $url = "https://homeease.pro/dashboard/agent";

            // Send Email
            $mail = Mail::send("emails.update", ['name' => $name, 'msg' => $msg, 'url' => $url], function ($m) use ($send_to, $subject) {
                $m->to($send_to)->subject($subject);
            });

            //payment update to user
            $payment_by = $property->user()->value('first_name');
            $payment_by_email = $property->user()->value('email');
            $text = "Agent Review for your property";
            $description = "Professional review of your calculations";

            // Send Email
            $mail = Mail::send("emails.professional-review-payment", ['name' => $payment_by, 'text' => $text, 'amount' => 55.00], function ($m) use ($payment_by_email) {
                $m->to($payment_by_email)->subject("Successful Payment");
            });


            // //new homey opinion
            $new_opinion = new HomeyOpinion;
            $new_opinion->user_id = $property->user_id;
            $new_opinion->property_id = $subject_prop_id;
            $new_opinion->agent_id = $user_id;
            $new_opinion->opinion_no = 1;
            $new_opinion->comment = isset($request->comment) ? $request->comment : NULL;
            $new_opinion->save();

            CompProperty::where('subject_prop_id', $request->subject_prop_id)->where('type', 2)->where('is_finished', 0)->update(['homey_opinion_id' => $new_opinion->id]);
            
            //inserting in payments
            $payment = new Payment;
            $payment->user_id = $user_id;
            $payment->agent_id = $property->agent_id;
            $payment->property_id = $subject_prop_id;
            $payment->payment_type = 2;
            $payment->amount = 55;
            $payment->save();
        // }
        // else {
        //     $amount = 5500;
        //     $stripe = CommonFunction::makeStripePayment($request, $amount);
        //     if($stripe != 'payment_success') {
        //         return $stripe;
        //     }
        //     Property::where('id', $subject_prop_id)->update(['status' => 8]);
            
        //     //inserting in payments
        //     $payment = new Payment;
        //     $payment->user_id = $user_id;
        //     $payment->property_id = $subject_prop_id;
        //     $payment->payment_type = 3;
        //     $payment->amount = 55;
        //     $payment->save();

        //     //payment update to user
        //     $payment_by = $property->user()->value('first_name');
        //     $payment_by_email = $property->user()->value('email');
        //     $text = "Second Agent Review for your property";
        //     $description = "Professional review of your calculations";

        //     // Send Email
        //     $mail = Mail::send("emails.professional-review-payment", ['name' => $payment_by, 'text' => $text, 'amount' => 55.00], function ($m) use ($payment_by_email) {
        //         $m->to($payment_by_email)->subject("Successful Payment");
        //     });
        // }
        return response()->json(['data' => "Status updated"], 200);
    }

}
