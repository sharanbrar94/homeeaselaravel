<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExpenseDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Email;
use App\Models\Contact;
use App\Models\Property;
use Carbon\Carbon;
use App\Library\CommonFunction;
use Stripe;
use AWS;
use App\Models\Chat;
use App\Models\Payment;

class ApiController extends Controller
{

    /*-----------------------------------------------------
	-------------------------------------------------------
     * Functions
    -------------------------------------------------------
    ------------------------------------------------------- */
    
        //Api Expense Details Listing
        public static function apiExpenseDetailsListing() {

            $expense_details = ExpenseDetail::get();

             //Send JSON response
             return response()->json($expense_details,200); 
        }

        // Api Contact Us

        public static function apiContactUs(Request $request) {

            // Check Validation	        
            // $validation = Validator::make($request->all(), array('first_name' => 'required', 'email' => 'required|email', 'message' => 'required'));
            // if($validation->fails())
            //     return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);   
            $dbEmail = Email::where('id',1)->first();
            $client_email = $dbEmail->client_email;
            $test_email = $dbEmail->test_email;
            
            $contact = new Contact;
            $contact->first_name = $request->first_name;
            $contact->email = $request->email;
            $contact->phone_number = $request->phone_number;
            if($request->type == 1)
            {
                $contact->subject = $request->subject;
                $contact->message = $request->message;
                $contact->save();
                $mail = Mail::send("emails.contact-us", [ 'contact' => $request], function ($m) use ($client_email, $contact) {
                    $m->to($client_email)->subject('Contact Us');
                });
                $mail = Mail::send("emails.contact-us", [ 'contact' => $request], function ($m) use ($test_email, $contact) {
                    $m->to($test_email)->subject('Contact Us');
                });
            }
            else
            {
                
                $contact->mls = $request->message;
                $contact->state = $request->state;
                $contact->city = $request->city;
                $contact->website = $request->website;
                $contact->save();
                $mail = Mail::send("emails.contact-us", [ 'contact' => $request], function ($m) use ($client_email, $contact) {
                    $m->to($client_email)->subject('New agent network request');
                });
                $mail = Mail::send("emails.contact-us", [ 'contact' => $request], function ($m) use ($test_email, $contact) {
                    $m->to($test_email)->subject('New agent network request');
                });
            }
            return response()->json(['data' => 'Email has been sent.'], 200);
        }

        // Api Upload Image
	    public static function apiUploadImage(Request $request) {
            
            // Check Validation	        
            $validation = Validator::make($request->all(), array('image' => 'required|image'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);  
            

            // Manage Image Upload    
            $image = $request->file('image');
            if(!empty($image)) {
                $image = CommonFunction::PICTURE_PATH.CommonFunction::uploadImage($image);
                $image = url($image);
            }
            else 
                $image = "";				
            
            //Send JSON response
            return response()->json(array('data' => $image),200);        	        
        }

        //Api test stripe
        public static function testStripe(Request $request) {

            \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
            $customer = \Stripe\Token::create(array(
                "card" => array(
                  "number" => "4242424242424242",
                  "exp_month" => 6,
                  "exp_year" => 2019,
                  "cvc" => "314"
                )
              ));
            return $customer;
        }

        // Api Mail Test
	    public static function mailTest(Request $request) {

			// return view('emails.forgot-password');

			$email = 'meemansa.henceforth@gmail.com';
            // Send Email
            try {
                $mail = Mail::send("emails.backup", ['name' => 'Meemansa'], function ($m) use ($email) {
                    $m->to($email)->subject("Test Mail");
                });
            }
            catch(\Exception $e) {
                return $e->getMessage();
            }
                
            return 123;
			// dd($mail);
		}

        // Api Upload File
	    public static function apiUploadFile(Request $request) {
            
            // Check Validation	        
            $validation = Validator::make($request->all(), array('file' => 'required|file'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);  
            

            // Manage file Upload    
            $file = $request->file('file');
            if(!empty($file)) {
                $file = CommonFunction::uploadFile($file);
                $file = json_decode(json_encode($file), FALSE);

                
                $file_path = CommonFunction::FILE_PATH.$file->filename;
                $file_path = url($file_path);
                $original_file = $file->original_filename;
            }
            else {
                $file_path = "";
                $original_file = "";				
            }
            
            $response[] = [
                'file' => $file_path,
                'original_file' => $original_file
            ];

            //Send JSON response
            return response()->json(array('data' => $response),200);        	        
        }

        
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Version 2
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/

        //Api Follow up email
        public static function apiFollowUpEmail() {
            $current_time = Carbon::now();

            $properties = Property::where('follow_up', 0)->selectRaw("properties.*, (Select email from users where users.id = properties.user_id) as user_email, DATEDIFF('$current_time', properties.`created_at`) as days")->where('is_subject_prop', 1)->get();
            $prop_ids =[];

            foreach($properties as $key => $value) {
                if($value->how_soon == 4) {
                    if($value->days >= 420) {
                        $prop_ids[] = $value->id;
                        //send follow up email
                        $mail = Mail::send("emails.follow-up", ['data' => $value], function ($m) use ($value) {
                            $m->to($value->user_email)->subject("Follow Up - HomeEase");
                        });
                    }
                }
                elseif($value->how_soon == 3) {
                    if($value->days >= 240) {
                        $prop_ids[] = $value->id;
                        //send follow up email
                        $mail = Mail::send("emails.follow-up", ['data' => $value], function ($m) use ($value) {
                            $m->to($value->user_email)->subject("Follow Up - HomeEase");
                        });
                    }
                }
                elseif($value->how_soon == 2) {
                    if($value->days >= 150) {
                        $prop_ids[] = $value->id;
                        //send follow up email
                        $mail = Mail::send("emails.follow-up", ['data' => $value], function ($m) use ($value) {
                            $m->to($value->user_email)->subject("Follow Up - HomeEase");
                        });
                    }
                }
            }
            Property::whereIn('id', $prop_ids)->update(['follow_up' => 1]);

            return 'done';
        }

        //Api Agent Payment
        public static function apiAgentPayment() {

            // $users = Payment::where('payment_status', 1)->whereIn('payment_type', [4,6])->groupBy('agent_id')->pluck('agent_id');

            // $start = new Carbon('first day of last month');
            // $end = new Carbon('last day of last month');
            // $start = $start->toDateString();
            // $end = $end->toDateString();

            // foreach($users as $key => $value) {
            //     $data = User::join('user_stripes', 'users.id', '=', 'user_stripes.user_id')
            //             ->select('users.*', 'user_stripes.customer_id', 'user_stripes.card_id')
            //             ->where('users.id', $value)
            //             ->first();

            //     $payment_data = Payment::orderBy('payment_type', 'desc')->where('agent_id', $value)->where('payment_status', 1)->whereIn('payment_type', [4,6])->get();
            //     $total_amount = Payment::where('agent_id', $value)->where('payment_status', 1)->whereIn('payment_type', [4,6])->sum('amount');

            //     $amount = $total_amount*100;

            //     //payment using credits or stripe
            //     // if($data->credits != NULL) {
            //     //     $payment = (($data->credits * 100) - $amount);
            //     //     if($payment >= 0) {
            //     //         $credits = $payment/100;
            //     //         User::where('id', $value)->update(['credits' => $credits]);

            //     //     }
            //     //     else {
            //     //         $amount = ($amount - ($data->credits*100));

            //     //                 //create charge
            //     //                 try {
            //     //                     \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
            //     //                     $charge = \Stripe\Charge::create(array(
            //     //                         "amount" => $amount,
            //     //                         "currency" => "usd",
            //     //                         "customer" => $data->customer_id,
            //     //                         "source" => $data->card_id,
            //     //                     ));
            //     //                 }
            //     //                 catch (\Exception $e) {                         
            //     //                     return $e->getMessage();   
            //     //                 }

            //     //         User::where('id', $value)->update(['credits' => 0]);
            //     //     }
    
            //     // }
            //     // else {
            //         //create charge
            //         try {
            //             \Stripe\Stripe::setApiKey(env("STRIPE_SECRET"));
            //             $charge = \Stripe\Charge::create(array(
            //                 "amount" => $amount,
            //                 "currency" => "usd",
            //                 "customer" => $data->customer_id,
            //                 "source" => $data->card_id,
            //             ));
            //         }
            //         catch (\Exception $e) {                         
            //             return $e->getMessage();   
            //         }
            //     //} 

            //     // Send Email
            //     $mail = Mail::send("emails.agent-payment", ['data' => $data, 'payment_data' => $payment_data, 'total_amount' => $total_amount, 'start' => $start, 'end' => $end], function ($m) use ($data) {
            //         $m->to($data->email)->subject("Payment Invoice - HomeEase");
            //     });
                     
            // }

            // Payment::where('payment_status', 1)->whereIn('payment_type', [4,6])->update(['payment_status' => 2]);
            
             return 'done';
        }


        // Api Contact Us
        public static function apiSendEmail(Request $request) {

            // Check Validation	        
            $validation = Validator::make($request->all(), array('to' => 'required|email', 'from' => 'required|email', 'subject' => 'required', 'body' => 'required'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);   
        
            $to = $request->to;
            $from = $request->from;
            $subject = $request->subject;
            $body = $request->body;

                    
            // Send Email
            $mail = Mail::send("emails.send-email", [ 'to' => $to, 'body' => $body, 'from' => $from], function ($m) use ($to, $subject) {
                $m->to($to)->subject($subject);
            });
        
            return response()->json(['data' => 'Email has been sent.'], 200);
        }


        // Api Connect To Pro
        public static function apiConnectToPro(Request $request) {

            // Check Validation	        
            $validation = Validator::make($request->all(), array('selling_status' => 'required', 'property_type' => 'required', 'email' => 'required|email', 'how_soon' => 'required', 'question' => 'required', 'pref_communication' => 'required'));
            if($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);   
            
            $dbEmail = Email::where('id',1)->first();
            $test_email = $dbEmail->test_email;
            $client_email = $dbEmail->client_email;
                    
            // Send Email
            if($test_email != ''){
                $mail = Mail::send("emails.new-agent-lead", [ 'request' => $request ], function ($m) use ($test_email) {
                    $m->to($test_email)->subject("New agent referral lead");
                });
            }
            
            $mail = Mail::send("emails.new-agent-lead", [ 'request' => $request ], function ($m) use ($client_email) {
                $m->to($client_email)->subject("New agent referral lead");
            });
        
            return response()->json(['data' => "We are excited to help you succeed in real estate! We will be in touch shortly, so keep an eye out for HomeEase on your phone and email. Thank you!"], 200);
        }

    /*=============Get in touch form after start pricing==========*/

	public function getIntouch(Request $request)
	{
		
	}

        // //Api image delete
		// public static function apiImageDelete() {
            
        //     $s3 = AWS::createClient('s3');
        //     $s3_files = array();
        //     $s3_images = array();
        //     $db_files = array();
      
        //     $iterator = $s3->getIterator('ListObjects', array(
        //         'Bucket' => 'homeease-test'
        //     ));

        //     foreach ($iterator as $object) {
        //         if (strpos($object['Key'], 'chat-files') !== false) {                
        //             $file_name = explode('/', $object['Key']);                                
        //             $file_name_end = end($file_name);
        //             array_push($s3_files, $file_name_end);
        //         }
        //         // if (strpos($object['Key'], 'uploads') !== false) {                
        //         //     $img_name = explode('/', $object['Key']);                                
        //         //     $img_name_end = end($img_name);
        //         //     array_push($s3_images, $img_name_end);
        //         // }
        //     }

        //     $files = Chat::where('file', '<>', NULL)->pluck('file');
         
        //     foreach ($files as $value) {
        //         foreach($value as $key1 => $object) {
        //             if (strpos($object->file, 'chat-files') !== false) {                
        //                 $file_name = explode('/', $object->file);                                
        //                 $file_name_end = end($file_name);
        //                 array_push($db_files, $file_name_end);
        //             }
        //         }
        //     }

        //     $extra_files = array_diff($s3_files,$db_files);

        //     $response[] = [
        //         // 's3_files' => $s3_files,
        //         // 's3_images' => $s3_images,
        //         // 'db_files' => $db_files,
        //         'extra_files' => $extra_files
        //     ];
        //     return $response;
		// }
}
