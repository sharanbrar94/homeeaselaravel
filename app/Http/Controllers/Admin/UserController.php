<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DB;
use App\Models\UserDevice;
use App\Models\Property;
use App\Models\Payment;
use App\Models\Upload;
use App\Models\State;
use App\Models\City;
use App\Models\County;
use Carbon\Carbon;

class UserController extends Controller
{
    //Admin User Listing

    public static function userListing(Request $request) {
        $keyword = "";

        $query = User::where('user_type', 1);

        if($request->keyword){
            $keyword = $request->keyword;
            $query->where(function ($q) use($keyword) {
                $q->where('first_name','like','%'.$keyword.'%')
                ->orWhere('email','like','%'.$keyword.'%')
                ->orWhere('agent_email','like','%'.$keyword.'%');
            });
        }

        $users = $query->orderBy('id','desc')
                            ->paginate(12);
        
        return $users;
    }

    //Admin Agent Listing

    public static function agentListing(Request $request) {
        $keyword = "";
        $licence_type = "";             
        // 1- Broker, 2- Agent
        $query = User::where('user_type', 2);
        if($request->licence_type) {
            $licence_type = $request->licence_type;
            $query = User::where('user_type', 2)->where('licence_type', $licence_type);
        }
        if($request->keyword){
            $keyword = $request->keyword;
            $query = User::where('user_type', 2)->where(function ($q) use($keyword) {
                        $q->where('first_name','like','%'.$keyword.'%')
                        ->orWhere('email','like','%'.$keyword.'%')
                        ->orWhere('mls_id','like','%'.$keyword.'%');
                    });
            
        }
        $users = $query->orderBy('id','desc')->paginate(12);
        return $users;
    }

    //Admin User Detail

    public static function userDetail(Request $request) {

        $validation = Validator::make($request->all(),[
            'user_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $user_detail = User::where('id', $request->user_id)->first();

        if($user_detail->user_type ==1) {
            $user_detail->active_properties = Property::where('user_id', $request->user_id)->where('status', '<>', 3)->where('deleted_at', NULL)->count();
            $user_detail->completed_properties = Property::where('user_id', $request->user_id)->where('status', 3)->where('deleted_at', NULL)->count();
            $user_detail->property_list = Property::where('user_id', $request->user_id)->where('is_subject_prop', 1)->orderBy('id','DESC')->get();
        }
        else {
            $user_detail->active_properties = Property::where('agent_id', $request->user_id)->where([
                ['status', '<>', 3],
                ['status', '<>', 0],
                ['status', '<>', 9],
                ['deleted_at', NULL],
            ])->count();
            $user_detail->completed_properties = Property::where('agent_id', $request->user_id)->where('status', 3)->where('deleted_at', NULL)->count();
            $user_detail->connected_users = Payment::where([
                ['agent_id', $request->user_id],
                ['payment_type', 4]
                ])->count();
            $user_detail->property_list = Property::where('agent_id', $request->user_id)->where('is_subject_prop', 1)->orderBy('id','DESC')->get();
            $user_detail->files = Upload::where('user_id', $request->user_id)->get();
            foreach($user_detail->files as $key => $value) {
                $value->city_name = City::where('id', $value->city_id)->value('name');
                $value->county_name = County::where('id', $value->county_id)->value('name');
                $value->state_name = State::where('id', $value->state_id)->value('name');
            }
        }
        return $user_detail;
    }

    // Admin Block User

    public function adminBlockUser(Request $request) {
        $validation = Validator::make($request->all(),[
            'user_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);


        $current_time = Carbon::now();
        $block = User::where('id', $request->user_id)
                        ->update(['blocked_at' => $current_time]);

        UserDevice::where('user_id', $request->user_id)->delete();
        return response()->json(['data' => "User Blocked"], 200);
    }

    // Admin Unblock User

    public function adminUnblockUser(Request $request) {
        $validation = Validator::make($request->all(),[
            'user_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $unblock = User::where('id', $request->user_id)
                        ->update(['blocked_at' => NULL]);

        return response()->json(['data' => "User Unblocked"], 200);
    }


    /*---------------------------------
    * Version 3
    ----------------------------------*/

        // Admin Block User
        public function addUserCredits(Request $request) {

            $validation = Validator::make($request->all(),[
                'user_id' => 'required',
                'credits' => 'required'
            ]);

            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);


            $user = User::where('id', $request->user_id)->first();
            $credits = $request->credits + $user->credits;
            $current_time = Carbon::now();
            $block = User::where('id', $request->user_id)
                            ->update(['credits' => $credits]);
            return response()->json(['data' => "Credits Added"], 200);
        }
}
