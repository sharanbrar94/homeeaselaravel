<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\City;
use App\Models\ExtraCity;
use App\Models\ExtraProperty;
use App\Models\County;
use App\Models\State;
use App\Models\User;
use App\Models\Upload;
use Carbon\Carbon;

class CityController extends Controller
{
    //Admin cities listing
    public static function citiesListing(Request $request) {
        $cities = City::get();
    
        return $cities;
    }
    public static function apiAllCounties() {
        
        $states = County::where("deleted_at", NULL)->get();
    
        //send json response 
        return response()->json($states,200);
    }

    //Admin counties listing
    public static function countiesListing(Request $request) {

        $validation = Validator::make($request->all(),[
            'state_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            
        $counties = County::where('state_id', $request->state_id)->get();
        foreach($counties as $key => $value) {
            $value->cities = City::select('id', 'name')->where('county_id', $value->id)->get();
        }
        return $counties;
    }

    //Admin city list
    public static function cityList(Request $request) {
        $validation = Validator::make($request->all(),[
            'county_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $cities = City::where('county_id', $request->county_id)->get();

        return $cities;
    }

    //Admin add county
    public static function addCounty(Request $request) {

        $validation = Validator::make($request->all(),[
            'name' => 'required',
            'state_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $exist = County::where('name', $request->name)->where('state_id', $request->state_id)->first();
        if(!empty($exist)) {
            return response()->json(['error' => 'bad_request', 'error_description' => 'The name has already been taken', 'error_code' => 1], 400);
        }

        $county = new County;
        $county->state_id = $request->state_id;
        $county->name = $request->name;
        $county->save();

        return $county;
    }

    //Admin add city
    public static function addCity(Request $request) {
        $validation = Validator::make($request->all(),[
            'county_name' => 'required',
            'state_name' => 'required',
            'name' => 'required',
            'above_sqft' => 'required',
            'below_sqft' => 'required',
            'year_build' => 'required',
            'garage1' => 'required',
            'garage2' => 'required',
            'heat' => 'required',
            'ac' => 'required',
            'fire_place' => 'required',
            'porch' => 'required',
            'bedroom_above' => 'required',
            'bedroom_below' => 'required',
            'bathroom_above' => 'required',
            'bathroom_below'  => 'required',
            'quality' => 'required',
            'condition_value' => 'required'
        ]);

        if(isset($request->url)) {
            $url = $request->url;
        }
        else {
            $url = "https://homeease.pro/";
        }

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $state = State::where('name', $request->state_name)->first();
        if(empty($state)) {
            $state = new State;
            $state->name = $request->state_name;
            $state->save();
        }

        $county = County::where('name', $request->county_name)->where('state_id', $state->id)->first();
        if(empty($county)) {
            $county = new County;
            $county->state_id = $state->id;
            $county->name = $request->county_name;
            $county->save();
        }


        $exist = City::where('name', $request->name)->where('county_id', $county->id)->first();
        if(!empty($exist)) {
            return response()->json(['error' => 'bad_request', 'error_description' => 'The name has already been taken', 'error_code' => 1], 400);
        }

        $city = new City;
        $city->county_id = $county->id;
        $city->name = $request->name;
        $city->above_sqft = $request->above_sqft;
        $city->below_sqft = $request->below_sqft;
        $city->year_build = $request->year_build;
        $city->garage1 = $request->garage1;
        $city->garage2 = $request->garage2;
        $city->heat = $request->heat;
        $city->ac = $request->ac;
        $city->fire_place = $request->fire_place;
        $city->porch = $request->porch;
        $city->bedroom_above = $request->bedroom_above;
        $city->bedroom_below = $request->bedroom_below;
        $city->bathroom_above = $request->bathroom_above;
        $city->bathroom_below = $request->bathroom_below;
        $city->quality = $request->quality;
        $city->condition_value = $request->condition_value;
        $city->save();

        $users = ExtraProperty::where('city', $request->name)->where('county', $request->county_name)->where('state', $request->state_name)->where('added',0)->groupBy('user_id')->pluck('user_id');

        foreach($users as $key => $value) {
            $user_details = User::where('id', $value)->first();

            $type = "City";

            // Send Email
            $mail = Mail::send("emails.approved", ['user_details' => $user_details, 'city_name' => $city->name, 'type' => $type, 'url' => $url], function ($m) use ($user_details) {
                $m->to($user_details->email)->subject("Approved - HomeEase");
            });

        }

        $extra = ExtraProperty::where('city', $request->name)->where('county', $request->county_name)->where('state', $request->state_name)->get();
        if(count($extra) > 0)
        {
            foreach($extra as $key => $value) {
            
                $prop[] = array(
                    'user_id' => $value->user_id,
                    'agent_id' => $value->agent_id,
                    'is_subject_prop' => 1,
                    'type' => $value->type,
                    'agency_type' => $value->agency_type,
                    'tax_key' => $value->tax_key,
                    'address_1' => $value->address_1,
                    'address_2' => $value->address_2,
                    'city' => $city->id,
                    'county' => $county->id,
                    'state' => $state->id,
                    'zip_code' => $value->zip_code,
                    'country' => $value->country,
                    'lat' => $value->lat,
                    'lng' => $value->lng,
                    'how_soon' => $value->how_soon,
                    'selling_status' => $value->selling_status,
                    'progress_status' => 0,
                    'status' => 0
                );
            }
            DB::table('properties')->insert($prop);
        }
        

        ExtraProperty::where('city', $request->name)->where('county', $request->county_name)->where('state', $request->state_name)->update([
            'added' => 1
        ]);
        return $city;
    }

    //Admin Multipliers list
    public static function multiplierList(Request $request) {
        $validation = Validator::make($request->all(),[
            'city_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
        
        $multiplier = City::where('id', $request->city_id)->first();
        $multiplier->files = Upload::selectRaw("*, (select name from states where id = state_id) as state_name, (select name from counties where id = county_id) as county_name, (select name from cities where id = city_id) as city_name, (select CONCAT(first_name, ' ' ,last_name) from users where id = user_id) as user_name")->where('city_id', $request->city_id)->get();
        
        return $multiplier;

    }

    // Admin disable County
    public function disableCounty(Request $request) {

        $validation = Validator::make($request->all(),[
            'county_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $current_time = Carbon::now();
        $disable = County::where('id', $request->county_id)
                        ->update(['deleted_at' => $current_time]);
        return response()->json(['data' => "County Disabled"], 200);
    }

    // Admin enable County
    public function enableCounty(Request $request) {

        $validation = Validator::make($request->all(),[
            'county_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
            
        $enable = County::where('id', $request->county_id)
                        ->update(['deleted_at' => NULL]);
        return response()->json(['data' => "County Enabled"], 200);
    }

    
    // *******************************
    // Version 2
    // *******************************

        //agent city
        public function adminAgentCity(Request $request) {
            // $query = ExtraCity::selectRaw("*, count(`id`) as count ")->where('is_city', 1);
            
            // if($request->keyword) {
            //     $query->where('name','like','%'.$request->keyword.'%');
            // }
            // $cities = $query->groupBy('name')->orderBy("count", "desc")->get();

            // foreach($cities as $key => $value) {
            //     $user_ids = ExtraCity::where('name', $value->name)->where('is_city', 1)->pluck('user_id');

            //     $value->users = User::whereIn('id', $user_ids)->select("id", "first_name", "email")->get();
            // }

            $query = ExtraCity::selectRaw("state, county, count(`id`) as count ");
            
            if($request->keyword) {
                $query->where('state','like','%'.$request->keyword.'%')->orWhere('county','like','%'.$request->keyword.'%')->orWhere('city','like','%'.$request->keyword.'%');
            }
            $cities = $query->groupBy('state', 'county')->orderBy("count", "desc")->get();

            foreach($cities as $key => $value) {
                // $value->city_names = ExtraCity::where('county', $value->county)->where('state', $value->state)->selectRaw("city, count(`id`) as count")->groupBy('city')->get();
                // $user_ids = ExtraCity::where('county', $value->county)->where('state', $value->state)->pluck('user_id');

                // $value->users = User::whereIn('id', $user_ids)->select("id", "first_name", "email")->get();
                $value->users = ExtraCity::where('county', $value->county)->where('state', $value->state)->selectRaw('user_id as id, (Select email from users where id = user_id) as email, (Select first_name from users where id = user_id) as first_name, (Select last_name from users where id = user_id) as last_name, (Select image from users where id = user_id) as image')->get();
            }


            return $cities;
        }

        //agent county
        public function adminAgentCounty(Request $request) {
            $query = ExtraCity::selectRaw("*, count(`id`) as count ")->where('is_city', 2);
            
            if($request->keyword) {
                $query->where('name','like','%'.$request->keyword.'%');
            }
            $cities = $query->groupBy('name')->get();

            foreach($cities as $key => $value) {
                $user_ids = ExtraCity::where('name', $value->name)->where('is_city', 2)->pluck('user_id');

                $value->users = User::whereIn('id', $user_ids)->select("id", "first_name", "email")->get();
            }
    
            return $cities;
        }

        //approve city county
        public function adminApproveCityCounty(Request $request) {
            $validation = Validator::make($request->all(),[
                'county_name' => 'required',
                'state_name' => 'required'
                // 'name' => 'required'
            ]);


            if(isset($request->url)) {
                $url = $request->url;
            }
            else {
                $url = "https://homeease.pro/";
            }
    
            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            // $detail = ExtraCity::where('id', $request->city_id)->first();
            $users = ExtraCity::where('county', $request->county_name)->where('state', $request->state_name)->pluck('user_id');

            $state_id = State::where('name', $request->state_name)->first();
            if(empty($state_id)) {
                $state_id = new State;
                $state_id->name = $request->state_name;
                $state_id->save();
            }

            $county_id = County::where('name', $request->county_name)->where('state_id', $state_id->id)->first();
            if(empty($county_id)) {
                $county_id = new County;
                $county_id->state_id = $state_id->id;
                $county_id->name = $request->county_name;
                $county_id->save();
            }


            // if($detail->is_city == 1) {
            // $state_id = State::where('name', $request->state_name)->first();
            // $county_id = County::where('name', $request->county_name)->where('state_id', $state_id->id)->first();
            // $city_id = City::where('name', $request->name)->where('county_id', $county_id->id)->first();

            foreach($users as $value) {
                // $preferred_cities = User::where('id', $value)->value('preferred_cities');
                $preferred_counties = User::where('id', $value)->value('preferred_counties');
                $preferred_states = User::where('id', $value)->value('preferred_states');
                
                // $preferred_cities[] = $city_id->id;
                $preferred_counties[] = $county_id->id;
                $preferred_states[] = $state_id->id;

                // $preferred_cities = array_unique($preferred_cities);
                $preferred_counties = array_unique($preferred_counties);
                $preferred_states = array_unique($preferred_states);

                User::where('id', $value)->update([
                    // 'preferred_cities' => json_encode($preferred_cities),
                    'preferred_counties' => json_encode($preferred_counties),
                    'preferred_states' => json_encode($preferred_states)
                    ]);

                $user_details = User::where('id', $value)->first();

            // if($detail->is_city == 1) {
                $type = "County";

                // Send Email
                $mail = Mail::send("emails.approved", ['user_details' => $user_details, 'city_name' => $request->county_name, 'type' => $type, 'url' => $url], function ($m) use ($user_details) {
                    $m->to($user_details->email)->subject("Approved - HomeEase");
                });
            }
            // }
            // elseif($detail->is_city == 2) {
            //     $city_id = County::where('name', $detail->name)->first();
                
            //     foreach($users as $value) {
            //         $preferred = User::where('id', $value)->value('preferred_counties');
                    
            //         $preferred[] = $city_id->id;
            //         $preferred = json_encode($preferred);
            //         User::where('id', $value)->update(['preferred_counties' => $preferred]);
            //     }
            // }

            // foreach($users as $key => $value) {
            //     $user_details = User::where('id', $value)->first();

            //     // if($detail->is_city == 1) {
            //         $type = "City";

            //         // Send Email
            //         $mail = Mail::send("emails.approved", ['user_details' => $user_details, 'detail' => $detail, 'type' => $type, 'url' => $url], function ($m) use ($user_details) {
            //             $m->to($user_details->email)->subject("Approved - HomeEase");
            //         });
            //     // }
            //     // elseif($detail->is_city == 2) {
            //     //     $type = "County";
            //     // }

            // }

            ExtraCity::where('county', $request->county_name)->where('state', $request->state_name)->delete();
            return response()->json(['data' => "done"], 200);
        }

    // *******************************
    // Version 3
    // *******************************


        //Admin add state
        public static function addState(Request $request) {

            $validation = Validator::make($request->all(),[
                'name' => 'required'
            ]);

            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $exist = State::where('name', $request->name)->first();
            if(!empty($exist)) {
                return response()->json(['error' => 'bad_request', 'error_description' => 'The name has already been taken', 'error_code' => 1], 400);
            }

            $state = new State;
            $state->name = $request->name;
            $state->save();

            return $state;
        }

        // //Admin counties listing
        // public static function adminCounties(Request $request) {
        
        //     $states = State::get();
        //     foreach($states as $key => $value) {
        //         $value->counties = County::select('id', 'name')->where('state_id', $value->id)->get();
        //     }
        //     return $states;
        // }

        //Admin states listing
        public static function adminAllStates(Request $request) {
                
            $states = State::get();
            if(isset($request->keyword)) {
                $county = County::where('name','like','%'.$request->keyword.'%')->pluck('state_id');

                $states = State::whereIn('id', $county)->orWhere('name','like','%'.$request->keyword.'%')->get();
            }

            foreach($states as $key => $value) {
                $value->counties = County::select('id', 'name')->where('state_id', $value->id)->get();
                if(isset($request->keyword)) {
                    $value->counties = County::select('id', 'name')->where('state_id', $value->id)->get();
                    // $states = State::get();
                }
            }
            return $states;
        }

        // Admin disable state
        public function disableState(Request $request) {

            $validation = Validator::make($request->all(),[
                'state_id' => 'required'
            ]);

            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

            $current_time = Carbon::now();
            $disable = State::where('id', $request->state_id)
                            ->update(['deleted_at' => $current_time]);
            return response()->json(['data' => "State Disabled"], 200);
        }

        // Admin enable state
        public function enableState(Request $request) {

            $validation = Validator::make($request->all(),[
                'state_id' => 'required'
            ]);

            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
            $enable = State::where('id', $request->state_id)
                            ->update(['deleted_at' => NULL]);
            return response()->json(['data' => "State Enabled"], 200);
        }

        // Admin editMultipliers
        public function editMultipliers(Request $request) {

            $validation = Validator::make($request->all(),[
                'city_id' => 'required'
            ]);

            if ($validation->fails())
                return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);
                
            $enable = City::where('id', $request->city_id)
                            ->update([
                                'above_sqft' =>  $request->above_sqft,
                                'below_sqft' =>  $request->below_sqft,
                                'year_build' =>  $request->year_build,
                                'garage1' =>  $request->garage1,
                                'garage2' =>  $request->garage2,
                                'heat' =>  $request->heat,
                                'ac' =>  $request->ac,
                                'fire_place' =>  $request->fire_place,
                                'porch' =>  $request->porch,
                                'bedroom_above' =>  $request->bedroom_above,
                                'bedroom_below' =>  $request->bedroom_below,
                                'bathroom_above' =>  $request->bathroom_above,
                                'bathroom_below' =>  $request->bathroom_below,
                                'quality' =>  $request->quality,
                                'condition_value' =>  $request->condition_value,
                            ]);
            return response()->json(['data' => "Updated Successfully"], 200);
        }
}
