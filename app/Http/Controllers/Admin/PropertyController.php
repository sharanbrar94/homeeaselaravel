<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Property;
use App\Models\CompProperty;
use App\Models\Multiplier;
use App\Models\User;
use App\Models\Admin;
use App\Models\City;
use App\Models\County;
use App\Models\Apartment;
use App\Models\ExtraProperty;
use App\Models\Payment;
use DB;

use Carbon\Carbon;

class PropertyController extends Controller
{
    //Admin Property listing
    public static function propertyListing(Request $request) {
        $keyword = "";
        DB::enableQueryLog();
        $query = Property::orderBy('id','desc')
                ->where('type', 1)
                ->where('deleted_at', NULL)
                ->where('is_subject_prop', 1);
                if($request->status) {
                    if($request->status == 11) {
                        $query->where('status', 0);
                    }
                    elseif($request->status != 10) {
                        $query->where('status', $request->status);
                    }
                }
                if($request->keyword){
                    $keyword = $request->keyword;
                    $query->where(function ($q) use($keyword) {
                        $q->where(DB::raw("CONCAT_WS(' ',address_1,address_2)"), 'like', '%'.$keyword.'%')
                        ->orWhereRaw("(select name from cities where id = city) like '%$keyword%'")
                        ->orWhereRaw("(select name from counties where id = county) like '%$keyword%'")
                        ->orWhereRaw("(select name from states where id = state) like '%$keyword%'");
                    });
                }
        $properties = $query->orderBy('id','desc')->paginate(12);
        foreach($properties as $key => $value) {
            $value->city_name = City::where('id', $value->city)->value('name');
            $value->county_name = County::where('id', $value->county)->value('name');
        }
        return $properties;
    }

    //Admin Duplex listing
    public static function duplexListing(Request $request) {
        $keyword = "";


        $query = Property::orderBy('id','desc')
                        ->where('type', 2)
                        ->where('deleted_at', NULL)
                        ->where('is_subject_prop', 1);

        if($request->status) {
            if($request->status == 11) {
                $query->where('status', 0);
            }
            elseif($request->status != 10) {
                $query->where('status', $request->status);
            }
        }
        if($request->keyword){
            $keyword = $request->keyword;
            $query->where(function ($q) use($keyword) {
                $q->where(DB::raw("CONCAT_WS(' ',address_1,address_2)"), 'like', '%'.$keyword.'%')
                ->orWhereRaw("(select name from cities where id = city) like '%$keyword%'")
                ->orWhereRaw("(select name from counties where id = county) like '%$keyword%'")
                ->orWhereRaw("(select name from states where id = state) like '%$keyword%'");
            });
        }

        $properties = $query->orderBy('id','desc')
                            ->paginate(12);
        
        foreach($properties as $key => $value) {
            $value->city_name = City::where('id', $value->city)->value('name');
            $value->county_name = County::where('id', $value->county)->value('name');
        }

        return $properties;
    }

    //Admin condo listing
    public static function condoListing(Request $request) {
        $keyword = "";

        $query = Property::orderBy('id','desc')
                        ->where('type', 3)
                        ->where('deleted_at', NULL)
                        ->where('is_subject_prop', 1);

        
        if($request->status) {
            if($request->status == 11) {
                $query->where('status', 0);
            }
            elseif($request->status != 10) {
                $query->where('status', $request->status);
            }
        }
        if($request->keyword){
            $keyword = $request->keyword;
            $query->where(function ($q) use($keyword) {
                $q->where(DB::raw("CONCAT_WS(' ',address_1,address_2)"), 'like', '%'.$keyword.'%')
                ->orWhereRaw("(select name from cities where id = city) like '%$keyword%'")
                ->orWhereRaw("(select name from counties where id = county) like '%$keyword%'")
                ->orWhereRaw("(select name from states where id = state) like '%$keyword%'");
            });
        }

        $properties = $query->orderBy('id','desc')
                            ->paginate(12);

        foreach($properties as $key => $value) {
            $value->city_name = City::where('id', $value->city)->value('name');
            $value->county_name = County::where('id', $value->county)->value('name');
        }
        
        return $properties;
    }

    //Admin Property Detail
    public static function propertyDetail(Request $request) {

        $validation = Validator::make($request->all(),[
            'property_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $prop_detail = Property::where('id', $request->property_id)->first();
        $compproperty = CompProperty::where('subject_prop_id', $request->property_id)->where('type',2)->where('is_finished',1)->first();
        if($compproperty != ''){
            $prop_detail->agent_cal = 1;
        }
        else{
            $prop_detail->agent_cal = 0;
        }
        $prop_detail->user = User::select('first_name', 'last_name','email')->where('id', $prop_detail->user_id)->first();
        if($prop_detail->agent_id != NULL) {
            $prop_detail->agent = User::select('first_name', 'last_name','email')->where('id', $prop_detail->agent_id)->first();
        }
        else {
            $prop_detail->agent = NULL;
        }
        $prop_detail->city_name = City::where('id', $prop_detail->city)->value('name');
        $prop_detail->county_name = County::where('id', $prop_detail->county)->value('name');
        $prop_detail->reviews = CompProperty::where('subject_prop_id', $request->property_id)->count();
        $prop_detail->payment_detail = Payment::where('property_id', $request->property_id)->first();
       


        
        return $prop_detail;
    }

    //Admin apartment listing
    public static function apartmentListing(Request $request) {
        $keyword = "";


        $query = Property::orderBy('id','desc')
                        ->where('type', 4)
                        ->where('deleted_at', NULL)
                        ->where('is_subject_prop', 1);

        // if($request->keyword){
        //     $keyword = $request->keyword;
        //     $query->where('address_1','like','%'.$keyword.'%');
        // }
        if($request->keyword){
            $keyword = $request->keyword;
            $query->where(function ($q) use($keyword) {
                $q->where(DB::raw("CONCAT_WS(' ',address_1,address_2)"), 'like', '%'.$keyword.'%')
                ->orWhereRaw("(select name from cities where id = city) like '%$keyword%'")
                ->orWhereRaw("(select name from counties where id = county) like '%$keyword%'")
                ->orWhereRaw("(select name from states where id = state) like '%$keyword%'");
            });
        }

        $properties = $query->orderBy('id','desc')
                            ->paginate(12);

        foreach($properties as $key => $value) {
            $value->city_name = City::where('id', $value->city)->value('name');
            $value->county_name = County::where('id', $value->county)->value('name');
        }
        
        return $properties;
    }

    //Admin apartment Detail
    public static function apartmentDetail(Request $request) {

        $validation = Validator::make($request->all(),[
            'property_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $apartment = Apartment::where('id', $request->property_id)->first();
        $property = Property::where('id',$request->property_id)->first();

        $payment = Payment::orderBy('created_at', 'desc')->where('user_id', $property->user_id)->whereIn('payment_type', [5,8])->first(); //5-yearly, 8-monthly
        $loan_amount = 0;
        $monthly_payment = 0;
        $invested = 0;
        $yearly_reduction = 0;
        $yearly_cash_flow = 0;
        $yearly_profit = 0;
        $return = 0;
        
        if(empty($payment)) {
            $apartment_payment = 0;
        }
        else {
            $diff = Carbon::parse(Carbon::now())->diffInDays($payment->created_at);
            if($payment->payment_type == 8) {
                if($diff > 30) {
                    $apartment_payment = 0;
                }
                else {
                    $apartment_payment = 1;
                }
            }
            else {
                if($diff > 365) {
                    $apartment_payment = 0;
                }
                else {
                    $apartment_payment = 1;
                }
            }

            // dd($diff);
        }
        if($apartment->purchase_price!= null && $apartment->down_payment != null && $apartment->interest != null && $apartment->ammortized != null) {
            // financing and ROI calculations
            $loan_amount = round(($apartment->purchase_price)*(1-($apartment->down_payment/100)),2);
            $monthly_payment = round(((($apartment->interest/1200)*$loan_amount)/(1 - pow((1 + ($apartment->interest/1200)),-($apartment->ammortized*12)))),2);
            if($apartment->all_cash_offer == 0) {
                $invested1 = round(($apartment->purchase_price * $apartment->down_payment),2);
                $invested = $invested1/100;
            }
            else {
                $invested = $apartment->purchase_price;
            }
            $yearly_reduction = round((($monthly_payment*12) - ($loan_amount*($apartment->interest/100))),2);
            $yearly_cash_flow = round($apartment->operating_income - ($monthly_payment*12),2);
            $yearly_profit = round(($yearly_reduction + $yearly_cash_flow),2);
            $return = round(($yearly_profit/$invested)*100,2);
        }

        // =(Monthly Pymt x 12) - (Loan Amount x Interest Rate)

        $apartment->selling_status = $property->selling_status;
        $apartment->apartment_payment = $apartment_payment;
        $apartment->address_1 = $property->address_1;
        $apartment->address_2 = $property->address_2;
        $apartment->city = City::where('id', $property->city)->value('name');
        $apartment->county = County::where('id', $property->county)->value('name');
        $apartment->zip_code = $property->zip_code;

        // financing and ROI
        $apartment->loan_amount = $loan_amount;
        $apartment->monthly_payment = $monthly_payment;
        $apartment->invested = $invested;
        $apartment->yearly_reduction = $yearly_reduction;
        $apartment->yearly_cash_flow = $yearly_cash_flow;
        $apartment->yearly_profit = $yearly_profit;
        $apartment->return = $return;



        //send json response 
        return response()->json($apartment,200);
        // $prop_detail->user = User::select('first_name', 'last_name')->where('id', $prop_detail->user_id)->first();
        // $prop_detail->city_name = City::where('id', $prop_detail->city)->value('name');
        // $prop_detail->county_name = County::where('id', $prop_detail->county)->value('name');
        
        // return $prop_detail;
    }

    //Api Step1 Data
    public static function apiStep1Data(Request $request) {

        //Check Validation	        
        $validation = Validator::make($request->all(), CompProperty::$apiStep1Rules);
        if($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $admin_id = $request->admin_data->id;
        $user = Admin::where('id', $admin_id)->first();

        $subject_prop_id = $request->subject_prop_id;
        $type = $request->type;

        $user->s1 = Property::select('id', 'user_id', 'agent_id', 'second_agent', 'address_1', 'address_2', 'progress_status', 'shortcut', 'other', 'status', 'sqft_above', 'sqft_below', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'step1', 'step2', 'step3', 'step4')->where('id', $subject_prop_id)->first();

        if($type == 2) {
            $user->agent_details = User::where('id', $user->s1->agent_id)->first();
            $user_id = $user->s1->agent_id;
        }
        if($type == 1) {
            $user->agent_details = User::where('id', $user->s1->agent_id)->first();
            $user_id = $user->s1->user_id;
        }
        elseif($type == 3) {
            $user->agent_details = User::where('id', $user->s1->second_agent)->first();
            $user_id = $user->s1->second_agent;
        }

        $step1 = isset($request->step1) ? $request->step1 : $user->s1->step1;
            $progress_status = isset($request->progress_status) ? $request->progress_status : $user->s1->progress_status;

            $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
            if(empty($comp_properties)) {
                Property::addProperties($subject_prop_id, $type, $user_id);
                $comp_properties = CompProperty::where('type', $type)->where('subject_prop_id', $subject_prop_id)->first();
            }

            $user->c1 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop1)->first();
            $user->c2 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop2)->first();
            $user->c3 = Property::select('id', 'address_1', 'address_2', 'sqft_above', 'sqft_below', 'other', 'year_build', 'garage_space', 'heat_type', 'ac', 'fire_place', 'porch', 'sale_price')->where('id', $comp_properties->comp_prop3)->first();

            //Multipliers
            Multiplier::getMultipliers($request, $subject_prop_id, $type);
            $multiplier = Multiplier::where('type', $type)->where('property_id', $subject_prop_id)->first();
            $user->multipliers = $multiplier;

            //Adjustment sqft above
            $user->c1->sqft_above1 = ($user->s1->sqft_above - $user->c1->sqft_above)*$multiplier->above_sqft;
            $user->c2->sqft_above2 = ($user->s1->sqft_above - $user->c2->sqft_above)*$multiplier->above_sqft;
            $user->c3->sqft_above3 = ($user->s1->sqft_above - $user->c3->sqft_above)*$multiplier->above_sqft;

            //Adjustment sqft below
            $user->c1->sqft_below1 = ($user->s1->sqft_below - $user->c1->sqft_below)*$multiplier->below_sqft;
            $user->c2->sqft_below2 = ($user->s1->sqft_below - $user->c2->sqft_below)*$multiplier->below_sqft;
            $user->c3->sqft_below3 = ($user->s1->sqft_below - $user->c3->sqft_below)*$multiplier->below_sqft;
            
            //Adjustment year build
            if(abs($user->s1->year_build - $user->c1->year_build) >= 10){
                $user->c1->year_build1 = (($user->s1->year_build - $user->c1->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c1->year_build1 = 0;
            }
            if(abs($user->s1->year_build - $user->c2->year_build) >= 10){
                $user->c2->year_build2 = (($user->s1->year_build - $user->c2->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c2->year_build2 = 0;
            }
            if(abs($user->s1->year_build - $user->c3->year_build) >= 10){
                $user->c3->year_build3 = (($user->s1->year_build - $user->c3->year_build)/10)*$multiplier->year_build;
            }
            else {
                $user->c3->year_build3 = 0;
            }
       
            //Adjustment garage space
            // if($user->s1->garage_space > 0) {
            //     $user->c1->garage_space1 = ($user->s1->garage_space - $user->c1->garage_space)*$multiplier->garage2;
            //     $user->c2->garage_space2 = ($user->s1->garage_space - $user->c2->garage_space)*$multiplier->garage2;
            //     $user->c3->garage_space3 = ($user->s1->garage_space - $user->c3->garage_space)*$multiplier->garage2;
    
            // }
            // elseif($user->s1->garage_space == 0) {
            //     if(abs($user->s1->garage_space - $user->c1->garage_space) > 1) {
            //         $d1 = abs($user->s1->garage_space - $user->c1->garage_space) - 1;
            //         $user->c1->garage_space1 = -($multiplier->garage1 + $d1*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c1->garage_space1 = -($multiplier->garage1);
            //     }

            //     if(abs($user->s1->garage_space - $user->c2->garage_space) > 1) {
            //         $d2 = abs($user->s1->garage_space - $user->c2->garage_space) - 1;
            //         $user->c2->garage_space2 = -($multiplier->garage1 + $d2*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c2->garage_space2 = -($multiplier->garage1);
            //     }

            //     if(abs($user->s1->garage_space - $user->c3->garage_space) > 1) {
            //         $d3 = abs($user->s1->garage_space - $user->c3->garage_space) - 1;
            //         $user->c3->garage_space3 = -($multiplier->garage1 + $d3*$multiplier->garage2);
            //     }
            //     else {
            //         $user->c3->garage_space3 = -($multiplier->garage1);
            //     }
            // }

            if($user->s1->garage_space == 0) {
                $c1_garage_absolute = abs( $user->s1->garage_space - $user->c1->garage_space );
                if ($c1_garage_absolute > 1) {
                  $c1_garage_difference = $c1_garage_absolute - 1;
                  $user->c1->garage_space1 = -( $multiplier->garage1 + $c1_garage_difference * $multiplier->garage2 );
                } else {
                  $user->c1->garage_space1 = -$multiplier->garage1;
                }
          
                $c2_garage_absolute = abs( $user->s1->garage_space - $user->c2->garage_space);
                if ($c2_garage_absolute > 1) {
                  $c2_garage_difference = $c2_garage_absolute - 1;
                  $user->c2->garage_space2 = -( $multiplier->garage1 + $c2_garage_difference * $multiplier->garage2 );
                } else {
                    $user->c2->garage_space2 = -$multiplier->garage1;
                }
          
                $c3_garage_absolute = abs( $user->s1->garage_space - $user->c3->garage_space);
                if ($c3_garage_absolute > 1) {
                   $c3_garage_difference = $c3_garage_absolute - 1;
                  $user->c3->garage_space3 = -( $multiplier->garage1 + $c3_garage_difference * $multiplier->garage2 );
                } else {
                  $user->c3->garage_space3 = -$multiplier->garage1;
                }
              
              }
              else if($user->s1->garage_space > 0) {
                if($user->s1->garage_space == $user->c1->garage_space){
                  $user->c1->garage_space1 = 0;
                }
                else if($user->c1->garage_space == 0){
                  
                   $c1_garage_absolute = abs( $user->s1->garage_space - $user->c1->garage_space );
                  if ($c1_garage_absolute > 0) {
                     $c1_garage_difference = $c1_garage_absolute - 1;
                    $user->c1->garage_space1 = ( $multiplier->garage1 + $c1_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c1->garage_space1 = $multiplier->garage1;
                  }
                }
                else{
                   $c1_garage_absolute = ( $user->s1->garage_space - $user->c1->garage_space );
                  $user->c1->garage_space1 = $c1_garage_absolute * $multiplier->garage2;
                }
          
                if($user->s1->garage_space == $user->c2->garage_space){
                    $user->c2->garage_space2 = 0;
                }
                else if($user->c2->garage_space == 0){
                  $c2_garage_absolute = abs( $user->s1->garage_space - $user->c2->garage_space );
                  if ($c2_garage_absolute > 0) {
                    $c2_garage_difference = $c2_garage_absolute - 1;
                    $user->c2->garage_space2 = ( $multiplier->garage1 + $c2_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c2->garage_space2 = $multiplier->garage1;
                  }
                }
                else{
                  $c2_garage_absolute = ( $user->s1->garage_space - $user->c2->garage_space );
                  $user->c2->garage_space2 = ($c2_garage_absolute) * $multiplier->garage2;
                }
          
                if($user->s1->garage_space == $user->c3->garage_space){
                  $user->c3->garage_space3 = 0;
                }
                else if($user->c3->garage_space == 0){
                   $c3_garage_absolute = ( $user->s1->garage_space - $user->c3->garage_space );
                  if ($c3_garage_absolute > 0) {
                    $c3_garage_difference = $c3_garage_absolute - 1;
                    $user->c3->garage_space3 = ( $multiplier->garage1 + $c3_garage_difference * $multiplier->garage2 );
                  } else {
                    $user->c3->garage_space3 = $multiplier->garage1;
                  }
                }
                else{
                   $c3_garage_absolute = ( $user->s1->garage_space - $user->c3->garage_space );
                  $user->c3->garage_space3 = ($c3_garage_absolute) * $multiplier->garage2;
                }
              }
              else if ($user->s1->garage_space < 0) {
                $user->c1->garage_space3 = ($user->s1->garage_space - $user->c1->garage_space) * $multiplier->garage2;
                $user->c2->garage_space3 = ($user->s1->garage_space - $user->c2->garage_space) * $multiplier->garage2;
                $user->c3->garage_space3 = ($user->s1->garage_space - $user->c3->garage_space) * $multiplier->garage2;
              } 


            //Adjustment heat & ac
            $ac1 = ($user->s1->ac - $user->c1->ac)*$multiplier->ac;
            $ac2 = ($user->s1->ac - $user->c2->ac)*$multiplier->ac;
            $ac3 = ($user->s1->ac - $user->c3->ac)*$multiplier->ac;
            if($user->s1->heat_type == 0) {
                if($user->c1->heat_type == 0) {
                    $heat1 = 0;
                }
                else {
                    $heat1 = $multiplier->heat;
                }
                if($user->c2->heat_type == 0) {
                    $heat2 = 0;
                }
                else {
                    $heat2 = $multiplier->heat;
                }
                if($user->c3->heat_type == 0) {
                    $heat3 = 0;
                }
                else {
                    $heat3 = $multiplier->heat;
                }

            }
            else {
                if($user->c1->heat_type == 0) {
                    $heat1 = -($multiplier->heat);
                }
                else {
                    $heat1 = 0;
                }
                if($user->c2->heat_type == 0) {
                    $heat2 = -($multiplier->heat);
                }
                else {
                    $heat2 = 0;
                }
                if($user->c3->heat_type == 0) {
                    $heat3 = -($multiplier->heat);
                }
                else {
                    $heat3 = 0;
                }
            }
            $user->c1->heat_ac1 = $ac1 + $heat1;
            $user->c2->heat_ac2 = $ac2 + $heat2;
            $user->c3->heat_ac3 = $ac3 + $heat3;


            //Adjustment fire place
            $user->c1->fire_place1 = ($user->s1->fire_place - $user->c1->fire_place)*$multiplier->fire_place;
            $user->c2->fire_place2 = ($user->s1->fire_place - $user->c2->fire_place)*$multiplier->fire_place;
            $user->c3->fire_place3 = ($user->s1->fire_place - $user->c3->fire_place)*$multiplier->fire_place;
            
            //Adjustment porch
            $user->c1->porch1 = ($user->s1->porch - $user->c1->porch)*$multiplier->porch;
            $user->c2->porch2 = ($user->s1->porch - $user->c2->porch)*$multiplier->porch;
            $user->c3->porch3 = ($user->s1->porch - $user->c3->porch)*$multiplier->porch;

            //comparison property 1 details
            $detail1 = ($user->c1->sqft_above1 + $user->c1->sqft_below1 + $user->c1->year_build1 + $user->c1->garage_space1 + $user->c1->heat_ac1 + $user->c1->fire_place1 + $user->c1->porch1);

            //comparison property 2 details
            $detail2 = ($user->c2->sqft_above2 + $user->c2->sqft_below2 + $user->c2->year_build2 + $user->c2->garage_space2 + $user->c2->heat_ac2 + $user->c2->fire_place2 + $user->c2->porch2);

            //comparison property 3 details
            $detail3 = ($user->c3->sqft_above3 + $user->c3->sqft_below3 + $user->c3->year_build3 + $user->c3->garage_space3 + $user->c3->heat_ac3 + $user->c3->fire_place3 + $user->c3->porch3);
            // dd($detail1);

            $user->running_c1 = $detail1;
            $user->running_c2 = $detail2;
            $user->running_c3 = $detail3;
            $user->running_estimate =round((($user->c1->sale_price + $user->running_c1) + ($user->c2->sale_price + $user->running_c2) + ($user->c3->sale_price + $user->running_c3))/3, 2);


            Property::where('id', $comp_properties->comp_prop1)->update(['detail_result' => $detail1]);
            Property::where('id', $comp_properties->comp_prop2)->update(['detail_result' => $detail2]);
            Property::where('id', $comp_properties->comp_prop3)->update(['detail_result' => $detail3]);

            Property::where('id', $subject_prop_id)->update([
                'step1' => $step1, 
                'step1_status' => 1, 
                'progress_status' => $progress_status
            ]); 
            if($user->s1->step1 == 1){
                $user->s1->step1_status = 1;
            }
            else{
                $user->s1->step1_status = 0;
            }
            
            //return json response
            return response()->json($user,200);     
                
    }

    // Admin Block Property
    public function adminBlockProperty(Request $request) {

        $validation = Validator::make($request->all(),[
            'property_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $current_time = Carbon::now();
        $block = Property::where('id', $request->property_id)
                        ->update(['blocked_at' => $current_time]);

        return response()->json(['data' => "Property Blocked"], 200);
    }

    // Admin Unblock Property
    public function adminUnblockProperty(Request $request) {

        $validation = Validator::make($request->all(),[
            'property_id' => 'required'
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $unblock = Property::where('id', $request->property_id)
                        ->update(['blocked_at' => NULL]);
        
        return response()->json(['data' => "Property Unblocked"], 200);
    }

    //Admin City County for other properties
    public function adminCityCounty(Request $request) {

        $query = ExtraProperty::selectRaw("user_id, city, county, state, deleted_at, count(id) as count" )->where('added',0);
        if(isset($request->keyword)) {
            $keyword = $request->keyword;
            $query->where(function ($q) use($keyword) {
                $q->where('state','like','%'.$keyword.'%')->orWhere('county','like','%'.$keyword.'%')->orWhere('city','like','%'.$keyword.'%');
            });
        }

        $city_county = $query->groupBy('state', 'county', 'city')->get();
        foreach($city_county as $list) {
            $list->user = ExtraProperty::where('city', $list->city)->where('county', $list->county)->where('state', $list->state)->selectRaw('user_id as id, (Select email from users where id = user_id) as email, (Select first_name from users where id = user_id) as first_name, (Select last_name from users where id = user_id) as last_name, (Select image from users where id = user_id) as image, address_1')->get();
            // $list->user = User::whereIn('id', $user_ids)->select('id', 'email', 'first_name', 'last_name', 'image')->get();
        }
        

        //return json response
        return response()->json($city_county,200); 
    }
    
    
    // *******************************
    // Version 2
    // *******************************

        // delete extra prop
        public function adminDeleteExtraProp(Request $request) {

            $current_time = Carbon::now();

            ExtraProperty::where('city', $request->city)->where('county', $request->county)->delete();
            return response()->json(['data' => "Deleted"], 200);
        }

        // resolve extra prop
        public function adminResolveExtraProp(Request $request) {

            $current_time = Carbon::now();

            if($request->resolve == 1) {
                ExtraProperty::where('city', $request->city)->where('county', $request->county)->update(['deleted_at' => $current_time]);
                return response()->json(['data' => "Resolved"], 200);
            }
            elseif($request->resolve == 0) {
                ExtraProperty::where('city', $request->city)->where('county', $request->county)->update(['deleted_at' => NULL]);
                return response()->json(['data' => "UnResolved"], 200);
            }

        }

        //analysis
        public static function adminAnalysis() {
            
            $city = City::select('id', 'name')->get();
            foreach($city as $list) {
                $list->waiting_count = Property::where('city', $list->id)->where('status', 0)->count('id');
                $list->active_count = Property::where('city', $list->id)->where('status', '<>', 0)->where('status', '<>', 3)->count('id');
                $list->complete_count = Property::where('city', $list->id)->where('status', 3)->count('id');
            }

            //return json response
            return response()->json($city,200); 
        }


}
