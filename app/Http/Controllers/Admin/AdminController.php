<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Admin;
use App\Models\Contact;

class AdminController extends Controller
{

    //Admin LoginCode
    public function adminLoginCode(Request $request){
        
        $validation = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validation->fails())
            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $admin_data = Admin::where('email', $request->email)->first();
        
        if (empty($admin_data))
            return response()->json(['error' => 'bad_request', 'error_description' => 'Invalid Email'], 400);
        else {
            $password = md5($request->password);
            
            if ($admin_data->password == $password) {

                // Updating the user Token
                $access_token = Admin::generateAndSaveUserToken($admin_data->id);
                
                Session::put('access_token', $access_token);
                Session::put('email',$admin_data->email);

                $admin_data1 = Admin::where('email', $request->email)->first();
                return $admin_data1;
            } else
                return response()->json(['error' => 'bad_request', 'error_description' => 'Password Incorrect'], 400);
        }
    }

    

    //Admin Change Password
    public function adminChangePassword(Request $request){
        $admin_id = $request->admin_data->access_token;
        // dd($access_token);
        $validation = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
        ]);

        if ($validation->fails())

            return response()->json(['error' => 'bad_request', 'error_description' => $validation->getMessageBag()->first()], 400);

        $admin = Admin::where('access_token',$admin_id)->first();
        // dd($admin);
        if(!empty($admin)){
            $old_password = md5($request->old_password);
            // dd($old_password);
            $new_password = md5($request->new_password);
            
            // $check = Hash::check($request->old_password, $admin->password);
            // dd($check);
            if($old_password == $admin->password){
                $change_pass = Admin::where('id',$admin->id)->update(array(
                    'password' => $new_password,
                ));
                return response()->json(['data' => "success"], 200);
            }else{
                return response()->json(['error' => 'bad_request', 'error_description' => 'Sorry Old password was incorrect.Please try again.'], 400);
            }
        }
    }

    //Admin Logout
    public function adminLogout(Request $request) {
        
        $admin_id = $request->header('Authorization');
        Admin::where('access_token', $admin_id)->update(['access_token' => ""]);
        return response()->json(['data' => "success"], 200);
    }

    //Admin Dashboard
    public function adminDashboard(){
        
        $items = Admin::adminDashboard();
        return $items;
    }

    //Admin Contact Us
    public function adminContactUs() {
       $contact = Contact::get();

       return $contact;
    }

        
    // *******************************
    // Version 2
    // *******************************

        // delete Support
        public function adminDeleteSupport(Request $request) {

            Contact::where('id', $request->contact_id)->delete();
            return response()->json(['data' => "Deleted"], 200);
        }

        // resolve Support
        public function adminResolveSupport(Request $request) {

            $current_time = Carbon::now();

            if($request->resolve == 1) {
                Contact::where('id', $request->contact_id)->update(['deleted_at' => $current_time]);
                return response()->json(['data' => "Resolved"], 200);
            }
            elseif($request->resolve == 0) {
                Contact::where('id', $request->contact_id)->update(['deleted_at' => NULL]);
                return response()->json(['data' => "UnResolved"], 200);
            }

        }

}
