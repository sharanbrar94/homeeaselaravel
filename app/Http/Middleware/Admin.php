<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use Illuminate\Support\Facades\Session;
use App\Library\CommonFunction;


class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $access_token = $request->header('Authorization');
        $admin_data = CommonFunction::validateAdminAccessToken($access_token);

        if(empty($admin_data))            
            return Response::json(array('error'=>'forbidden', 'error_description'=>'Invalid token'), 403);
        else {
            $request->admin_data = $admin_data;
            return $next($request);
        }
    }
}
