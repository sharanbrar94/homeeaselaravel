<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Closure;
use Response;
use App\Library\CommonFunction;

class ApiUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $access_token = $request->header('Authorization');
        $user_data = CommonFunction::validateAccessToken($access_token);

        if(empty($user_data))            
            return Response::json(array('error'=>'forbidden', 'error_description'=>'Invalid token'), 403);
        else {
            $request->user_data = $user_data;
            return $next($request);
        }
    }
}