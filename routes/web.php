<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

    /*************************************
    | Image Routes
    |************************************/ 

    Route::get('photos/thumb/{id}', function($id) 
    {			
        try {
            $img = Image::make('https://s3-us-west-1.amazonaws.com/homeease-test/uploads/'.$id);                
            // $img = Image::make('uploads/'.$id);
            return $img->response($img);
        }
        catch (\Exception $e) {                 	
            $img = Image::make('images/banner.png');
            return $img->response($img);                
        }    		    
    });


    Route::get('photos/thumb/{id}/{width}', function($id, $width)
    {
        try {
            $img = Image::make('https://s3-us-west-1.amazonaws.com/homeease-test/uploads/'.$id);
            // $img = Image::make('uploads/'.$id);
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            return $img->response($img);
        }
        catch (\Exception $e) {     
            $img = Image::make('images/banner.png');
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            return $img->response($img);                
        }    		    		
    });

    Route::get('photos/thumb/{id}/{width}/{height}', function($id, $width, $height)
    {
        try {
            $img = Image::make('https://s3-us-west-1.amazonaws.com/homeease-test/uploads/'.$id);
            // $img = Image::make('uploads/'.$id);
            $img->resize($width, $height);
            return $img->response($img);
        }
        catch (\Exception $e) {     
            $img = Image::make('images/banner.png');
            $img->resize($width, $height);
            return $img->response($img);                
        }    		    
    });



