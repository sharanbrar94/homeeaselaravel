<?php

use Illuminate\Http\Request;

// sudo ssh -i homeease.pem ubuntu@3.131.5.132



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


    /* -------------------------------------------------------------------
     * -------------------------------------------------------------------
     * Version  Api Routes
     * -------------------------------------------------------------------
     * ------------------------------------------------------------------- */

        Route::group(['middleware' => []], function () {
            
            /*---------------------------------
            * User Routes
            ----------------------------------*/

                $this->post('signup', 'Api\UserController@apiAgentSignUp');   //Done
                $this->post('user-signup', 'Api\UserController@apiUserSignUp');   //Done
                $this->post('start-trial', 'Api\UserController@apiStartTrial');  //Done in v3
                $this->post('check-user', 'Api\UserController@apiCheckUser');  //Done
                $this->post('login', 'Api\UserController@apiLogin');  //Done
                $this->post('investor-admin', 'Api\UserController@specialLogin'); 	
                $this->post('forgot-password','Api\UserController@apiForgotPassword'); //Done
                $this->post('reset-password','Api\UserController@apiResetPassword');  //Done   

            /*---------------------------------
            * Chat Routes
            ----------------------------------*/

                $this->post('chat/send-message', 'Api\ChatController@apiSendMessage');   //Done
                $this->get('chat/get-recent-messages', 'Api\ChatController@apiGetRecentMessages');   //Done 
                $this->get('chat/get-previous-messages', 'Api\ChatController@apiPreviousMessages');  //Done 
                $this->get('chat/conversations', 'Api\ChatController@apiConversations');   //Done

            /*---------------------------------
            * Static Data Routes
            ----------------------------------*/

                $this->get('expense-details', 'Api\ApiController@apiExpenseDetailsListing');  //Done

            /*---------------------------------
            * Static Data Routes
            ----------------------------------*/
                
                $this->get('cities', 'Api\CityController@apiCitiesListing');  //done
                $this->get('counties', 'Api\CityController@apiCountiesListing');  //done
                $this->get('cities-all', 'Api\CityController@apiAllCities');  //done

                //v3
                $this->get('v3/states', 'Api\CityController@apiStatesListing');

            /*---------------------------------
            * Test Routes
            ----------------------------------*/

                $this->get('test-stripe', 'Api\ApiController@testStripe');  //done
                $this->get('mail-test', 'Api\ApiController@mailTest');  //done

            /*---------------------------------
            * Subscription Routes
            ----------------------------------*/
                
                $this->get('subscribe', 'Api\UserController@apiRenewSubscription'); //Done

            /*---------------------------------
            * Agent Payment Routes
            ----------------------------------*/
            
                $this->get('agent-payment', 'Api\ApiController@apiAgentPayment'); //Done


            /*---------------------------------
            * Contact Us Routes
            ----------------------------------*/

                $this->post('contact-us', 'Api\ApiController@apiContactUs');  //Done

            
            /*---------------------------------
            * Image script Route
            ----------------------------------*/

                $this->get('image-delete', 'Api\ApiController@apiImageDelete');



                 $this->post('upload-image', 'Api\ApiController@apiUploadImage');  

        });


        Route::group(['middleware' => ['apiuser']], function () {

            /*---------------------------------
            * User Routes
            ----------------------------------*/
            
                $this->get('profile', 'Api\UserController@apiProfileUser'); //Done
                $this->post('logout', 'Api\UserController@apiLogout');  //Done
                $this->put('change-password', 'Api\UserController@apiChangePassword');  //Done  
                $this->get('user-dashboard', 'Api\UserController@apiUserDashboard');   //Done
                $this->get('agent-dashboard', 'Api\UserController@apiAgentDashboard');   //Done
                $this->put('edit-profile', 'Api\UserController@apiEditProfile');  //Done
                $this->delete('deactivate-account', 'Api\UserController@apiDeactivateAccount');  //Done
                $this->get('user-compd', 'Api\UserController@apiUserCompd');  //Done

                //subscription routes
                $this->post('add-subscription', 'Api\UserController@apiAddSubscription');  //Done
                $this->post('cancel-subscription', 'Api\UserController@apiCancelSubscription');  //Done
                


            /*---------------------------------
            * Property Routes
            ----------------------------------*/

                $this->post('add-comp-prop', 'Api\PropertyController@apiAddCompProp');  //done
                $this->put('edit-property', 'Api\PropertyController@apiEditProperty');  //done
                $this->put('edit-property-free-eappraisal', 'Api\PropertyController@apiEditPropertyFreeEAppraisal');  //done
                $this->get('comp-data', 'Api\PropertyController@apiCompData'); //done
                $this->get('property-details', 'Api\PropertyController@apiPropertyDetails'); //done
                $this->post('take-shortcut', 'Api\PropertyController@apiTakeShortcut');  //done
                $this->post('remove/prop', 'Api\PropertyController@apiRemoveProperty');  //done
                $this->post('delete/property', 'Api\PropertyController@apiDeleteProperty');  //done


            /*---------------------------------
            * Apartment Routes
            ----------------------------------*/

                $this->get('apartment-data', 'Api\ApartmentController@apiApartmentData');  //done
                $this->post('apartment-calculation', 'Api\ApartmentController@apiApartmentCalculation'); //done

            /*----------------------------------
            * Adjustment Routes
            -----------------------------------*/

                /*-----------------Calculation Routes-----------------------*/
                $this->get('step1-calculation', 'Api\AdjustmentController@apiStep1Calculation');  //done
                $this->get('step2-data', 'Api\AdjustmentController@apiStep2Data');  //done
                $this->post('step2-calculation', 'Api\AdjustmentController@apiStep2Calculation');  //done
                $this->get('step3-data', 'Api\AdjustmentController@apiStep3Data');  //done
                $this->post('step3-calculation', 'Api\AdjustmentController@apiStep3Calculation');  //done
                $this->get('step4-data', 'Api\AdjustmentController@apiStep4Data');  //done
                $this->post('step4-calculation', 'Api\AdjustmentController@apiStep4Calculation');  //done


                /*------------------Review Page Routes--------------------- */
                $this->get('review-page', 'Api\AdjustmentController@apiReviewPage');  //Done
                $this->post('cash-flow', 'Api\AdjustmentController@apiCashFlow'); //Done
                $this->post('agent-review', 'Api\AdjustmentController@apiAgentReview'); 
                $this->get('agent-review-data', 'Api\AdjustmentController@apiAgentReviewData');  
                $this->put('edit-agent-review', 'Api\AdjustmentController@apiEditAgentReview'); 



                /*-----------------Homey Opinion Routes------------------- */
                $this->post('homey-opinion', 'Api\AdjustmentController@apiHomeyOpinion'); //Done
                $this->post('homey-opinion-payment', 'Api\AdjustmentController@apiHomeyOpinionPayment'); //Done
                

            /*---------------------------------
            * Chat Routes
            ----------------------------------*/

                $this->get('chat/get-messages', 'Api\ChatController@apiMessages');  //Done       
                $this->delete('delete-conversation', 'Api\ChatController@apiDeleteConversation'); //Done
                $this->post('payment-check', 'Api\ChatController@apiPaymentCheck');  //Done
                $this->post('chat/payment', 'Api\ChatController@apiChatPayment');  //Done
                $this->post('chat-request', 'Api\ChatController@apiChatRequest');  //Done
            
            /*---------------------------------
            * Upload File Routes
            ----------------------------------*/

                $this->post('upload-file', 'Api\ApiController@apiUploadFile');  //not done

            /*---------------------------------
            * Upload Image Routes
            ----------------------------------*/


                $this->post('upload-image', 'Api\ApiController@apiUploadImage');  //Done

               //Done




            
        });



    /*********************************************************************
     * *******************************************************************
     * Admin Routes
     * *******************************************************************
    ******************************************************************** */
            
        Route::group(['prefix' => 'admin'], function () {    
                        
            $this->post('login', 'Admin\AdminController@adminLoginCode'); 
            
            $this->get('logout', 'Admin\AdminController@adminLogout');   
        });   


        Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function (){

            $this->get('dashboard', 'Admin\AdminController@adminDashboard');      //done
            $this->post('change-password','Admin\AdminController@adminChangePassword');   //done  
            $this->get('contact-us', 'Admin\AdminController@adminContactUs');  //done
            $this->post('admin-upload-file', 'Api\ApiController@apiUploadFile');   //done 

            /*---------------------------------
            * Version 2
            ----------------------------------*/

                $this->post('v2/resolve-support', 'Admin\AdminController@adminResolveSupport');  //done
                $this->post('v2/delete-support', 'Admin\AdminController@adminDeleteSupport');  //done

            /*---------------------------------
            * User Routes
            ----------------------------------*/
            
                $this->get('user-listing', 'Admin\UserController@userListing');  //done
                $this->get('agent-listing', 'Admin\UserController@agentListing');  //done
                $this->get('user-detail', 'Admin\UserController@userDetail');  //done
                $this->put('block-user', 'Admin\UserController@adminBlockUser');   //done
                $this->put('unblock-user', 'Admin\UserController@adminUnblockUser');  //done

                /*---------------------------------
                * Version 3
                ----------------------------------*/

                    $this->put('v3/add-credits', 'Admin\UserController@addUserCredits');  //done
                    $this->delete('v3/delete-file', 'Api\UserController@apiDeleteFile');  

            /*---------------------------------
            * Property Routes
            ----------------------------------*/

                $this->get('single-property-listing', 'Admin\PropertyController@propertyListing');  //done
                $this->get('duplex-listing', 'Admin\PropertyController@duplexListing');  //done
                $this->get('condo-listing', 'Admin\PropertyController@condoListing');  //done
                $this->get('property-detail', 'Admin\PropertyController@propertyDetail');  //done
                $this->get('apartment-listing', 'Admin\PropertyController@apartmentListing');  //done
                $this->get('apartment-detail', 'Admin\PropertyController@apartmentDetail');  //done
                $this->put('block-property', 'Admin\PropertyController@adminBlockProperty');   //done
                $this->put('unblock-property', 'Admin\PropertyController@adminUnblockProperty');  //done
                $this->get('city-county', 'Admin\PropertyController@adminCityCounty');  //done

                /*---------------------------------
                * Version 2
                ----------------------------------*/

                    $this->post('v2/resolve-extra-prop', 'Admin\PropertyController@adminResolveExtraProp');  //done
                    $this->post('v2/delete-extra-prop', 'Admin\PropertyController@adminDeleteExtraProp');  //done
                    $this->get('v2/analysis', 'Admin\PropertyController@adminAnalysis');  //done

            /*---------------------------------
            * City Routes
            ----------------------------------*/

                $this->get('cities-all', 'Admin\CityController@citiesListing');  //done
                $this->get('counties-all', 'Admin\CityController@apiAllCounties'); 
                $this->get('counties-listing', 'Admin\CityController@countiesListing');  //done
                $this->get('city-list', 'Admin\CityController@cityList');  //done
                $this->post('add-county', 'Admin\CityController@addCounty');  //done
                $this->post('add-city', 'Admin\CityController@addCity');  //done
                $this->get('multiliers-list', 'Admin\CityController@multiplierList');  //done
                $this->put('disable-county','Admin\CityController@disableCounty');  //done
                $this->put('enable-county','Admin\CityController@enableCounty');  //done

                /*---------------------------------
                * Version 2
                ----------------------------------*/

                    $this->get('v2/agent-city', 'Admin\CityController@adminAgentCity');  //done
                    $this->get('v2/agent-county', 'Admin\CityController@adminAgentCounty');  //done
                    $this->post('v2/approve-city-county', 'Admin\CityController@adminApproveCityCounty');  //done

                
                /*---------------------------------
                * Version 3
                ----------------------------------*/

                    $this->get('v3/all-states', 'Admin\CityController@adminAllStates');
                    $this->put('v3/disable-state','Admin\CityController@disableState');
                    $this->put('v3/enable-state','Admin\CityController@enableState');
                    $this->post('v3/add-state', 'Admin\CityController@addState');  //done
                    $this->put('v3/multipliers','Admin\CityController@editMultipliers');
            
            /*---------------------------------
            * Adjustment Routes
            ----------------------------------*/

                $this->get('step1', 'Admin\PropertyController@apiStep1Data');  //done
                $this->get('step2', 'Api\AdjustmentController@apiStep2Data');  //done
                $this->get('step3', 'Api\AdjustmentController@apiStep3Data');  //done
                $this->get('step4', 'Api\AdjustmentController@apiStep4Data');  //done
                
            /*---------------------------------
            * Review Routes
            ----------------------------------*/

                $this->get('review', 'Api\AdjustmentController@apiReviewPage');   //done
                $this->get('agent-review', 'Api\AdjustmentController@apiAgentReviewData');  //done
        });

    /* -------------------------------------------------------------------
     * -------------------------------------------------------------------
     * Version 2 Api Routes
     * -------------------------------------------------------------------
     * ------------------------------------------------------------------- */

        Route::group(['prefix' => 'v2'], function () {

            /*---------------------------------
            * Cron Routes
            ----------------------------------*/

                $this->get('follow-up', 'Api\ApiController@apiFollowUpEmail'); //Done 


            /*---------------------------------
            * send email Routes
            ----------------------------------*/

                $this->post('send-email', 'Api\ApiController@apiSendEmail'); //Done 

            /*---------------------------------
            * Connect to pro
            ----------------------------------*/

                $this->post('connect-to-pro', 'Api\ApiController@apiConnectToPro'); //Done 

        });
        
        Route::group(['prefix' => 'v2', 'middleware' => ['apiuser']], function () {

            /*---------------------------------
            * Apartment Routes
            ----------------------------------*/

                $this->post('apartment-payment', 'Api\ApartmentController@apiApartmentPayment');  //not done 
                
            /*---------------------------------
            * User Routes
            ----------------------------------*/

                $this->post('invite-user', 'Api\UserController@apiInviteUser');  //Done
                $this->post('delete-invite', 'Api\UserController@apiDeleteInvite');  //Done

            /*---------------------------------
            * Property Routes
            ----------------------------------*/

                $this->post('finish-calculation', 'Api\AdjustmentController@apiFinishCalculation');  //Done
                $this->post('add-comment', 'Api\AdjustmentController@apiAddComment');  //Done
                $this->get('complete-calculation', 'Api\AdjustmentController@apiCompleteCalculation');//Done

        });


    /* -------------------------------------------------------------------
     * -------------------------------------------------------------------
     * Version 3 Api Routes
     * -------------------------------------------------------------------
     * ------------------------------------------------------------------- */

    Route::group(['prefix' => 'v3'], function () {

        /*---------------------------------
        * Static Data Routes
        ----------------------------------*/

        $this->get('states', 'Api\CityController@apiStatesListing');//Done
        $this->get('counties-all', 'Api\CityController@apiAllCounties'); //Done

        /*---------------------------------
        * Cron Routes
        ----------------------------------*/

        $this->get('unread-notif', 'Api\ChatController@unreadChatNotif');

    });
    
    // Route::get('testing-api', function() {
    //         dd("Hello Pappu Pelu");
    //     });

    Route::group(['prefix' => 'v3', 'middleware' => ['apiuser']], function () {


        /*---------------------------------
        * User Routes
        ----------------------------------*/

            //$this->post('start-trial', 'Api\UserController@apiStartTrial1'); //Done 
            
            //$this->post('apartment-connect', 'Api\UserController@apartmentConnect');
            //$this->post('apartment-join-contact-list', 'Api\UserController@apartmentJoinContactList');
            $this->post('/add-file', 'Api\UserController@apiAddFile');  //Done
            $this->delete('/delete-file', 'Api\UserController@apiDeleteFile');  //Done
            $this->post('/broker-payment', 'Api\UserController@apiBrokerPayment');  //Done
            $this->post('/start-pricing-payment', 'Api\UserController@startPricingPayment');
            $this->post('/coupon-discount', 'Api\UserController@couponDiscount');
            $this->post('/get-in-touch', 'Api\ApiController@getIntouch');


    });
    Route::group(['prefix' => 'v3', 'middleware' => []], function () {

   
        $this->post('incomplete-pricing', 'Api\UserController@incompletePricing');
        $this->post('incomplete-calculation', 'Api\UserController@incompleteCalculation');
        $this->post('start-trial', 'Api\UserController@apiStartTrial1');
        $this->post('apartment-connect', 'Api\UserController@apartmentConnect');
        $this->post('apartment-join-contact-list', 'Api\UserController@apartmentJoinContactList');
        $this->post('apartment-start-pricing', 'Api\UserController@apartmentStartPricing');
        
        $this->get('sendemail', 'Api\UserController@sendEmail');
    });
    

