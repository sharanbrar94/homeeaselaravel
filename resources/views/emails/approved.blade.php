<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$user_details->first_name}} {{$user_details->last_name}},</p>
      <p style="font-size: 15px;">HomeEase has expanded and in now available in your location, {{$city_name}}! Click the link below to Start Pricing!</p>
      <p style="font-size: 15px;"><a href="https://homeease.pro/">HomeEase.pro</a></p>
      <p style="font-size: 15px;">Thank You!</p>

    </div>
 
  </body>
</html>