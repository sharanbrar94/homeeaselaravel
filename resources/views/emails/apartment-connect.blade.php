<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$request->first_name.' '.$request->last_name}},</p>
      <p style="font-size: 15px;"><strong>{{$request->email}}</strong> has reached out to you.</p>
      <p style="font-size: 18px;">Below are the details:</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>First Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->first_name}}
                </td>
  			    </tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Last Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->last_name}}
                </td>
  			    </tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Email : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->email}}
                </td>
  			    </tr>
            </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Preferred communication : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->pref_communication}}
                </td>
  			</tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Phone No : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if(isset($request->phone_no))
                    {{$request->phone_no}}
                @else
                    -
                @endif
                
                </td>
  			</tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Property Address : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->property_address}}
                </td>
  			
        <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>How Soon?</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->how_soon}}
                </td>
  			</tr>
  			

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'> What questions do you have bout your sale?</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->question}}
                </td>
  			
           


        </table>
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thank You!</p>

    </div>
 
  </body>
</html>