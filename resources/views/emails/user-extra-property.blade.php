<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$first_name.' '.$last_name}},</p>
      <p style="font-size: 15px;">Thank you for submitting your request! As HomeEase works on expanding its database, some locations are not yet ready to support self-appraisal use. The e-appraisal options are still available and are done custom by our pro's! We are sorry for the inconvenience and encourage you to contact support to let them know you need HomeEase self-appraisals in your area!</p>
      <p style="font-size: 15px;">All the best!</p>
      <p style="font-size: 15px;">If you have any questions feel free to <a href="https://homeease.pro/about">contact us</a> on our website or reply to this email.</p>

    </div>
 
  </body>
</html>