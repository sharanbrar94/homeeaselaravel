<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head> 
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$userData->first_name.' '.$userData->last_name}},</p>
      <p style="font-size: 15px;">Congratulations on taking pricing into your own hands with HomeEase! As a unbiased third party resource, we can help you get the most out of your apartment investment.</p>
      <p style="font-size: 15px;">If you are looking for qualified buyers or a qualified broker to represent you, reply to this email and use us as a resource! We have a network of buyers and brokers we can connect you with that may be right for you. </p>
      <p style="font-size: 15px;line-height: 24px;margin-top: 10px;;">All the best,</p>
      <p style="font-size: 15px;"><a href="http://3.131.5.132:8083/investor">Homeease</a></p>

    </div>
 
  </body>
</html>