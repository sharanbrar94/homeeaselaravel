<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;">Hello,</p>
      <p style="font-size: 15px;">{{$first_name}} {{$last_name}} has added a new property on HomeEase for a location where the tool is not available yet.</p>
      <p style="font-size: 18px;">Below are the details of the property:</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>User Email:</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$email}}
                </td>
  			</tr>
            @if($new_property->phone_number)
                <tr class="mail">
                    <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Phone Number:</th>
                    <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{'+'.$new_property->country_code.''.$new_property->phone_number}}
                    </td>
                </tr>
            @endif
  			<tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Address:</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$new_property->address_1}}
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Type:</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($new_property->type==1)
                    Single
                @elseif($new_property->type==2)
                    Duplex
                @elseif($new_property->type==3)
                    Condo
                @elseif($new_property->type==4)
                    Apartment
                @endif
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Agency Type:</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($new_property->agency_type==1)
                        I have an Agent
                    @elseif($new_property->agency_type==2)
                        Seeking knowledgeable agent
                    @elseif($new_property->agency_type==3)
                        For sale by owner
                    @elseif($new_property->agency_type==4)
                        For sale by owner
                    @endif
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>City</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$new_property->city}}
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>County</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$new_property->county}}
                </td>
  			</tr>
            @if($new_property->zip_code != '')
                <tr class="mail">
                    <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Zip Code</th>
                    <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$new_property->zip_code}}
                    </td>
                </tr>
            @endif
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>How Soon</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($new_property->how_soon==1)
                    As soon as possible
                @elseif($new_property->how_soon==2)
                    In the next 3 months
                @elseif($new_property->how_soon==3)
                    In 3-6 months
                @elseif($new_property->how_soon==4)
                    In 6-12 months
                @elseif($new_property->how_soon==5)
                    I am just curious
                @endif
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Selling Status</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($new_property->selling_status==1)
                    Selling
                @elseif($new_property->selling_status==2)
                    Buying
                @endif
                </td>
  			</tr>


        </table>
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thank You!</p>

    </div>
 
  </body>
</html>