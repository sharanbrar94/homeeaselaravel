<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"> </p>
      <p style="font-size: 15px;">Hello {{$user->first_name.' '.$user->last_name}},</p>
      <p style="font-size: 15px;">You have partially completed your request to price your property! Here is a <a href="http://3.131.5.132:8083/dashboard/user">link</a> to our website so you can finish submitting your request! </p>
      <p>We are ready to help you succeed</p>
      <p style="font-size: 18px;">See you soon,</p>
      <p style="font-size: 15px;">HomeEase.pro</p>
      
    </div>
 
  </body>
</html>