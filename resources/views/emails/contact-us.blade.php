<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 18px;font-weight: 600;">Hello HomeEase,</p>
      @if($contact->type == 2)
        <p style="font-size: 15px;">Agent {{$contact->first_name}} would like to join your network of referable agents!</p>
      @else
        <p style="font-size: 15px;">{{$contact->first_name}} has {{$contact->subject}}</p>
      @endif
      
      <p style="font-size: 15px;"><strong>Email: </strong>{{$contact->email}}</p>
      <p style="font-size: 15px;"><strong>Phone Number: </strong>{{$contact->phone_number}}</p>
      @if($contact->type == 1)
        <p style="font-size: 15px;"><strong>Message: </strong>{{$contact->message}}</p>
      @else
        <p style="font-size: 15px;"><strong>Mls: </strong>{{$contact->mls}}</p>
        <p style="font-size: 15px;"><strong>State: </strong>{{$contact->state}}</p>
        <p style="font-size: 15px;"><strong>City: </strong>{{$contact->city}}</p>
        <p style="font-size: 15px;"><strong>Website(url): </strong>{{$contact->website}}</p>
      @endif
        <p style="font-size: 15px;">Thank You!</p>

    </div>
 
  </body>
</html>