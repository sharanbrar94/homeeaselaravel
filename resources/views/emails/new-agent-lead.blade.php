<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;">Hello {{$request->first_name.' '.$request->last_name}},</p>
      <p style="font-size: 15px;">{{$request->email}} has reached out to you.</p>
      <p style="font-size: 18px;">Below are the details:</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>First Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->first_name}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Last Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->last_name}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Email : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->email}}
                </td>
  			    </tr>
            
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Preferred communication : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->pref_communication}}
                </td>
  			</tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Phone No : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if(isset($request->phone_no))
                    {{$request->phone_no}}
                @else
                    -
                @endif
                
                </td>
  			</tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>User Is : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->selling_status}}
                </td>
  			</tr>
        @if($request->selling_status == 'selling')
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Type of property :  </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->property_type}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Property Address : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->property_address}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>City and State : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->location}}
                </td>
  			</tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>HomeEase e-appraisal : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($request->homeease_e_appraisal == 'no')
                      {{$request->homeease_e_appraisal.' ('.$request->price.')' }}
                    @else
                      {{$request->homeease_e_appraisal }}
                    @endif
                </td>
  			    </tr>
        @else
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Type of property : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->property_type}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Price Range : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->price_range}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Estimated Credit Score  : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->estimated_credit_score}}
                </td>
  			    </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Prequalified :  </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($request->neither_prequalified != '')
                      {{$request->prequalified.' ('.$request->neither_prequalified.')' }}
                    @else
                      {{$request->prequalified }}
                    @endif
                </td>
  			    </tr>
        @endif
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>When would you like to get started?</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->how_soon}}
                </td>
  			</tr>
  			


            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>What is the biggest real estate question you have that we can help you with?</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$request->question}}
                </td>
  			</tr>
            
           


        </table>
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thank You!</p>

    </div>
 
  </body>
</html>