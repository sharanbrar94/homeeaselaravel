<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$name}}</p>
      <p style="font-size: 15px;">Your payment has been successful.</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
        <tr class="mail">
            <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>DESCRIPTION</th>
            <td style='text-align:left;font-size: 16px;padding: 8px 0;'>AMOUNT</td>
        </tr>
        <tr class="mail">
            <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Professional pricing (Shortcut)</th>
            <td style='text-align:left;font-size: 16px;padding: 8px 0;'>${{$amount}}</td>
        </tr>
      </table>
      <p style="font-size: 15px;">Thank you for using HomeEase.</p>
      <p style="font-size: 15px;">If you have any questions, feel free to <a href= "https://homeease.pro/about">contact us</a> by clicking the link or replying to this email. </p>
    </div>
  </body>
</html>