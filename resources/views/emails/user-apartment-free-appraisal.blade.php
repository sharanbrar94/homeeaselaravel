<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$request->first_name.' '.$request->last_name}},</p>
      <p style="font-size: 15px;">You’ve made the right choice consulting a professional! We have received your request to be connected with a local broker. Once we complete your free e-appraisal, it will be sent to a proven apartment broker in your area so they can share the results with you!</p>
      <p style="font-size: 15px;">The investment professionals at HomeEase are working hard to keep up with requests. They should be able to complete yours within 48 hours and will reach out to you if they have any questions.</p>
      <p style="font-size: 15px;">All the best!</p>
      <p style="font-size: 15px;">If you have any questions feel free to <a href="https://homeease.pro/about">contact us</a> on our website or reply to this email.</p>

    </div>
 
  </body>
</html>