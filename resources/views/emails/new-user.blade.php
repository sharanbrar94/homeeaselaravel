<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;font-size: 16px;">
    
        <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </p>
      <p style="font-size: 15px;">Hello {{$request->first_name.' '.$request->last_name}},</p>
      <p style="font-size: 15px;"> Congrats on taking pricing into your own hands with HomeEase! Below is a password for you to log in to your account to view your results from our team. You can always change this password by clicking “forgot password” and entering your email on our website. <a href="https://homeease.pro">HomeEase.pro</a></p>
      <p style="font-size: 15px;">Email : {{$request->email}}<strong></strong></p>
      <p style="font-size: 15px;">Password : {{$password}}<strong></strong></p>
      
      <p style="font-size: 15px;line-height: 24px;margin-top: 10px;;">Thank You!</p>
      <p style="font-size: 15px;">Any questions? Just reply to this email to ask.</p>

    </div>
 
  </body>
</html>
