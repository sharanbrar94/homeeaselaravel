<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
        <div style="display:flex;">
        <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </div>
      <p style="width: 100%;text-align: center;margin-bottom: 30px;font-size: 16px;">
        Reset your password
      </p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$name->first_name.' '.$name->last_name}},</p>
      
      <p style="font-size: 15px;">Want to create a new password for your account? CLick the link below. If you requested this by accident, please disregard this email.<strong></strong></p>
      <button type="button" style="background-color: #1579bf; border:none; color:#fff; border-radius:4px; font-size: 12px;text-transform: uppercase; font-weight: 600;padding: 14px 24px; outline:none; font-family: arial; cursor: pointer; align:center;"><a style="color:#fff;" href="{{$url}}?token={{$token}}">Reset Password</a></button>
      
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thanks You!</p>

    </div>
 
  </body>
</html>