<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </p>
      <p style="font-size: 15px;font-weight: 600;">Hello {{$data1->first_name}} {{$data1->last_name}},</p>
      <p style="font-size: 15px;">Below is a breakdown of your account charges and credits from {{$start}} to {{$end}}.The balance will be charged to the payment method we have on file.</p>
      <!-- <p style="font-size: 18px;">Below are the details:</p> -->
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>DESCRIPTION</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                AMOUNT
                </td>
            </tr>
            @foreach($payment_data as $key => $value)
            @if($value->payment_type == 6)
                <tr class="mail">
                    <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Subscription Fee</th>
                    <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    ${{$value->amount}}
                    </td>
                </tr>
            @elseif($value->payment_type == 4)
                <tr class="mail">
                    <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Referral for chat</th>
                    <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    ${{$value->amount}}
                    </td>
                </tr>
            @endif
            @endforeach

  			<tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Credits</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($data1->credits > 0)
                  $-{{$data1->credits}}
                @else
                  $0
                @endif
                </td>
  			</tr>

              <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'></th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                
                </td>
  			</tr>
              <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'></th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                
                </td>
  			</tr>
              <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Total Payable</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if(($total_amount - $data1->credits) >= 0)
                    ${{$total_amount - $data1->credits}}
                @else 
                    $0
                @endif
                </td>
  			</tr> 

              <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Balance Credits</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if(($data1->credits - $total_amount) >= 0)
                    ${{$data1->credits - $total_amount}}
                @else 
                    $0
                @endif
                </td>
  			</tr> 


        </table>
        <p style="font-size: 15px;">We are happy to answer any questions.<a href= "https://homeease.pro/about">click here</a></p>
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thank You!</p>

    </div>
 
  </body>
</html>