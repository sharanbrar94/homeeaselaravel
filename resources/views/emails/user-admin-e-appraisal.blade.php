<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
    <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;"></p>
      <p style="font-size: 15px;font-weight: 600;">{{$request->first_name.' '.$request->last_name}} has paid for home appraisal:</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
            <tr class="mail">
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>Address : </td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>{{$property->address_1}}</td>
  			    </tr>

            <tr class="mail">
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>Phone Number : </td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>+{{$property->country_code.''.$property->phone_number}}</td>
  			    </tr>

            <tr class="mail">
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>Email : </td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>{{$request->email}}</td>
  			    </tr>

            <tr class="mail">
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>Agency Type : </td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($property->agency_type==1)
                    I have a great agent
                @elseif($property->agency_type==2)
                    I need a great agent
                @elseif($property->agency_type==3)
                    I don't need an broker
                @elseif($property->agency_type==4)
                    Unsure
                @endif
                
                </td>
  			</tr>

            <tr class="mail">
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>Selling or Buying : </td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($property->selling_status==1)
                        Selling
                    @elseif($property->selling_status==2)
                        Buying
                    @endif
                </td>
                
  			</tr>

        <tr class="mail">
            <td style='text-align:left;font-size: 16px;padding: 8px 0;'>How Soon?</td>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($property->how_soon==1)
                        As soon as possible
                    @elseif($property->how_soon==2)
                        In the next 3 months
                    @elseif($property->how_soon==3)
                        In 3-6 months
                    @elseif($property->how_soon==4)
                        In 6-12 months
                    @elseif($property->how_soon==5)
                        I am just curious
                    @endif
                </td>
                
  			</tr>
      </table>
      
      <p style="font-size: 15px;">Complete this e-appraisal ASAP!</p>
      <p style="font-size: 15px;">Congrats!</p>
    </div>
 
  </body>
</html>