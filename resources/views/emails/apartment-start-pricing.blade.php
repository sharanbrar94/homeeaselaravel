<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>email template</title>
  </head>
  <body>
  <div class="temp_wdt" style="margin: auto;width: 70%;color:black;background-color: #D8E9F7;padding:25px">
      <p style="float: left;width: 100%;text-align: center;margin-bottom: 30px;">
      <img src="{{url('/email.png')}}" style="width: 240px;margin-bottom: 12px;">
      </p>
      <p style="font-size: 15px;font-weight: 600;">Hello Ben,</p>
      <p style="font-size: 15px;"><strong>{{$userData->email}}</strong> has reached out to you.</p>
      <p style="font-size: 18px;">Below are the details:</p>
      <table border='0'  style='text-align:left;padding: 0 35px; margin-bottom: 0;margin-top: 0; width:100%;'>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>First Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$userData->first_name}}
                </td>
  			    </tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Last Name : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$userData->last_name}}
                </td>
  			    </tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Email : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                {{$userData->email}}
                </td>
  			    </tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Phone No : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->phone_number}}
                </td>
  			</tr>

            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Address : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    {{$request->address_1}}
                </td>
                
  			</tr>

        <tr class="mail">
            <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Property Type</th>
            <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                
                @if($request->type==1)
                   Single
                @elseif($request->type==2)
                   Duplex
                @elseif($request->type==3)
                   Condo
                @elseif($request->type==4)
                   Apartment
                @endif
                
            </td>
                
        </tr>

        <tr class="mail">
            <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Agency Type</th>
            <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                @if($request->type != 4)
                    @if($request->agency_type==1)
                        I have a great agent
                    @elseif($request->agency_type==2)
                        I need a great agent
                    @elseif($request->agency_type==3)
                        For sale by owner
                    @elseif($request->agency_type==4)
                        I am a broker/agent
                    @endif
                @else   
                    @if($request->agency_type==1)
                        I have a great broker
                    @elseif($request->agency_type==2)
                        I need a great broker
                    @elseif($request->agency_type==3)
                        I don't need a broker
                    @elseif($request->agency_type==4)
                        Unsure
                    @endif
                @endif
            </td>
                
        </tr>

        <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>How Soon?</th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($request->how_soon==1)
                        As soon as possible
                    @elseif($request->how_soon==2)
                        In the next 3 months
                    @elseif($request->how_soon==3)
                        In 3-6 months
                    @elseif($request->how_soon==4)
                        In 6-12 months
                    @elseif($request->how_soon==5)
                        I am just curious
                    @endif
                </td>
                
        </tr>

        <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Selling Status : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($request->selling_status==1)
                        Selling
                    @elseif($request->selling_status==2)
                        Buying
                    @endif
                </td>
                
  			</tr>
  		@if($request->selling_status==2)
            <tr class="mail">
                    <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Credit Score : </th>
                    <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                        {{$request->credit_score}}
                    </td>
            </tr>
            <tr class="mail">
                <th style='text-align:left;font-size: 16px;padding: 8px 3px;text-align: left;width: 180px;'>Prequalified : </th>
                <td style='text-align:left;font-size: 16px;padding: 8px 0;'>
                    @if($request->prequalified==1)
                        Pre-approved
                    @elseif($request->prequalified==2)
                        Pre-qualified
                    @elseif($request->prequalified==3)
                        Neither
                    @endif
                </td>
            </tr>
        @endif
  			
        </table>
      <p style="text-align: center;margin-top: 20px;float: left;width: 100%;">
      <p style="font-size: 15px;float: left;line-height: 24px;margin-top: 12px;;">Thank You!</p>

    </div>
 
  </body>
</html>